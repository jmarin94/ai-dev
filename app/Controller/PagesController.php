<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('User', 'UserType', 'Province', 'City', 'Information');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('contact', 'aboutUs', 'agencies', 'desarrolladoras', 'banks');
    }

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     * 	or MissingViewException in debug mode.
     */
    public function display() {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    /**
     * Página de "Contáctenos" del sitio
     */
    public function contact() {
        if($this->request->is('post')){
            $data = $this->request->data;

            $Email = new CakeEmail();
            $Email->config('america');
            $Email->emailFormat('html');
            $Email->to('info@americainmobiliaria.com');
            $Email->subject('Mensaje de la página de Contáctenos del sitio web.');
            $mensaje = "<b>Nombre: </b>&nbsp;" . $data['name'];
            $mensaje .= "<br><b>Asunto: </b>&nbsp;" . $data['subject'];
            $mensaje .= "<br><b>E-mail: </b>&nbsp;" . $data['email'];
            $mensaje .= "<br><b>Mensaje: </b>&nbsp;" . $data['message'];
            $Email->send($mensaje);

            $this->Flash->success(__('Tu mensaje fue enviado con éxito.'));
        }
    }

    /**
     * Página de "Quiénes somos" del sitio
     */
    public function aboutUs() {
        $this->set('information', $this->Information->find('first', array('page' => 1)));
    }
}
