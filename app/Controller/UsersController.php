<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $uses = array(
        'Property',
        'Project',
        'UserType',
        'User'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('add', 'login', 'forgot', 'agencies', 'banks', 'view', 'recover');
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $user = $this->User->find('first', $options);
        $this->set('user', $user);
        if ($user['User']['user_type'] != 1 && $user['User']['user_type'] != 6) {
            $this->Paginator->settings = array(
                'conditions' => array(
                    'Property.users_id' => $id
                ),
                'limit' => 15
            );
            $this->set('properties', $this->Paginator->paginate('Property'));
        } else if ($user['User']['user_type'] == 6) {
            $this->Paginator->settings = array(
                'conditions' => array(
                    'Project.users_id' => $id
                ),
                'limit' => 15
            );
            $this->set('projects', $this->Paginator->paginate('Project'));
        }
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            if ($data['User']['confirm_password'] == $data['User']['password']) {
                $this->User->create();
                if ($this->User->save($data)) {
                    $this->Flash->success(__('Tu cuenta fue creada con éxito.'));
                    return $this->redirect('/');
                } else {
                    $this->Flash->error(__('Tu cuenta no pudo ser creada. Por favor intenta de nuevo, si el error persiste ponte en contacto con el administrador del sistema.'));
                }
            } else {
                $this->Flash->error(__('Las contraseñas que ingresaste deben ser iguales.'));
            }
        } else {
            if ($this->loggedUser) {
                if ($this->checkRole($this->loggedUser['user_type'], 'administrador')) {
                    $url = Router::url(array('controller' => 'users', 'action' => 'dashboard', 'aipanel' => true), true);
                } else {
                    $url = Router::url(array('controller' => 'users', 'action' => 'dashboard'), true);
                }
                return $this->redirect($url);
            }
        }
        $userTypes = $this->UserType->find('list');
        $this->set('userTypes', array_map('strtolower', $userTypes));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->User->exists($id) || ($id != $this->Auth->user('id'))) {
            $this->Flash->error(__('El usuario buscado no existe'));
            return $this->redirect('/');
        }
        if ($this->request->is(array('post', 'put'))) {
            $data = $this->request->data;

            if ($_FILES) {
                $hostImages = WWW_ROOT . 'img/ai/files/users/';
                $nameImage = uniqid() . '_' . $_FILES['img-profile']['name'];
                $upload = move_uploaded_file($_FILES['img-profile']['tmp_name'], $hostImages . $nameImage);
                if ($upload) {
                    $userImage['id'] = $data['User']['id'];
                    $userImage['img'] = $nameImage;
                    if ($this->User->save($userImage)) {
                        $this->Flash->success(__('Tu imagen fue actualizada con éxito.'));
                        $this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));
                        return $this->redirect(array('action' => 'dashboard'));
                    } else {
                        $this->Flash->error(__('Tu imagen de perfil no se pudo guardar, intenta de nuevo.'));
                    }
                } else {
                    $this->Flash->error(__('Tu imagen de perfil no se pudo guardar, intenta de nuevo.'));
                }
            } else {
                if ($data['User']['password'] != "") {
                    if (!($data['User']['confirm_password'] == $data['User']['password'])) {
                        $this->Flash->error(__('Las contraseñas que ingresaste deben ser iguales.'));
                        return $this->redirect(array('action' => 'edit/' . $id));
                    }
                } else {
                    unset($data['User']['password']);
                }

                if ($this->User->save($data)) {
                    $this->Flash->success(__('Tu información fue actualizada con éxito.'));
                    $this->Session->write('Auth', $this->User->read(null, $this->Auth->User('id')));
                    return $this->redirect(array('action' => 'dashboard'));
                } else {
                    $this->Flash->error(__('Tu información no se pudo guardar, intenta de nuevo.'));
                }
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                if ($this->loggedUser['user_type'] == $this->checkRole($this->loggedUser['user_type'], 'administrador')) {
                    return $this->redirect(Router::url(array('action' => 'dashboard', 'aipanel' => true), true));
                }
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Credenciales inválidas, por favor revisa e intenta de nuevo'));
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function dashboard() {
        $user = $this->Auth->user();

        if ($user['user_type'] != 1 && $user['user_type'] != 6) {
            $this->Paginator->settings = array(
               'conditions' => array(
                    'Property.users_id' => $user['id']
                ),
                'limit' => 10
            );
            $this->set('properties', $this->Paginator->paginate('Property'));
        } else if ($user['user_type'] == 6) {
            $this->Paginator->settings = array(
               'conditions' => array(
                    'Project.users_id' => $user['id'],
                ),
                'limit' => 10
            );
            $this->set('projects', $this->Paginator->paginate('Project'));
        }
        $this->set('user', $user);
    }

    public function forgot() {
        if ($this->request->is('post')) {
            $userEmail = $this->User->find('first', [
                'conditions' => [
                    'User.username' => $this->request->data('User.username')
                ]
            ]);
            if ($userEmail) {
                $userData['User']['id'] = $userEmail['User']['id'];
                $userData['User']['token'] = bin2hex(openssl_random_pseudo_bytes(16));
                if ($this->User->save($userData)) {
                    $Email = new CakeEmail();
                    $Email->config('america');
                    $Email->emailFormat('html');
                    $Email->to($userEmail['User']['username']);
                    $Email->subject('Restaurar la contraseña de tu cuenta, América Inmobiliaria');
                    $enlace = Router::url(array('controller' => 'users', 'action' => 'recover', $userEmail['User']['id'], $userData['User']['token']), true);
                    $mensaje = "<h1>Restaurar la contraseña de mi cuenta</h1>";
                    $mensaje .= "<p>Para restaurar tu contraseña, accede al siguiente link:<br/>";
                    $mensaje .= "<a href='$enlace'>$enlace</a></p>";
                    $Email->send($mensaje);
                    $this->Flash->success(__('Se envió un correo electrónico a tu dirección para restaurar tu contraseña'));
                } else {
                    $this->Flash->error(__('No se pudo crear los datos de recuperacion, por favor intentelo de nuevo'));
                }
            } else {
                $this->Flash->error(__('El correo electronico no se encuentra en nuestra base de datos'));
            }
        } else {
            if ($this->loggedUser) {
                if ($this->checkRole($this->loggedUser['user_type'], 'administrador')) {
                    $url = Router::url(array('controller' => 'users', 'action' => 'dashboard', 'aipanel' => true), true);
                } else {
                    $url = Router::url(array('controller' => 'users', 'action' => 'dashboard'), true);
                }
                return $this->redirect($url);
            }
        }
    }

    public function recover($idUser, $token) {

        $user = $this->User->find('first', [
            'conditions' => [
                'User.id' => $idUser,
                'User.token' => $token
            ]
        ]);
        if ($user) {
            $this->set('user', $user);
        } else {
            return $this->redirect('/');
        }
        if ($this->request->is('post')) {
            $userData = $this->request->data;
            if ($userData['User']['password'] == $userData['User']['confirm_password']) {
                if ($this->User->save($userData)) {
                    $this->Flash->success(__('Tu contraseña ha sido restaurada con éxito.'));
                    return $this->redirect(['controller' => 'users', 'action' => 'login']);
                } else {
                    $this->Flash->error(__('No se pudo cambiar la contraseña, por favor intenta de nuevo.'));
                }
            } else {
                $this->Flash->error(__('Las contraseñas no coinciden'));
            }
        }
    }

    /**
     * aipanel_index method
     * )
     * @return void
     */
    public function aipanel_index() {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate('User'));
    }

    /**
     * aipanel_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * aipanel_add method
     *
     * @return void
     */
    public function aipanel_add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * aipanel_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_edit($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    /**
     * aipanel_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_delete($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function aipanel_dashboard() {
        $projects = $this->Project->find('all', array('limit' => 9, 'order' => array('Project.id' => 'DESC')));
        $properties = $this->Property->find('all', array('limit' => 9, 'order' => array('Property.id' => 'DESC')));
        $this->set(array('projects' => $projects, 'properties' => $properties));
    }

    public function aipanel_logout() {
        return $this->redirect($this->Auth->logout());
    }

    
    /**
     * Página de "Agentes y agencias" del sitio
     */
    public function agencies() {
        $options = array(
            'conditions' => array(
                'OR' => array(
                    array('name' => 'Agente'),
                    array('name' => 'Agencia'),
                )
            ),
            'fields' => array('id')
        );
        $userType = $this->UserType->find('all', $options);

        $this->Paginator->settings = array(
            'conditions' => array(
                'OR' => array(
                    array('user_type' => $userType[0]['UserType']['id']),
                    array('user_type' => $userType[1]['UserType']['id']),
                )
            ),
            'fields' => array(
                'id',
                'username',
                'full_name',
                'phone',
                'description',
                'img'
            ),
            'order' => array(
                'User.outstanding' => 'DESC',
                'User.id' => 'DESC'
            )
        );
        $this->set('agencies', $this->Paginator->paginate('User'));
    }
    
    /**
     * Página de "Bancos" del sitio
     */
    public function banks() {
        $options = array(
            'conditions' => array(
                'OR' => array(
                    array('name' => 'Banco'),
                )
            ),
            'fields' => array('id')
        );
        $userType = $this->UserType->find('first', $options);

        $this->Paginator->settings = array(
            'conditions' => array(
                'OR' => array(
                    array('user_type' => $userType['UserType']['id']),
                )
            ),
            'fields' => array(
                'id',
                'username',
                'full_name',
                'phone',
                'description',
                'img'
            )
        );
        $this->set('bancos', $this->Paginator->paginate('User'));
    }
    
    //payment
    public function payment($status = 'new'){
        $userData = $this->Auth->user();
        
        if($this->checkRole($userData['user_type'], 'desarrolladora')){
            $paymentTotal = 100;
        } else if($this->checkRole($userData['user_type'], 'agencia') || $this->checkRole($userData['user_type'], 'agente')) {
            $paymentTotal = 50;
        }
        
        $data = array('paymentTotal' => $paymentTotal);

        if($status == 'new'){
            $this->set(array('data' => $data));
        } else if($status == 'success') {
            $this->User->id = $data['id'];
            $propertyData = array('outstanding' => 1);
            $this->User->save($propertyData);

            $this->Payment->create();
            $paymentData = array(
                'user_id' => $userData['id'],
                'total' => $data['paymentTotal'],
                'status' => 1,
                'message' => 'outstanding_status: ' . $this->request->data['payment_status'] . ' mc_gross: ' . $this->request->data['mc_gross'] . ' verify_sign: ' . $this->request->data['verify_sign']
            );
            $this->Payment->save($paymentData);
            $this->set(array('status' => 'sucess'));
        } else if($status == 'cancel' || $status == 'notify') {
            $this->User->id = $data['id'];
            $propertyData = array(
                'outstanding' => 0,
            );
            $this->User->save($propertyData);
            $this->set(array('status' => 'cancel'));
        }
    }
}
