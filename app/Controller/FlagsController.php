<?php
App::uses('AppController', 'Controller');
/**
 * Flags Controller
 *
 * @property Flag $Flag
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class FlagsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * aipanel_index method
 *
 * @return void
 */
	public function aipanel_index() {
		$this->Flag->recursive = 0;
		$this->set('flags', $this->Paginator->paginate());
	}

/**
 * aipanel_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function aipanel_view($id = null) {
		if (!$this->Flag->exists($id)) {
			throw new NotFoundException(__('Invalid flag'));
		}
		$options = array('conditions' => array('Flag.' . $this->Flag->primaryKey => $id));
		$this->set('flag', $this->Flag->find('first', $options));
	}

/**
 * aipanel_add method
 *
 * @return void
 */
	public function aipanel_add() {
		if ($this->request->is('post')) {
			$this->Flag->create();
			if ($this->Flag->save($this->request->data)) {
				$this->Flash->success(__('The flag has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The flag could not be saved. Please, try again.'));
			}
		}
//		$properties = $this->Flag->Property->find('list');
//		$this->set(compact('properties'));
	}

/**
 * aipanel_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function aipanel_edit($id = null) {
		if (!$this->Flag->exists($id)) {
			throw new NotFoundException(__('Invalid flag'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Flag->save($this->request->data)) {
				$this->Flash->success(__('The flag has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The flag could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Flag.' . $this->Flag->primaryKey => $id));
			$this->request->data = $this->Flag->find('first', $options);
		}
//		$properties = $this->Flag->Property->find('list');
//		$this->set(compact('properties'));
	}

/**
 * aipanel_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function aipanel_delete($id = null) {
		$this->Flag->id = $id;
		if (!$this->Flag->exists()) {
			throw new NotFoundException(__('Invalid flag'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Flag->delete()) {
			$this->Flash->success(__('The flag has been deleted.'));
		} else {
			$this->Flash->error(__('The flag could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
