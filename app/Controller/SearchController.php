<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP searchController
 * @author rudysibaja
 */
class SearchController extends AppController {

    public $uses = array(
        'Property',
        'PropertyType',
        'Country',
        'Province',
        'City',
        'Flag',
        'Promotion',
        'Category',
        'PropertiesImage',
        'PropertiesFlag',
        'User'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index');
    }
    
    public function index($propertyType = "null", $category = "null", $country = "null", $province = "null", $citie = "null") {

        if ($this->request->is('post')) {
            $data = $this->request->data('Property');
            if ($data['property_type_id'] != "")
                $propertyType = $data['type_properties_id'];
            if ($data['categories_id'] != "")
                $category = $data['categories_id'];
            if ($data['countries_id'] != "")
                $country = $data['countries_id'];
            if ($data['provinces_id'] != "")
                $province = $data['provinces_id'];
            if ($data['cities_id'] != "")
                $citie = $data['cities_id'];

            $searchUrl = $propertyType . '/' . $category . '/' . $country . '/' . $province . '/' . $citie;
            return $this->redirect(['controller' => 'Search', 'action' => 'index/' . $searchUrl]);
        }

        $conditions = array();
        if ($propertyType != "null")
            $conditions['Property.property_type_id'] = $propertyType;
        if ($category != "null")
            $conditions['Property.categories_id'] = $category;
        if ($country != "null")
            $conditions['Property.countries_id'] = $country;
        if ($province != "null")
            $conditions['Property.provinces_id'] = $province;
        if ($citie != "null")
            $conditions['Property.cities_id'] = $citie;

        $typeProperties = $this->PropertyType->find('list');
        $countries = $this->Country->find('list');
        $categories = $this->Category->find('list');

        $properties = $this->Property->find('all', [
            'conditions' => $conditions,
            'limit' => 10,
            'page' => 1,
            'oder' => [
                'Property.id' => 'DESC'
            ],
        ]);

        $this->set(['properties' => $properties]);
        $this->set(compact('typeProperties', 'countries', 'categories'));
    }

}
