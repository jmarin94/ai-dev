<?php

App::uses('AppController', 'Controller');

/**
 * Properties Controller
 *
 * @property Property $Property
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class InformationsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Flash', 'Session');
    public $uses = array('Information');
    public $pages = array(
         1 => '¿Quiénes somos?'  
    );
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->set('pages', $this->pages);
    }

    /**
     * aipanel_index method
     *
     * @return void
     */
    public function aipanel_index() {
        $this->Information->recursive = 0;
        $this->set('information', $this->Information->paginate());
    }

    /**
     * aipanel_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_view($id = null) {
        if (!$this->Information->exists($id)) {
            throw new NotFoundException(__('Invalid information'));
        }
        $options = array('conditions' => array('Information.' . $this->Information->primaryKey => $id));
        $this->set('information', $this->Information->find('first', $options));
    }

    /**
     * aipanel_add method
     *
     * @return void
     */
    public function aipanel_add() {
        if ($this->request->is('post')) {
            $this->Information->create();
            if ($this->Information->save($this->request->data)) {
                $this->Flash->success(__('The information has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The information could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * aipanel_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_edit($id = null) {
        if (!$this->Information->exists($id)) {
            throw new NotFoundException(__('Invalid information'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Information->create();
            if ($this->Information->save($this->request->data)) {
                $this->Flash->success(__('The information has been saved.'));
                return $this->redirect(array('action' => 'edit', $id));
            } else {
                $this->Flash->error(__('The information could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Information.' . $this->Information->primaryKey => $id));
            $this->request->data = $this->Information->find('first', $options);
        }
    }

    /**
     * aipanel_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_delete($id = null) {
        $this->Information->id = $id;
        if (!$this->Information->exists()) {
            throw new NotFoundException(__('Invalid information'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Information->delete()) {
            $this->Flash->success(__('The information has been deleted.'));
        } else {
            $this->Flash->error(__('The information could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

}
