<?php

App::uses('AppController', 'Controller');

/**
 * Projects Controller
 *
 * @property Project $Project
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ProjectsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $uses = array(
        'Project',
        'Country',
        'Province',
        'City',
        'ProjectsImage'
    );
    public $helper = array('getUrl');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('view', 'viewAll', 'getCities', 'getProvinces');
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'map';
        $this->Project->recursive = 0;
        $this->set('projects', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Project->exists($id)) {
                throw new NotFoundException(__('Invalid project'));
        }
        $options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
        $this->set('project', $this->Project->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $userData = $this->Auth->user();
        if (!$this->checkRole($userData['user_type'], 'desarrolladora')) {
            return $this->redirect(['controller' => 'properties', 'action' => 'add']);
        }

        if ($this->request->is('post')) {
            $this->Project->create();
            $data = $this->request->data;
            $data['Project']['outstanding'] = 1;
            if($data['Project']['free-outstanding'] == 'on'){
                $outstanding = false;
            } else {
                $outstanding = true;
            }
            if ($this->Project->save($data)) {
                if ($_FILES) {
                    $host = WWW_ROOT . 'img/ai/files/projects/' . $this->Project->getLastInsertID();
                    $hostImages = $host . DS . 'images';
                    $hostBlueprints = $host . DS . 'blueprints';
                    if(mkdir($hostImages, 0777, true) && mkdir($hostBlueprints, 0777, true)){
                        $filesImg = count($_FILES['input-file-desarrolladora']['name']);
                        $errors = array();
                        for ($i = 0; $i < $filesImg; $i++){
                            $nameImage = uniqid() . '_' . $_FILES['input-file-desarrolladora']['name'][$i];

                            $this->ProjectsImage->create();
                            $data['projects_id'] = $this->Project->getLastInsertID();
                            $data['rute'] = 'ai/files/projects/' .  $this->Project->getLastInsertID() . DS . 'images' . DS . $nameImage;
                            $data['type'] = 'image';
                            $this->ProjectsImage->save($data);
                            $upload = move_uploaded_file($_FILES['input-file-desarrolladora']['tmp_name'][$i], $hostImages . DS . $nameImage);
                            if(!$upload){
                                $this->ProjectsImage->delete();
                                $errors[] =  $_FILES['input-file-desarrolladora']['name'][$i];
                            }
                        }

                        $filesBp = count($_FILES['input-file-planos']['name']);
                        $errors = array();
                        for ($i = 0; $i < $filesBp; $i++){
                            $nameImage = uniqid() . '_' . $_FILES['input-file-planos']['name'][$i];

                            $this->ProjectsImage->create();
                            $data['projects_id'] = $this->Project->getLastInsertID();
                            $data['rute'] = 'ai/files/projects/' .  $this->Project->getLastInsertID() . DS . 'blueprints' . DS . $nameImage;
                            $data['type'] = 'blueprint';
                            $this->ProjectsImage->save($data);
                            $upload = move_uploaded_file($_FILES['input-file-planos']['tmp_name'][$i], $hostBlueprints . DS . $nameImage);
                            if(!$upload){
                                $this->ProjectsImage->delete();
                                $errors[] =  $_FILES['input-file-planos']['name'][$i];
                            }
                        }

                        if(count($errors) > 0){
                            $featuredTotal = 0;
                            if($outstanding){
                                $featuredTotal = $data['Project']['months'] * 50; //IMPORTANTE PRECIO
                            }
                            if($featuredTotal > 0){
                                $data = array(
                                    'paymentTotal' => $featuredTotal,
                                    'id' => $this->Project->id,
                                    'outstanding' => 1,
                                    'months' => $data['Project']['months']
                                );
                                $this->Session->write('paymentInfoProject', $data);
                                $url = array(
                                    'controller' => 'Projects',
                                    'action' => 'payment',
                                );

                                $this->Flash->success(__('Tu proyecto ha sido guardado éxitosamente, solo queda pendiente el pago de los destacados'));
                                $this->redirect($url);
                            } else {
                                $message = "Tu proyecto se guardó, pero hubo un error insertando los archivos adjuntos: ";
                                foreach ($errors as $error){
                                    $message .= $error . " | ";
                                }
                                $this->Flash->success($message);
                                $this->redirect(array('controller' => 'users', 'action' => 'dashboard' ));
                            }
                        } else {
                            $featuredTotal = 0;
                            if($outstanding){
                                $featuredTotal = $data['Project']['months'] * 50; //IMPORTANTE PRECIO
                            }
                            if($featuredTotal > 0){
                                $data = array(
                                    'paymentTotal' => $featuredTotal,
                                    'id' => $this->Project->id
                                );
                                $this->Session->write('paymentInfoProject', $data);
                                $url = array(
                                    'controller' => 'Projects',
                                    'action' => 'payment',
                                    'outstanding' => 1,
                                    'months' => $data['Project']['months']
                                );

                                $this->Flash->success(__('Tu proyecto ha sido guardado éxitosamente, solo queda pendiente el pago de los destacados'));
                                $this->redirect($url);
                            } else {
                                $this->Flash->success(__('Tu proyecto se guardó con éxito.'));
                                $this->redirect(array('controller' => 'users', 'action' => 'dashboard' ));
                            }
                        }
                    } else {
                        $featuredTotal = 0;
                        if($outstanding){
                            $featuredTotal = $data['Project']['months'] * 50; //IMPORTANTE PRECIO
                        }
                        if($featuredTotal > 0){
                            $data = array(
                                'paymentTotal' => $featuredTotal,
                                'id' => $this->Project->id
                            );
                            $this->Session->write('paymentInfoProject', $data);
                            $url = array(
                                'controller' => 'Projects',
                                'action' => 'payment',
                                'outstanding' => 1,
                                'months' => $data['Project']['months']
                            );

                            $this->Flash->success(__('Tu proyecto ha sido guardado éxitosamente, solo queda pendiente el pago de los destacados'));
                            $this->redirect($url);
                        } else {
                          $this->Flash->success(__('Tu proyecto se guardó, sin embargo tus archivos no se pudieron almacenar, ponte en contacto al administrador del sitio.'));
                          $this->redirect(array('controller' => 'users', 'action' => 'dashboard' ));
                        }
                    }
                }

                $featuredTotal = 0;
                if($outstanding){
                    $featuredTotal = $data['Project']['months'] * 50; //IMPORTANTE PRECIO
                }

                if($featuredTotal > 0){
                    $data = array(
                        'paymentTotal' => $featuredTotal,
                        'id' => $this->Project->id,
                        'outstanding' => 1,
                        'months' => $data['Project']['months']
                    );
                    $this->Session->write('paymentInfoProject', $data);
                    $url = array(
                        'controller' => 'Projects',
                        'action' => 'payment',
                    );

                    $this->Flash->success(__('Tu proyecto ha sido guardado éxitosamente, solo queda pendiente el pago de los destacados'));
                    $this->redirect($url);
                } else {
                    $this->Flash->success(__('Tu proyecto ha sido guardado éxitosamente'));
                    $this->redirect(array('controller' => 'users', 'action' => 'dashboard' ));
                }
            } else {
                $this->Flash->error(__('Tu proyecto no pudo ser guardado, intenta de nuevo'));
            }
        }
        $countries = $this->Country->find('list');
        $this->set('user', $this->Session->read('sessionUser'));
        $this->set(compact('countries'));
    }

    //payment
    public function payment($status = 'new'){
        $userData = $this->Auth->user();
        if($this->Session->check('paymentInfoProject')){
            $data = $this->Session->read('paymentInfoProject');
        }
        if($status == 'new'){
            $this->set(array('data' => $data));
        } else if($status == 'success') {
            $this->Project->id = $data['id'];
            $propertyData = array('payment' => 1, 'outstanding' => 1, 'months' => $data['months']);
            $this->Project->save($propertyData);

            $this->Payment->create();
            $paymentData = array(
                'user_id' => $userData['id'],
                'total' => $data['paymentTotal'],
                'status' => 1,
                'message' => 'project, payment_status: ' . $this->request->data['payment_status'] . ' mc_gross: ' . $this->request->data['mc_gross'] . ' verify_sign: ' . $this->request->data['verify_sign']
            );
            $this->Payment->save($paymentData);
            $this->set(array('status' => 'sucess'));
        } else if($status == 'cancel' || $status == 'notify') {
            $this->Project->id = $data['id'];
            $propertyData = array(
                'outstanding' => 1,
                'months' => 0
            );
            $this->Project->save($propertyData);
            $this->set(array('status' => 'cancel'));
        }
    }

    public function images($idProject) {

        $this->Project->id = $idProject;
        if (!$this->Project->exists()) {
            $this->Flash->error(__('Hay un problema con el projecto. Por favor, intentelo de nuevo'));
            return $this->redirect(array('action' => 'add'));
        }

        if ($this->request->is('post')) {
            $hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
            for ($i = 0; $i < count($_FILES['ImageUrl']['name']); $i++) {
                $nameImage = uniqid() . '_' . $_FILES['ImageUrl']['name'][$i];
                $upload = move_uploaded_file($_FILES['ImageUrl']['tmp_name'][$i], $hostImages . $nameImage);
                if ($upload) {
                    $imagesData['ProjectsImage']['rute'] = $nameImage;
                    $imagesData['ProjectsImage']['projects_id'] = $this->request->data('Image.project');
                    $imagesData['ProjectsImage']['type'] = 1;
                    $this->ProjectsImage->create();
                    if (!$this->ProjectsImage->save($imagesData)) {
                        $errorUpload = true;
                    }
                } else {
                    $errorUpload = true;
                }
            }
            if (!$errorUpload) {
                $this->Flash->success(__('Imagenes guardadas con exito, '));
                $this->redirect(['controller' => 'projects', 'action' => 'plans/' . $this->request->data('Image.project')]);
            } else {
                $this->Flash->error(__('Las imagenes no se pudieron guardar, intentelo de nuevo'));
            }
        }

        $images = $this->ProjectsImage->find('all', [
            'conditions' => [
                'ProjectsImage.projects_id' => $idProject,
                'ProjectsImage.type' => 1
            ],
            'recursive' => -1
        ]);

        $this->set('idProject', $idProject);
        $this->set('images', $images);
    }

    public function plans($idProject) {
        $this->Project->id = $idProject;
        if (!$this->Project->exists()) {
            $this->Flash->error(__('Hay un problema con el projecto. Por favor, intentelo de nuevo'));
            return $this->redirect(['action' => 'add']);
        }

        $images = $this->ProjectsImage->find('all', [
            'conditions' => [
                'ProjectsImage.projects_id' => $idProject,
                'ProjectsImage.type' => 2
            ],
            'recursive' => -1
        ]);

        $this->set('idProject', $idProject);
        $this->set('images', $images);
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Project->exists($id)) {
            $this->Flash->error(__('Hay un problema con el projecto. Por favor, intentelo de nuevo'));
            return $this->redirect(['action' => 'add']);
        }
        if ($this->request->is(array('post', 'put'))) {

            $project = $this->request->data;
            if (isset($_FILES['data']['name']['Project']['img'])) {
                $hostImages = WWW_ROOT . 'files' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
                $nameImage = uniqid() . '_' . $_FILES['data']['name']['Project']['img'];
                $upload = move_uploaded_file($_FILES['data']['tmp_name']['Project']['img'], $hostImages . $nameImage);
            }
            $project['Project']['img'] = $nameImage;

            if ($this->Project->save($project)) {
                $this->Flash->success(__('El Proyecto fue guardado.'));
                return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
            } else {
                $this->Flash->error(__('El proyecto no pudo ser guardado. Por favor, intentelo de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
            $this->request->data = $this->Project->find('first', $options);
        }
        $countries = $this->Country->find('list');
        $provinces = $this->Province->find('list');
        $cities = $this->City->find('list');
        $this->set(compact('propertyTypes', 'countries', 'categories', 'provinces', 'cities'));
    }

    public function publish($id = null) {
        if (!$this->Project->exists($id)) {
            $this->Flash->error(__('Hay un problema con el projecto. Por favor, intentelo de nuevo'));
            return $this->redirect(['action' => 'add']);
        }
    }

    public function users($id, $name) {

    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Project->id = $id;
        if (!$this->Project->exists()) {
            throw new NotFoundException(__('Invalid project'));
        }
        $this->ProjectsImage->deleteAll(['ProjectsImage.projects_id' => $id]);
        if ($this->Project->delete()) {
            $this->Flash->success(__('El proyecto ha sido eliminado.'));
        } else {
            $this->Flash->error(__('El proyecto no pudo ser sido eliminado. Por favor, intenta de nuevo.'));
        }
        return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
    }

    public function getProvinces($idCountry) {
        $this->layout = 'ajax';
        $provinces = $this->Province->find('list', [
            'conditions' => [
                'Province.countries_id' => $idCountry
            ]
        ]);
        $this->set('provinces', $provinces);
    }

    public function getCities($idProvince) {
        $this->layout = 'ajax';
        $cities = $this->City->find('list', [
            'conditions' => [
                'City.provinces_id' => $idProvince
            ]
        ]);
        $this->set('cities', $cities);
    }

    /**
     * aipanel_index method
     *
     * @return void
     */
    public function aipanel_index() {
        $this->Project->recursive = 0;
        $this->set('projects', $this->Paginator->paginate());
    }

    /**
     * aipanel_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_view($id = null) {
        if (!$this->Project->exists($id)) {
            throw new NotFoundException(__('Invalid project'));
        }
        $options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
        $this->set('project', $this->Project->find('first', $options));
    }

    /**
     * aipanel_add method
     *
     * @return void
     */
    public function aipanel_add() {
        if ($this->request->is('post')) {
            $this->Project->create();
            if ($this->Project->save($this->request->data)) {
                $this->Flash->success(__('The project has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The project could not be saved. Please, try again.'));
            }
        }
        $users = $this->Project->User->find('list');
        $countries = $this->Project->Country->find('list');
        $provinces = $this->Project->Province->find('list');
        $cities = $this->Project->City->find('list');
        $this->set(compact('users', 'countries', 'provinces', 'cities'));
    }

    /**
     * aipanel_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_edit($id = null) {
        if (!$this->Project->exists($id)) {
            throw new NotFoundException(__('Invalid project'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Project->save($this->request->data)) {
                $this->Flash->success(__('The project has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The project could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
            $this->request->data = $this->Project->find('first', $options);
        }
        $users = $this->Project->User->find('list');
        $countries = $this->Project->Country->find('list');
        $provinces = $this->Project->Province->find('list');
        $cities = $this->Project->City->find('list');
        $this->set(compact('users', 'countries', 'provinces', 'cities'));
    }

    /**
     * aipanel_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_delete($id = null) {
        $this->Project->id = $id;
        if (!$this->Project->exists()) {
            throw new NotFoundException(__('Invalid project'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Project->delete()) {
            $this->Flash->success(__('The project has been deleted.'));
        } else {
            $this->Flash->error(__('The project could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function viewAll($country = 35, $province = null, $city = null){
        $options = array(
            'conditions' => array(
                'OR' => array(
                    array('name' => 'Desarrolladora'),
                )
            ),
            'fields' => array('id')
        );
        $userType = $this->UserType->find('first', $options);

        $this->Paginator->settings = array(
            'conditions' => array(
                'OR' => array(
                    array('user_type' => $userType['UserType']['id']),
                ),
                'User.outstanding' => 1
            ),
            'fields' => array(
                'id',
                'username',
                'full_name',
                'phone',
                'description',
                'img'
            ),
            'order' => array(
                'User.id' => 'DESC'
            ),
        );

        $this->Project->recursive = 2;
        if($province != null || $city != null){
            $data = $this->request->data;
            if($province != null){
                $projects = $this->Project->find('all', array(
                    'fields' => array('name', 'email', 'phone', 'latitude', 'longitude', 'outstanding', 'countries_id', 'provinces_id', 'cities_id'),
                    'conditions' => array(
                        'Project.provinces_id' => $province,
                        'Project.countries_id' => $country,
                    )
                ));
            }
            if($city != null){
               $projects = $this->Project->find('all', array(
                    'fields' => array('name', 'email', 'phone', 'latitude', 'longitude', 'outstanding', 'countries_id', 'provinces_id', 'cities_id'),
                    'conditions' => array(
                        'Project.cities_id' => $city,
                        'Project.countries_id' => $country,
                    )
                ));
            }

            if($province != null && $city != null){
                $projects = $this->Project->find('all', array(
                    'fields' => array('name', 'email', 'phone', 'latitude', 'longitude', 'outstanding', 'countries_id', 'provinces_id', 'cities_id'),
                    'conditions' => array(
                        'Project.provinces_id' => $province,
                        'Project.cities_id' => $city,
                        'Project.countries_id' => $country,
                    )
                ));
            }
        } else {
            $projects = $this->Project->find('all', array(
                'fields' => array('name', 'email', 'phone', 'latitude', 'longitude', 'outstanding', 'countries_id', 'provinces_id', 'cities_id'),
                'conditions' => array(
                    'Project.countries_id' => $country,
                )
            ));
        }

        $this->set('projects', $projects);
        $this->set('desarrolladoras', $this->Paginator->paginate('User'));

        $this->request->data['Project']['countries_id'] = $country;
        $this->request->data['Project']['provinces_id'] = $province;
        $this->request->data['Project']['cities_id'] = $city;
        $countries = $this->Country->find('list');
        $provinces = $this->Province->find('list');
        $cities = $this->City->find('list');
        $this->set(compact('countries', 'provinces', 'cities'));
    }
}
