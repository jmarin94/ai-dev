<?php
App::uses('AppController', 'Controller');
/**
 * Provinces Controller
 *
 * @property Province $Province
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ProvincesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
        
        public $uses = array('Province', 'Country');
/**
 * aipanel_index method
 *
 * @return void
 */
	public function aipanel_index() {
		$this->Province->recursive = 0;
		$this->set('provinces', $this->Paginator->paginate());
	}

/**
 * aipanel_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function aipanel_view($id = null) {
		if (!$this->Province->exists($id)) {
			throw new NotFoundException(__('Invalid province'));
		}
		$options = array('conditions' => array('Province.' . $this->Province->primaryKey => $id));
		$this->set('province', $this->Province->find('first', $options));
	}

/**
 * aipanel_add method
 *
 * @return void
 */
	public function aipanel_add() {
		if ($this->request->is('post')) {
			$this->Province->create();
			if ($this->Province->save($this->request->data)) {
				$this->Flash->success(__('The province has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The province could not be saved. Please, try again.'));
			}
		}
		$countries = $this->Country->find('list');
		$this->set(compact('countries'));
	}

/**
 * aipanel_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function aipanel_edit($id = null) {
		if (!$this->Province->exists($id)) {
			throw new NotFoundException(__('Invalid province'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Province->save($this->request->data)) {
				$this->Flash->success(__('The province has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The province could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Province.' . $this->Province->primaryKey => $id));
			$this->request->data = $this->Province->find('first', $options);
		}
		$countries = $this->Province->Country->find('list');
		$this->set(compact('countries'));
	}

/**
 * aipanel_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function aipanel_delete($id = null) {
		$this->Province->id = $id;
		if (!$this->Province->exists()) {
			throw new NotFoundException(__('Invalid province'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Province->delete()) {
			$this->Flash->success(__('The province has been deleted.'));
		} else {
			$this->Flash->error(__('The province could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
