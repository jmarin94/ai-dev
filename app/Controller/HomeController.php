<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP HomeController
 * @author rudysibaja
 */
class HomeController extends AppController {

    public $uses = array(
        'Property',
        'PropertyType',
        'Country',
        'Province',
        'City',
        'Flag',
        'Promotion',
        'Category',
        'PropertiesImage',
        'PropertiesFlag',
        'User',
        'Advert',
        'UserType'
    );
    public $helper = array('getUrl');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index');

        $advertsHome = $this->Advert->find('all', array(
            'conditions' => array(
                'OR' => array(
                    'Advert.location' => 2,
                    'Advert.location' => 3,
                    'Advert.location' => 4,
                ),
                'Advert.start <=' => date('Y-m-d H:i:s'),
                'Advert.end >=' => date('Y-m-d H:i:s')
            )
        ));

        $this->set(array('advertsHome' => $advertsHome));
    }

    public function index($propertyType = 0, $category = 0, $country = 0, $province = 0, $city = 0, $currency = 0, $from = 0, $to = 0) {
        $userTypes = $this->UserType->find('list');
        $userTypes = array_map('strtolower',$userTypes);
        $desarrolladoras = $this->User->find('all', array(
            'conditions' => array(
                'User.user_type' => array_search('desarrolladora', $userTypes),
                'User.outstanding' => 1
            )
        ));
        $agencias = $this->User->find('all', array(
            'conditions' => array(
                'OR' => array(
                    array('User.user_type' => array_search('agente', $userTypes)),
                    array('User.user_type' => array_search('agencia', $userTypes))
                ),
                'User.outstanding' => 1
            )
        ));

        if ($country == 0){
            $ip = $this->getRealIP();
            $details = json_decode(file_get_contents("http://ipinfo.io/$ip/json"));
            switch ($details->country):
                case 'CR':
                    $country = 35;
                    break;
                case 'AR':
                    $country = 73;
                    break;
                case 'BO':
                    $country = 74;
                    break;
                case 'CO':
                    $country = 77;
                    break;
                case 'CE':
                    $country = 78;
                    break;
                case 'SV':
                    $country = 79;
                    break;
                case 'GT':
                    $country = 50;
                    break;
                case 'HN':
                    $country = 49;
                    break;
                case 'NI':
                    $country = 48;
                    break;
                case 'PA':
                    $country = 84;
                    break;
                case 'UY':
                    $country = 89;
                    break;
                default:
                    $country = 35;
            endswitch;
        }
        
        if ($this->request->is('post')) {
            $data = $this->request->data('Property');

            if ($data['property_type'] != '')
                $propertyType = $data['property_type'];
            if ($data['categories_id'] != '')
                $category = $data['categories_id'];
            if ($data['countries_id'] != '')
                $country = $data['countries_id'];
                
            if ($data['provinces_id'] != '')
                $province = $data['provinces_id'];
            if ($data['cities_id'] != '')
                $city = $data['cities_id'];
            if($data['currency'] != ''){
                if($data['currency'] == 1){
                    $currency = 1;
                    if($data['from_price'] != ''){
                        $from = $data['from_price'];
                    }
                    if($data['to_price'] != ''){
                        $to = $data['to_price'];
                    }
                } else if($data['currency'] == 2) {
                    $currency = 2;
                    if($data['from_price'] != ''){
                        $from = $data['from_price'];
                    }
                    if($data['to_price'] != ''){
                        $to = $data['to_price'];
                    }
                }
            }

            $searchUrl = $propertyType . '/' . $category . '/' . $country . '/' . $province . '/' . $city . '/' . $currency . '/' . $from . '/' . $to;
            return $this->redirect(['controller' => 'home', 'action' => 'index/' . $searchUrl]);
        }

        $conditions = array();
        if ($propertyType != 0){
            $conditions['Property.property_type'] = $propertyType;
            $this->request->data['property_type'] = $propertyType;
        }
        if ($category != 0){
            $conditions['Property.categories_id'] = $category;
            $this->request->data['categories_id'] = $category;
        }
        if ($country != 0){
            $conditions['Property.countries_id'] = $country;
            $this->request->data['countries_id'] = $country;
        }
        if ($province != 0){
            $conditions['Property.provinces_id'] = $province;
            $this->request->data['provinces_id'] = $province;
            $provinces = $this->Province->find('list');
            $this->set(compact('provinces'));
        }
        if ($city != 0){
            $conditions['Property.cities_id'] = $city;
            $this->request->data['cities_id'] = $city;
            $cities = $this->City->find('list');
            $this->set(compact('cities'));
        }
        if ($currency != 0){
            if($currency == 1){
                $this->request->data['currency'] = 1;
                if ($from != 0){
                    if($propertyType == 4){
                        $conditions['Property.c_s_price >='] = $from;
                    } else {
                        $conditions['Property.c_a_price >='] = $from;
                    }
                    $this->request->data['from_price'] = $from;
                }
                if ($to != 0){
                    if($propertyType == 4){
                        $conditions['Property.c_s_price <='] = $to;
                    } else {
                        $conditions['Property.c_a_price <='] = $to;
                    }
                    $this->request->data['to_price'] = $to;
                }
            }
            if($currency == 2){
                $this->request->data['currency'] = 2;
                if ($from != 0){
                    if($propertyType == 4){
                        $conditions['Property.d_s_price >='] = $from;
                    } else {
                        $conditions['Property.d_a_price >='] = $from;
                    }
                    $this->request->data['from_price'] = $from;
                }
                if ($to != 0){
                    if($propertyType == 4){
                        $conditions['Property.d_s_price <='] = $to;
                    } else {
                        $conditions['Property.d_a_price <='] = $to;
                    }
                    $this->request->data['to_price'] = $to;
                }
            }
        }


        if(count($conditions) > 0 ){
            $this->Paginator->settings = array(
                'limit' => 40,
                'page' => 1,
                'order' => array(
                    'Property.outstanding' => 'DESC',
                    'Property.created' => 'DESC'
                ),
                'conditions' => $conditions
            );
        } else {
            $this->Paginator->settings = array(
                'limit' => 40,
                'page' => 1,
                'order' => array(
                    'Property.outstanding' => 'DESC',
                    'Property.created' => 'DESC'
                )
            );
        }

        $this->set(array('desarrolladoras' => $desarrolladoras, 'agencias' =>  $agencias, 'propiedades' => $this->Paginator->paginate('Property')));
    }
    
    private function getRealIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];

        return $_SERVER['REMOTE_ADDR'];
    }
}
