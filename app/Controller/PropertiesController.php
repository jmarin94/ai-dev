<?php

App::uses('AppController', 'Controller');

/**
 * Properties Controller
 *
 * @property Property $Property
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class PropertiesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    public $uses = array(
        'Property',
        'PropertyType',
        'Country',
        'Province',
        'City',
        'Flag',
        'Promotion',
        'Category',
        'PropertiesImage',
        'PropertiesFlag',
        'Advert',
        'User',
        'Payment'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('viewAll', 'view', 'getProvinces', 'getCities');
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Property->recursive = 0;
        $this->set('properties', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Property->exists($id)) {
            throw new NotFoundException(__('Invalid property'));
        }
        $options = array('conditions' => array('Property.' . $this->Property->primaryKey => $id));
        $property = $this->Property->find('first', $options);
        $user = $this->User->find('first', array(
           'conditions' => array(
               'id' => $property['Property']['users_id']
            )
        ));
        $this->set(array(
            'property' => $property,
            'user' => $user
        ));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $userData = $this->Auth->user();
        if ($this->checkRole($userData['user_type'], 'desarrolladora')) {
            return $this->redirect(['controller' => 'projects', 'action' => 'add']);
        }

        if ($this->request->is('post')) {
            $this->Property->create();
            $data = $this->request->data;

            $paymentData['total'] = $data['Property']['payment_total'];
            $paymentData['user_id'] = $data['Property']['users_id'];
            $this->Payment->save($paymentData);

            if(isset($data['Property']['featured']) && $data['Property']['featured'] == 'on'){
                $data['Property']['outstanding'] = 2;
            } else if(isset($data['Property']['super_offer']) && $data['Property']['super_offer'] == 'on'){
                $data['Property']['outstanding'] = 1;
            } else if(isset($data['Property']['free_flags']) && $data['Property']['free_flags'] == 'on') {
                $data['Property']['outstanding'] = 0;
            }
            $data['Property']['status'] = 1;
            $data['Property']['payment'] = 0;
            if ($this->Property->save($data)) {
                if ($_FILES) {
                    $propertyId = $this->Property->getLastInsertID();
                    $hostImages = WWW_ROOT . 'img/ai/files/properties/' . $propertyId;
                    if(mkdir($hostImages, 0777, true)){
                        $files = count($_FILES['input-file-properties']['name']);
                        $errors = array();
                        for ($i = 0; $i < $files; $i++){
                            $nameImage = uniqid() . '_' . $_FILES['input-file-properties']['name'][$i];

                            $this->PropertiesImage->create();
                            $data['properties_id'] = $propertyId;
                            $data['rute'] = 'ai/files/properties/' .  $propertyId . DS . $nameImage;
                            $this->PropertiesImage->save($data);
                            $upload = move_uploaded_file($_FILES['input-file-properties']['tmp_name'][$i], $hostImages . DS . $nameImage);
                            if(!$upload){
                                $this->PropertiesImage->delete();
                                $errors[] =  $_FILES['input-file-properties']['name'][$i];
                            }
                        }
                        $flagsTotal = 0;
                        if(isset($data['PropertiesFlag'])){
                            foreach($data['PropertiesFlag'] as $key => $flags){
                                $this->PropertiesFlag->create();
                                $flag['flags_id'] = $key;
                                $flag['properties_id'] = $propertyId;
                                $this->PropertiesFlag->save($flag);
                                $flagsTotal += 10; //IMPORTANTE PRECIO
                            }
                        }

                        if(count($errors) > 0){
                            $message = "Tu propiedad se guardó, pero hubo un error insertando las imágenes: ";
                            foreach ($errors as $error){
                                $message .= $error . " | ";
                            }
                            $this->Flash->success($message);
                        } else {
                            $featuredTotal = 0;
                            if($data['Property']['outstanding'] > 0){
                                $featuredTotal = $data['Property']['months'] * 10; //IMPORTANTE PRECIO
                            }

                            if($featuredTotal > 0 || $flagsTotal > 0){
                                $url = Router::url(array(
                                    'controller' => 'Properties',
                                    'action' => 'payment',
                                    array(
                                        'flagsTotal' => $flagsTotal,
                                        'featuredTotal' => $featuredTotal,
                                        'paymentTotal' => $data['Property']['payment_total'],
                                        'outstanding' => $data['Property']['outstanding'],
                                        'months' => $data['Property']['months'],
                                        'flags' => $data['PropertiesFlag'],
                                        'id' => $this->Property->id
                                    )
                                ));

                                $this->Flash->success(__('Tu propiedad se guardó con éxito, solo queda pendiente el pago de los destacados, si no cancelas, tu propiedad quedará publicada sin los mismos.'));
                                return $this->redirect($url);
                            } else {
                                $this->Flash->success(__('Tu propiedad se guardó con éxito.'));
                                return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
                            }

                        }
                    } else {
                        $featuredTotal = 0;
                        if($data['Property']['outstanding'] > 0){
                            $featuredTotal = $data['Property']['months'] * 10; //IMPORTANTE PRECIO
                        }

                        if($featuredTotal > 0 || $flagsTotal > 0){
                            $data = array(
                                'flagsTotal' => $flagsTotal,
                                'featuredTotal' => $featuredTotal,
                                'paymentTotal' => $data['Property']['payment_total'],
                                'outstanding' => $data['Property']['outstanding'],
                                'months' => $data['Property']['months'],
                                'flags' => $data['PropertiesFlag'],
                                'id' => $this->Property->id
                            );
                            $this->Session->write('paymentInfoProject', $data);
                            $url = array(
                                'controller' => 'Properties',
                                'action' => 'payment',
                            );

                            $this->Flash->success(__('Tu propiedad se guardó con éxito, sin embargo tus fotos no se pudieron almacenar, ponte en contacto al administrador del sitio. Por otro lado queda pendiente el pago de los destacados, si no cancelas, tu propiedad quedará publicada sin los mismos.'));
                            return $this->redirect($url);
                        } else {
                            $this->Flash->success(__('Tu propiedad se guardó, sin embargo tus fotos no se pudieron almacenar, ponte en contacto al administrador del sitio.'));
                            return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
                        }
                    }
                }
                $featuredTotal = 0;
                if($data['Property']['outstanding'] > 0){
                    $featuredTotal = $data['Property']['months'] * 10; //IMPORTANTE PRECIO
                }

                if($featuredTotal > 0 || $flagsTotal > 0){
                    $data = array(
                        'flagsTotal' => $flagsTotal,
                        'featuredTotal' => $featuredTotal,
                        'paymentTotal' => $data['Property']['payment_total'],
                        'outstanding' => $data['Property']['outstanding'],
                        'months' => $data['Property']['months'],
                        'flags' => $data['PropertiesFlag'],
                        'id' => $this->Property->id
                    );
                    $this->Session->write('paymentInfo', $data);
                    $url = array(
                        'controller' => 'Properties',
                        'action' => 'payment',
                    );

                    $this->Flash->success(__('Tu propiedad se guardó con éxito, solo queda pendiente el pago de los destacados, si no cancelas, tu propiedad quedará publicada sin los mismos.'));
                    return $this->redirect($url);
                } else {
                    $this->Flash->success(__('Tu propiedad se guardó con éxito.'));
                    return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
                }
            } else {
                $this->Flash->error(__('Tu propiedad no pudo ser guardada, intenta de nuevo'));
            }
        }
        $this->set('flags', $this->Flag->find('all'));
    }

    //payment
    public function payment($status = 'new'){
        $userData = $this->Auth->user();
        if($this->Session->check('paymentInfo')){
            $data = $this->Session->read('paymentInfo');
        }
        if($status == 'new'){
            $this->Property->id = $data['id'];
            $propertyData = array(
                'payment' => 0,
                'outstanding' => 0,
                'months' => 0
            );
            $this->Property->save($propertyData);
            $this->PropertiesFlag->deleteAll(array('properties_id' => $data['id']), false);
            
            $this->set(array('data' => $data));
        } else if($status == 'success') {
            $this->Property->id = $data['id'];
            $propertyData = array(
                'payment' => 1,
                'outstanding' => $data['outstanding'],
                'months' => $data['months']
            );
            $this->Property->save($propertyData);
            
            if(isset($data['flags'])){
                foreach($data['flags'] as $key => $flags){
                    $this->PropertiesFlag->create();
                    $flag['flags_id'] = $key;
                    $flag['properties_id'] = $data['id'];
                    $this->PropertiesFlag->save($flag);
                    $flagsTotal += 10; //IMPORTANTE PRECIO
                }
            }

            $this->Payment->create();
            $paymentData = array(
                'user_id' => $userData['id'],
                'total' => $data['paymentTotal'],
                'status' => 1,
                'message' => 'payment_status: ' . $this->request->data['payment_status'] . ' mc_gross: ' . $this->request->data['mc_gross'] . ' verify_sign: ' . $this->request->data['verify_sign']
            );
            $this->Payment->save($paymentData);
            $this->set(array('status' => 'sucess'));
        } else if($status == 'cancel' || $status == 'notify') {
            $this->Property->id = $data['id'];
            $propertyData = array(
                'payment' => 0,
                'outstanding' => 0,
                'months' => 0
            );
            $this->Property->save($propertyData);
            $this->PropertiesFlag->deleteAll(array('properties_id' => $data['id']), false);
            $this->set(array('status' => 'cancel'));
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {

        if (!$this->Property->exists($id)) {
            $this->Flash->error(__('La propiedad que trata de acceder no existe'));
            return $this->redirect(array('action' => 'add'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Property->save($this->request->data)) {
                $this->Flash->success(__('La propiedad fue guardada.'));
                return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
            } else {
                $this->Flash->error(__('La Propiedad no pudo ser guardada, por favor intentelo de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Property.' . $this->Property->primaryKey => $id));
            $this->request->data = $this->Property->find('first', $options);
        }
        $propertyTypes = $this->Property->PropertyType->find('list');
        $countries = $this->Country->find('list');
        $provinces = $this->Province->find('list');
        $categories = $this->Category->find('list');
        $cities = $this->City->find('list');
        $this->set(compact('propertyTypes', 'countries', 'categories', 'provinces', 'cities'));
    }

    public function flags($idPropertie) {
        $flags = $this->Flag->find('all', [
            'recursive' => -1
        ]);
        $this->set('flags', $flags);
        $this->set('idPropertie', $idPropertie);
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Property->id = $id;
        if (!$this->Property->exists()) {
            throw new NotFoundException(__('Invalid property'));
        }
        $this->PropertiesImage->deleteAll(['PropertiesImage.properties_id' => $id]);
        $this->PropertiesFlag->deleteAll(['PropertiesFlag.properties_id' => $id]);
        if ($this->Property->delete()) {
            $this->Flash->success(__('La propiedad fue eliminada.'));
        } else {
            $this->Flash->error(__('La propiedad no fue eliminada. Por favor, intenta de nuevo.'));
        }
        return $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
    }

    /**
     * aipanel_index method
     *
     * @return void
     */
    public function aipanel_index() {
        $this->Property->recursive = 1;
        $this->set('properties', $this->Paginator->paginate());
    }

    /**
     * aipanel_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_view($id = null) {
        if (!$this->Property->exists($id)) {
            throw new NotFoundException(__('Invalid property'));
        }
        $options = array('conditions' => array('Property.' . $this->Property->primaryKey => $id));
        $this->set('property', $this->Property->find('first', $options));
    }

    /**
     * aipanel_add method
     *
     * @return void
     */
    public function aipanel_add() {
        if ($this->request->is('post')) {
            $this->Property->create();
            if ($this->Property->save($this->request->data)) {
                $this->Flash->success(__('The property has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The property could not be saved. Please, try again.'));
            }
        }
        $typeProperties = $this->Property->PropertyType->find('list');
        $countries = $this->Property->Country->find('list');
        $provinces = $this->Property->Province->find('list');
        $cities = $this->Property->City->find('list');
        $flags = $this->Property->Flag->find('list');
        $promotions = $this->Property->Promotion->find('list');
        $this->set(compact('typeProperties', 'countries', 'provinces', 'cities', 'flags', 'promotions'));
    }

    /**
     * aipanel_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_edit($id = null) {
        if (!$this->Property->exists($id)) {
            throw new NotFoundException(__('Invalid property'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Property->save($this->request->data)) {
                $this->Flash->success(__('The property has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The property could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Property.' . $this->Property->primaryKey => $id));
            $this->request->data = $this->Property->find('first', $options);
        }
        $typeProperties = $this->Property->PropertyType->find('list');
        $countries = $this->Property->Country->find('list');
        $provinces = $this->Property->Province->find('list');
        $cities = $this->Property->City->find('list');
        $flags = $this->Property->Flag->find('list');
        $promotions = $this->Property->Promotion->find('list');
        $this->set(compact('typeProperties', 'countries', 'provinces', 'cities', 'flags', 'promotions'));
    }

    /**
     * aipanel_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function aipanel_delete($id = null) {
        $this->Property->id = $id;
        if (!$this->Property->exists()) {
            throw new NotFoundException(__('Invalid property'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Property->delete()) {
            $this->Flash->success(__('The property has been deleted.'));
        } else {
            $this->Flash->error(__('The property could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function viewAll() {
        $this->Paginator->settings = array(
            'order' => array(
                'Property.outstanding' => 'DESC',
                'Property.created' => 'DESC'
            )
        );
        $this->set('properties', $this->Paginator->paginate());
    }

     public function getProvinces($idCountry) {
        $this->layout = 'ajax';
        $provinces = $this->Province->find('list', [
            'conditions' => [
                'Province.countries_id' => $idCountry
            ]
        ]);
        $this->set('provinces', $provinces);
    }

    public function getCities($idProvince) {
        $this->layout = 'ajax';
        $cities = $this->City->find('list', [
            'conditions' => [
                'City.provinces_id' => $idProvince
            ]
        ]);
        $this->set('cities', $cities);
    }

}
