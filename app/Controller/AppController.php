<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Flash',
        'Session',
        'Paginator',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'users',
                'action' => 'dashboard',
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'dashboard'
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            ),
            'authError' => 'Debes iniciar sesión para realizar esta funcionalidad.',
        )
    );
    public $uses = array('UserType', 'Country', 'Province',  'City', 'Category', 'PropertyType', 'Property', 'Project', 'User', 'Advert');
    public $loggedUser = '';
    
    //variables para paguelofacil
    private $userKey = 'E830366BDED32B268F84DE58FE6EB02667A0552B1B0A31781FA5FBD403EFFBD4';
    private $pgfUrl = 'https://secure.paguelofacil.com/LinkDeamon.cfm';
    private $description = "Pago para publicar propiedades en www.americainmobiliaria.com";
    
    
    /**
     * funcion que carga antes de todos los controladores
     * @return void 
     */
    public function beforeFilter() {
        $this->Auth->allow('countProperties', 'checkRole');
        $this->loggedUser = $this->Auth->user();
        if (isset($this->params['aipanel']) && $this->checkRole($this->loggedUser['user_type'], 'administrador')) {
            $this->layout = 'aipanel';
        } else if (isset($this->params['aipanel'])) {
            $action = split('_', $this->action);
            $url = Router::url('/', true) . DS . $this->name . DS . $action[1];
            $this->redirect($url);
        }
        $propertyTypes = $this->PropertyType->find('list');
        $countries = $this->Country->find('list');
        $categories = $this->Category->find('list');
        $this->set(compact('propertyTypes', 'countries', 'categories'));
        $this->set('loggedUser', $this->loggedUser);
        
        //publicidad sidebar
        $advertsSidebar = $this->Advert->find('all', array(
            'conditions' => array(
                'Advert.location' => 1,
                'Advert.start <=' => date('Y-m-d H:i:s'),
                'Advert.end >=' => date('Y-m-d H:i:s')
            )
        ));
         $this->set('advertsSidebar', $advertsSidebar);
        
        $advertsBottom = $this->Advert->find('all', array(
            'conditions' => array(
                'Advert.location' => 5,
                'Advert.start <=' => date('Y-m-d H:i:s'),
                'Advert.end >=' => date('Y-m-d H:i:s')
            )
        ));
         $this->set('advertsBottom', $advertsBottom);
    }

    public function checkRole($userType, $role){
        $userTypes = array_map('strtolower', $this->UserType->find('list'));
        return $userType == array_search(strtolower($role), $userTypes);
    }
    
    public function getRole($userType){
        $options = array(
            'conditions' => array('id' => $userType), 
            'fields' => array('name')
        );
        $first = $this->UserType->find('first', $options);
        return $first['UserType']['name'];
    }
    
    public function countProperties($userId){
        $userType = $this->User->find('first', array('conditions' => array('id' => $userId), 'fields' => array('user_type')));
        $userType = $userType['User']['user_type'];

        $isDesarrolladora = $this->checkRole($userType, 'desarrolladora');
        
        $options = array('conditions' => array('users_id' => $userId));
        if($isDesarrolladora){
           $count = $this->Project->find('count', $options);
        } else {
           $count = $this->Property->find('count', $options);
        }
        
        return $count;
    }
    
    public function getPagueloFacilUrl($payId, $payTotal){
        return $this->pgfUrl . "?CCLW=" .$this->userKey . "&CMTN=" . $payTotal . "&CDSC=" . $this->description . "&payid=" . $payId;
    }
}
