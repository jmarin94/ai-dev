<?php
App::uses('AppController', 'Controller');
/**
 * Adverts Controller
 *
 * @property Advert $Advert
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class AdvertsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * aipanel_index method
 *
 * @return void
 */
	public function aipanel_index() {
		$this->Advert->recursive = 0;
		$this->set('adverts', $this->Paginator->paginate());
	}

/**
 * aipanel_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function aipanel_view($id = null) {
		if (!$this->Advert->exists($id)) {
			throw new NotFoundException(__('Invalid advert'));
		}
		$options = array('conditions' => array('Advert.' . $this->Advert->primaryKey => $id));
		$this->set('advert', $this->Advert->find('first', $options));
	}

/**
 * aipanel_add method
 *
 * @return void
 */
	public function aipanel_add() {
		if ($this->request->is('post')) {
                    $hostImages = WWW_ROOT . 'img/ai/files' . DIRECTORY_SEPARATOR . 'adverts' . DIRECTORY_SEPARATOR;
                    
                    $data = $this->request->data;
                    if(isset($_FILES['data']['name']['Advert']['file']) && $_FILES['data']['name']['Advert']['file'] != ""){
                        $nameImage              = uniqid().'_'.$_FILES['data']['name']['Advert']['file'];
                        $upload                 = move_uploaded_file($_FILES['data']['tmp_name']['Advert']['file'], $hostImages . $nameImage);
                        $data['Advert']['file'] = $nameImage;
                    } else {
                        $data['Advert']['file'] = null;
                    }
                    $start = new DateTime($data['Advert']['start']);
                    $end = new DateTime($data['Advert']['end']);
                    
                    $data['Advert']['start'] = $start->format('Y-m-d');
                    $data['Advert']['end'] = $end->format('Y-m-d');
                    
                    $this->Advert->create();
                    if ($this->Advert->save($data)) {
                            $this->Flash->success(__('The advert has been saved.'));
                            return $this->redirect(array('action' => 'index'));
                    } else {
                            $this->Flash->error(__('The advert could not be saved. Please, try again.'));
                    }
                   
		}
	}

/**
 * aipanel_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function aipanel_edit($id = null) {
		if (!$this->Advert->exists($id)) {
			throw new NotFoundException(__('Invalid advert'));
		}
                
		if ($this->request->is('post') || $this->request->is('put')) {
                    $hostImages = WWW_ROOT . 'img/ai/files' . DIRECTORY_SEPARATOR . 'adverts' . DIRECTORY_SEPARATOR;
                    
                    $data = $this->request->data;
                    if(isset($_FILES['data']['name']['Advert']['file']) && $_FILES['data']['name']['Advert']['file'] != ""){
                        $nameImage              = uniqid().'_'.$_FILES['data']['name']['Advert']['file'];
                        $upload                 = move_uploaded_file($_FILES['data']['tmp_name']['Advert']['file'], $hostImages . $nameImage);
                        $data['Advert']['file'] = $nameImage;
                        $data['Advert']['code'] = null;
                    } else if($data['Advert']['code'] != "") {
                        $data['Advert']['file'] = null;
                    }
                    $start = new DateTime($data['Advert']['start']);
                    $end = new DateTime($data['Advert']['end']);
                    
                    $data['Advert']['start'] = $start->format('Y-m-d');
                    $data['Advert']['end'] = $end->format('Y-m-d');

                    if ($this->Advert->save($data)) {
                            $this->Flash->success(__('The advert has been saved.'));
                            return $this->redirect(array('action' => 'index'));
                    } else {
                            $this->Flash->error(__('The advert could not be saved. Please, try again.'));
                    }
                   
		} else {
			$options = array('conditions' => array('Advert.' . $this->Advert->primaryKey => $id));
			$this->request->data = $this->Advert->find('first', $options);
		}
	}

/**
 * aipanel_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function aipanel_delete($id = null) {
		$this->Advert->id = $id;
		if (!$this->Advert->exists()) {
			throw new NotFoundException(__('Invalid advert'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Advert->delete()) {
			$this->Flash->success(__('The advert has been deleted.'));
		} else {
			$this->Flash->error(__('The advert could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
