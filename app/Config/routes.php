<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'Home', 'action' => 'index'));
	Router::connect('/registro', array('controller' => 'users', 'action' => 'add'));
	Router::connect('/iniciar-sesion', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/recuperar-contrasena', array('controller' => 'users', 'action' => 'forgot'));
        Router::connect('/mi-cuenta', array('controller' => 'users', 'action' => 'dashboard'));
        Router::connect('/contactenos', array('controller' => 'pages', 'action' => 'contact'));
        Router::connect('/quienes-somos', array('controller' => 'pages', 'action' => 'aboutUs'));
        Router::connect('/ver-todas-las-propiedades', array('controller' => 'properties', 'action' => 'viewAll'));
        Router::connect('/proyectos-y-desarrolladoras', array('controller' => 'projects', 'action' => 'viewAll'));
        Router::connect('/agentes-y-agencias', array('controller' => 'users', 'action' => 'agencies'));
        Router::connect('/bancos', array('controller' => 'users', 'action' => 'banks'));

        Router::connect('/ver/:category/:id-:slug', array('controller' => 'users', 'action' => 'view'), array('pass' => array('id', 'slug'), 'id' => '[0-9]+'));
        Router::connect('/detalle/propiedad/:id-:slug', array('controller' => 'properties', 'action' => 'view'), array('pass' => array('id', 'slug'), 'id' => '[0-9]+'));
        Router::connect('/detalle/proyecto/:id-:slug', array('controller' => 'projects', 'action' => 'view'), array('pass' => array('id', 'slug'), 'id' => '[0-9]+'));

        
        
//        Router::connect('/propiedades', array('controller' => 'properties', 'action' => 'viewAll'));
//        Router::connect('/agentes-y-agencias', array('controller' => 'pages', 'action' => 'agencies'));
//        Router::connect('/bancos', array('controller' => 'pages', 'action' => 'banks'));

/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
