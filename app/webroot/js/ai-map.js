var latitude = 9.921;
var longitud = -84.088;
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
} 

function showPosition(position) {
    latitude = position.coords.latitude;
    longitud = position.coords.longitude;
    console.log(longitud);
}

var map = new google.maps.Map(document.getElementById('ai-map-canvas'), {
    zoom: 10,
    center: new google.maps.LatLng(latitude, longitud),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var myMarker = new google.maps.Marker({
    position: new google.maps.LatLng(latitude, longitud),
    draggable: true
});

google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    //document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>';
    $('.map-result').val(evt.latLng.lat().toFixed(3) + ', ' + evt.latLng.lng().toFixed(3));
});

google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
    //document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';
});

map.setCenter(myMarker.position);
myMarker.setMap(map);