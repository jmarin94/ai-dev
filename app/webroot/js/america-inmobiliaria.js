jQuery(document).ready(function(){
    if (jQuery(window).width() > 992){
        //for desarrolladoras and agencias images make it square
        jQuery('.properties-table').nailthumb({method: 'resize',width:188,height:188,fitDirection:'center center'});
        jQuery('.properties-demo').nailthumb({method: 'resize',width:181,height:181,fitDirection:'center center'});
        jQuery('.desarrolladoras').nailthumb({method: 'resize',width:80,height:80,fitDirection:'center center'});
        jQuery('.property-owner').nailthumb({method: 'resize',width:250,height:250,fitDirection:'center center'});
        jQuery('.agencies-list').nailthumb({method: 'resize',width:150,height:150,fitDirection:'center center'});
        jQuery('.property-box-image img').nailthumb({method: 'resize',width:270,height:270,fitDirection:'center center'});
    }
    jQuery('.agencies').nailthumb({method: 'resize',width:70,height:70,fitDirection:'center center'});
    if (jQuery(window).width() < 400){
        //for desarrolladoras and agencias images make it square
        jQuery('.properties-table').nailthumb({method: 'crop',width:134,height:90,fitDirection:'center center'});
    }
    if (jQuery(window).width() > 400 && jQuery(window).width() < 600){
        //for desarrolladoras and agencias images make it square
        jQuery('.properties-table').nailthumb({method: 'crop',width:256,height:192,fitDirection:'center center'});
    }
    window.addEventListener("orientationchange", function() {
        if (jQuery(window).width() < 400){
            //for desarrolladoras and agencias images make it square
            jQuery('.properties-table').nailthumb({method: 'crop',width:134,height:90,fitDirection:'center center'});
        }
        if (jQuery(window).width() > 400 && jQuery(window).width() < 600){
            //for desarrolladoras and agencias images make it square
            jQuery('.properties-table').nailthumb({method: 'crop',width:256,height:192,fitDirection:'center center'});
        }
      }, false);

   //session messages, close
   jQuery('.close').click(function(){
        jQuery('.alert-dismissible').fadeOut();
   });

   /*
    *register
    */
   jQuery('.select-typeUser').click(function(){
        var idCheck = $(this).attr('id');
        jQuery('#UserUserType').val($(this).attr('value'));
        jQuery('.select-typeUser').each(function(){
            if(jQuery(this).attr('id') != idCheck){
                jQuery(this).attr('checked', false)
            }
        });
    });
    //validate register
    jQuery('#UserAddForm').submit(function(){
        selected = false;
        jQuery('.select-typeUser').each(function(){
            if(jQuery(this).is(":checked")){
                selected = true;
            }
        });
        if(!selected){
            new Messi('Por favor selecciona el tipo de cliente que eres.', {title: '¡Alerta!', modal: true});
            return false;
        }
    });


    //account data
    jQuery("#img-profile").fileinput({
        autoReplace: true,
        maxFileCount: 5,
        browseLabel: "Explorar",
        removeLabel: "Remover foto",
        uploadLabel: "Subir"
    });

    //don't allow alphabetic characters on input[type=number]
    jQuery("input[type=number]").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && e.ctrlKey === true) ||
            (e.keyCode == 67 && e.ctrlKey === true) ||
            (e.keyCode == 88 && e.ctrlKey === true) ||
            (e.keyCode >= 35 && e.keyCode <= 39))  return;
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) e.preventDefault();
    });
    $(".page-wrapper").scroll(function() {
        var height = $('.page-wrapper').scrollTop();
        console.log('in');
        if(height  > 160) {
            jQuery('.footer-bar').css("opacity","1");
            jQuery('.scroll-top').click(function(){
                jQuery(".page-wrapper").animate({scrollTop:0}, '500');
            });
        } else {
            jQuery('.footer-bar').css("opacity","0");
        }
    });
});
