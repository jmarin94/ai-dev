<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script>tinymce.init({selector: 'textarea'});</script>
<div class="properties form">
    <?php echo $this->Form->create('Information'); ?>
    <fieldset>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('page', array('options' => $pages));
        echo $this->Form->input('text');
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>