<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script>tinymce.init({selector: '#InformationText'});</script>

<div class="container-fluid">
    <div class="row">
        <div class="properties form col-md-6">
            <?php echo $this->Form->create('Information'); ?>
            <fieldset>
                <legend><?= __("¿Quiénes somos?") ?></legend>
                <?php
                echo $this->Form->input('id');
                echo $this->Form->input('page', array('value' => 1, 'type' => 'hidden'));
                echo $this->Form->input('text', array('label' => false));
                ?>
                <br>
                <?php echo $this->Form->submit(__("Guardar"), array('class' => 'btn')); ?>
            </fieldset>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>