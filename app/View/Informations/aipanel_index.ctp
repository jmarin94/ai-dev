<div class="properties index">
    <h2><?php echo __('Propiedades'); ?></h2>
    <table class="table table-simple">
        <thead>
            <tr>
                <th class="min-width nowrap"><?php echo $this->Paginator->sort('id'); ?></th>
                <th class="min-width nowrap">Photo</th>
                <th><?php echo $this->Paginator->sort('title'); ?></th>
                <th>Type</th>
                <th><?php echo $this->Paginator->sort('created'); ?></th>
                <th class="min-width nowrap"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($properties as $property): ?>
                <tr>
                    <td class="semi-bold"><?= $property['Property']['id'] ?></td>
                    <td>
                        <?= $this->Html->image($property['PropertiesImages'][0]['rute'], array('width' => 40)); ?>
                    </td>
                    <td>
                        <a href="<?= $this->Html->url(array('controller' => 'properties', 'action' => 'edit', $property['Property']['id'])) ?>"><?= $property['Property']['title'] ?></a>
                    </td>
                    <td><?= $property['Property']['id'] ?></td>
                    <?php $date = new DateTime($property['Property']['created']); ?>
                    <td><?= $date->format('d-m-Y') ?></td>
                    <td class="min-width nowrap">
                        <a href="<?= $this->Html->url(array('controller' => 'properties', 'action' => 'edit', $property['Property']['id'])) ?>" class="btn-standard"><?=__("Editar")?></a>
                        <?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $property['Property']['id']), array('confirm' => __('¿Estás seguro de que quieres eliminar la propiedad: %s?', $property['Property']['title']), 'class' => 'btn-alert')); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('INSERTAR NUEVA PROPIEDAD'), array('action' => 'add')); ?></li>
        <li><?php echo $this->Html->link(__('LISTA DE TIPOS DE PROPIEDAD'), array('controller' => 'property_type', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('PAÍSES'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('PROVINCIAS'), array('controller' => 'provinces', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('CIUDADES'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('ETIQUETAS'), array('controller' => 'flags', 'action' => 'index')); ?> </li>
    </ul>
</div>
