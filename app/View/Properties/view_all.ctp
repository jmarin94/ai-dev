<?php 
$featured = 0;
$super_offer = 0;
foreach($properties as $property){
    if($property['Property']['outstanding'] == 2): 
        $featured++;
    elseif ($property['Property']['outstanding'] == 1):
        $super_offer++;
    endif; 
}
?>
<div class="container">
    <div class="content">
        <div class="container">
            <div class="row">
                <ul class="isotope-filter properties-filter">
                    <li class="selected"><a href="properties-isotope.html#" data-filter="*"><span><?=__("Todas las propiedades")?></span></a></li>
                    <?php if($featured > 0): ?>
                        <li><a href="properties-isotope.html#" data-filter=".property-featured"><span><?=__("Propiedades destacadas")?></span></a></li>
                    <?php endif; ?> 
                    <?php if($super_offer > 0): ?>
                        <li><a href="properties-isotope.html#" data-filter=".property-super-offer"><span><?=__("Super ofertas")?></span></a></li>
                    <?php endif; ?>
                </ul>

                <div class="properties-isotope">
                    <?php foreach($properties as $property): 
                        $url = Router::url('/').'detalle/propiedad/'.$property['Property']['id'].'-' .strtolower(Inflector::slug($property['Property']['title'], '-'));
                        $outstanding = "";
                        if(isset($property['Property']['outstanding'])): 
                            $date = new DateTime($property['Property']['created']);
                            $monts = $property['Property']['months'];
                            $expiration = strtotime('+' . $monts . ' month', strtotime($date->format('d-m-Y')));
                            $expiration = date ('d-m-Y' , $expiration);
                            
                            if(strtotime($expiration) > strtotime(date('d-m-Y'))):
                                if($property['Property']['outstanding'] == 2): 
                                    $outstanding = "property-featured";
                                elseif ($property['Property']['outstanding'] == 1):
                                    $outstanding = "property-super-offer";
                                endif; 
                            endif; 
                        endif; 
                    ?>
                        <div class="col-sm-3 item <?=$outstanding?>">
                            <div class="property-box">
                                <div class="property-box-image">
                                    <a href="<?=$url;?>">
                                        <div>
                                            <?php if($outstanding != "" && $outstanding == "property-featured"): ?>
                                                <div class="featured-icon"></div>
                                            <?php elseif($outstanding != "" && $outstanding == "property-super-offer"): ?>
                                                <div class="superoffer-icon" ></div>
                                            <?php endif; ?>
                                            <?php if(isset($property['PropertiesImages'][0]['rute'])): ?>
                                                <?= $this->Html->image($property['PropertiesImages'][0]['rute']) ?>
                                            <?php else: ?>
                                                <?= $this->Html->image("ai/no-image.jpg" , array("alt" => __("Imagen no disponible"))); ?>
                                            <?php endif; ?>
                                        </div>

                                        <span class="property-box-excerpt">
                                            <?php if(isset($property['Flag']) && count($property['Flag']) > 0): ?>
                                                <ul class="properties-flags-home">
                                                    <?php foreach($property['Flag'] as $flag): ?>
                                                        <li><?= $flag['name']; ?></li>
                                                    <?php endforeach;; ?>
                                                </ul>
                                            <?php else: ?>
                                                <?= $property['Property']['description'] ?>
                                            <?php endif; ?>
                                        </span>
                                    </a>
                                </div><!-- /.property-image -->

                                <div class="property-box-content">
                                    <div class="property-box-meta">
                                        <div class="property-box-meta-item">
                                            <span><?=__("Habitaciones")?></span>
                                            <strong><?=$property['Property']['rooms']?></strong>
                                        </div><!-- /.property-box-meta-item -->

                                        <div class="property-box-meta-item">
                                            <span><?=__("Baños")?></span>
                                            <strong><?=$property['Property']['bathrooms']?></strong>
                                        </div><!-- /.property-box-meta-item -->

                                        <div class="property-box-meta-item">
                                            <span><?=__("Garajes")?></span>
                                            <strong><?=$property['Property']['garages']?></strong>
                                        </div><!-- /.property-box-meta-item -->
                                    </div><!-- /.property-box-meta -->
                                </div><!-- /.property-box-content -->

                                <div class="property-box-bottom">
                                    <div class="property-box-price">
                                        <?=$property['Cities']['name']?>
                                    </div><!-- /.property-box-price -->

                                    <a href="<?=$this->Html->url(array('controller' => 'properties', 'action' => 'view', $property['Property']['id']))?>" class="property-box-view">
                                        <?= __("Ver detalle") ?>
                                    </a><!-- /.property-box-view -->
                                </div><!-- /.property-box-bottom -->
                            </div><!-- /.property-box -->
                        </div>
                    <?php endforeach; ?>
                </div><!-- /.properties-isotope -->
                <div class="cake-pagination col-xs-12 text-center">
                    <?php
                    echo $this->Paginator->prev('< ', array(), null, array('class' => 'prev disabled'));
                    echo $this->Paginator->numbers(array('separator' => ''));
                    echo $this->Paginator->next(' >', array(), null, array('class' => 'next disabled'));
                    ?>
                </div>
                <br><br>
                <div class="cake-pagination col-xs-12 text-center">
                    <?php
                    echo $this->Paginator->counter(array(
                        'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, empezando en el registro {:start}, y terminando en el {:end}')
                    ));
                    ?>	
                </div>
            </div><!-- /.row -->
        </div>
    </div>
</div>