<?php
$images = "";
foreach($this->request->data['PropertiesImages'] as $img):
    $images .= "'" . $this->Html->image($img['rute'], array('class' => 'file-preview-image')) . "'";
    $images .= ',';
endforeach;
?>
<div class="content">
    <div class="container">
        <div class="add-properties row">
            <div class="col-sm-8 col-md-9">
                <div class="widget-title">
                    <h2><?= __("Publicar nueva propiedad") ?></h2>
                </div>
                <?= $this->Form->create('Property', array('type' => 'file')); ?>
                <?=$this->Form->input('id');?>
                <?=$this->Form->input('users_id', array('type' => 'hidden', 'value' => $loggedUser['id']));?>
                <div class="box">
                    <div class="form-group">
                        <?=
                        $this->Form->input('title', array(
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => __("Título")
                        ));
                        ?>
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <?=
                        $this->Form->input('description', array(
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => __("Descripción")
                        ));
                        ?>
                    </div><!-- /.form-group -->
                </div><!-- /.box -->

                <div class="widget-title">
                    <h2><?= __("Atributos de la propiedad") ?></h2>
                </div>

                <div class="box">
                    <div class="row">
                        <div class="col-sm-6">
                            <?=
                            $this->Form->input('property_type', array(
                                'label' => false,
                                'class' => 'form-control',
                                'div' => 'form-group'
                            ));
                            ?>

                            <?=
                            $this->Form->input('categories_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'div' => 'form-group'
                            ));
                            ?>

                            <?=
                            $this->Form->input('countries_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'div' => 'form-group'
                            ));
                            ?>
                            <?=
                            $this->Form->input('provinces_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'div' => 'form-group'
                            ));
                            ?>
                            <?=
                            $this->Form->input('cities_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'div' => 'form-group'
                            ));
                            ?>
                        </div><!-- /.col-* -->

                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-arrows"></i></span>
                                <?=
                                $this->Form->input('lot_area', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __("Área de terreno (metros cuadrados)")
                                ));
                                ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-arrows"></i></span>
                                <?=
                                $this->Form->input('construction_area', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __("Área de construcción (metros cuadrados)")
                                ));
                                ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-picture-o"></i></span>
                                <?=
                                $this->Form->input('rooms', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __("Habitaciones")
                                ));
                                ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-tint"></i></span>
                                <?=
                                $this->Form->input('bathrooms', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __("Baños")
                                ));
                                ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                <?=
                                $this->Form->input('garages', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __("Garages")
                                ));
                                ?>
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.box -->


                <div class="widget-title">
                    <h2><?= __("Precio de la propiedad") ?></h2>
                </div>

                <div class="box">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?=
                                $this->Form->input('c_s_price', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'placeholder' => __('Precio de venta en colones'),
                                    'div' => 'form-group'
                                ));
                                ?>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?=
                                $this->Form->input('c_a_price', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'placeholder' => __('Precio de alquiler en colones'),
                                    'div' => 'form-group'
                                ));
                                ?>
                            </div>
                        </div><!-- /.col-* -->

                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?=
                                $this->Form->input('d_s_price', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __('Precio de venta en dólares')
                                ));
                                ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?=
                                $this->Form->input('d_a_price', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __('Precio de alquiler en dólares')
                                ));
                                ?>
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.box -->


                <div class="row">
                    <div class="col-sm-12">
                        <div class="widget-title">
                            <h2><?= __("Posición en el mapa") ?></h2>
                        </div>

                        <div class="box">
                            <input id="pac-input" class="controls" type="text" placeholder="Enter a location">

                            <div id="map-canvas"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <?=
                                        $this->Form->input('latitude', array(
                                            'class' => 'form-control',
                                            'id' => 'input-latitude',
                                            'type' => 'text',
                                            'label' => false,
                                            'placeholder' => __("Latitud")
                                        ));
                                        ?>
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <?=
                                        $this->Form->input('longitude', array(
                                            'class' => 'form-control',
                                            'id' => 'input-longitude',
                                            'type' => 'text',
                                            'label' => false,
                                            'placeholder' => __("Longitud")
                                        ));
                                        ?>
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->
                        </div><!-- /.box -->
                    </div>

                    <div class="col-sm-12">
                        <div class="widget-title">
                            <h2><?= __("Galería de imágenes") ?></h2>
                        </div>

                        <div class="box">
                            <input id="input-file-properties" name="input-file-properties[]" type="file" class="file-loading" accept="image/*" multiple>
                        </div><!-- /.box -->
                    </div>
                </div><!-- /.row -->

                <div class="center">
                    <button class="btn btn-xl" type="submit"><?= __('Continuar'); ?></button>
                </div><!-- /.center -->
                <?= $this->Form->end(); ?>
            </div><!-- /.content -->
            <div class="col-sm-4 col-md-3">
                <?= $this->element('Page/ads-sidebar') ?>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.content -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#PropertyCountriesId').change(function () {
            var getProvincesUrl = "<?= $this->Html->url(['controller' => 'properties', 'action' => 'getProvinces']); ?>/" + $(this).val();
            $.getJSON(getProvincesUrl, function (selectValues) {
                $('#PropertyProvincesId').find('option').remove().end();
                $('#PropertyProvincesId').append($("<option></option>").attr("value", "0").text("<?= __('Provincia'); ?>"));
                $.each(selectValues, function (key, value) {
                    $('#PropertyProvincesId').append($("<option></option>").attr("value", key).text(value));
                });
                $('#PropertyProvincesId').selectpicker('refresh');
            })
        });

        $('#PropertyProvincesId').change(function () {
            var getCitiesUrl = "<?= $this->Html->url(['controller' => 'properties', 'action' => 'getCities']); ?>/" + $(this).val();
            $.getJSON(getCitiesUrl, function (selectValues) {
                $('#PropertyCitiesId').find('option').remove().end();
                $('#PropertyCitiesId').append($("<option></option>").attr("value", "0").text("<?= __('Ciudad'); ?>"));
                $.each(selectValues, function (key, value) {
                    $('#PropertyCitiesId').append($("<option></option>").attr("value", key).text(value));
                });
                $('#PropertyCitiesId').selectpicker('refresh');
            });
        });

        'use strict';
        function initialize() {
            var mapOptions = {
                center: new google.maps.LatLng($('#input-latitude').val(), $('#input-longitude').val()),
                zoom: 16
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


            var image = 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png';
            var marker = new google.maps.Marker({
                position: {lat: parseFloat($('#input-latitude').val()), lng: parseFloat($('#input-longitude').val())},
                map: map,
                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });
            marker.setIcon(/** @type {google.maps.Icon} */({
                url: image,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));

            var input = /** @type {HTMLInputElement} */(
                document.getElementById('pac-input'));

                var types = document.getElementById('type-selector');
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);

                var infowindow = new google.maps.InfoWindow();

                google.maps.event.addListener(marker, "mouseup", function(event) {
                    $('#input-latitude').val(this.position.lat());
                    $('#input-longitude').val(this.position.lng());
                });

                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    infowindow.close();
                    marker.setVisible(false);
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        return;
                    }

                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }
                    marker.setIcon(/** @type {google.maps.Icon} */({
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(35, 35)
                    }));
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

                    $('#input-latitude').val(place.geometry.location.lat());
                    $('#input-longitude').val(place.geometry.location.lng());

                    var address = '';
                    if (place.address_components) {
                        address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');
                    }

                    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                    infowindow.open(map, marker);
                });
            }

            if ($('#map-canvas').length != 0) {
                google.maps.event.addDomListener(window, 'load', initialize);
            }
            //block enter on google maps
            var input = document.getElementById('pac-input');
            google.maps.event.addDomListener(input, 'keydown', function(e) {
              if (e.keyCode == 13) {
                  e.preventDefault();
              }
            });

            jQuery("#input-file-properties").fileinput({
                uploadUrl: "/file-upload-batch/2",
//                autoReplace: true,
                maxFileCount: 10,
                allowedFileExtensions: ["jpg", "png", "gif"],
                dropZoneTitle: "Arrastra las fotos de tu propiedad aquí",
                browseLabel: "Explorar",
                removeLabel: "Remover fotos",
                uploadLabel: "Subir",
                initialPreview: [
                    <?=$images?>
                ]
            });
    });
</script>
