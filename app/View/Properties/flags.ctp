<div class="container">
    <div class="row">
        <div class="content col-sm-8 col-md-9">
            <h1 class="page-header"><?=__('Promociona tu Anuncio');?></h1>
            <p class="text mb30">
                <?=__('Sabemos que esta venta es importante, y por ende su anuncio tambien. Demuéstralo resaltando tu anuncio, en una posición superior a miles de otros anunciantes, ¡Hoy!. Resaltar su anuncio puede aumentar sus ventas y visitas a su anuncio dramáticamente!')?>
            </p>
            <div class="content col-sm-12 col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>&emsp;</th>
                            <th><?=__('Etiquetas');?></th>
                            <th><?=__('Precio');?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $numLine = 0;?>
                        <?php foreach ($flags as $flag) { ?>
                            <?php 
                                $tipeColor = $numLine % 3; 
                                if($tipeColor == 0){
                                    $color = '1E88E5';
                                } else if( $tipeColor == 1 ){
                                    $color = '1565C0';
                                } else if( $tipeColor == 2 ){
                                    $color = 'E91E63';
                                }
                            ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="flags[]" class="input-flag" value="<?=$flag['Flag']['id']?>" cost-data="<?=$flag['Flag']['cost']?>">
                                </td>
                                <td>
                                    <img src="https://placeholdit.imgix.net/~text?txtsize=16&bg=<?=$color?>&txtclr=ffffff&txt=<?=urlencode($flag['Flag']['name'])?>&w=200&h=40">
                                </td>
                                <td>
                                    $<?=$flag['Flag']['cost']?>
                                </td>
                            </tr>
                            <?php $numLine++; ?>
                        <?php } ?>
                            <tr>
                                <td></td>
                                <td><b style="float: right"><?=__('Total');?></b></td>
                                <td class="total-numer"></td>
                            </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="col-sm-2 col-md-2">
                <a href="<?=$this->Html->url(['controller' => 'properties', 'action' => 'images/' . $idPropertie]);?>" class="btn">
                    <i class="fa fa-chevron-left"></i> <?=__('Anterior');?>
                </a>
            </div>
            <div class="col-sm-2 col-md-2 col-md-offset-8">
                
            </div>
            
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){
        $('.input-flag').click(function(){
            var total = 0;
            $('.input-flag').each(function(){
                if($(this).is(':checked')){
                    total = total + parseInt($(this).attr('cost-data'));
                }
            });
            if( total > 0 ){
                jQuery('.total-numer').html('$' + total);
            } else {
                jQuery('.total-numer').html("");
            }
        });
    });

</script>