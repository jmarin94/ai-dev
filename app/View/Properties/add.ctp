<div class="content">
    <div class="container">
        <div class="add-properties row">
            <div class="col-sm-8 col-md-9">
                <div class="widget-title">
                    <h2><?= __("Publicar nueva propiedad") ?></h2>
                </div>
                <?=$this->Form->create('Property', array('type' => 'file'));?>
                <?=$this->Form->input('id');?>
                <?=$this->Form->input('users_id', array('type' => 'hidden', 'value' => $loggedUser['id']));?>
                <?=$this->Form->input('payment_total', array('type' => 'hidden', 'value' => 0))?>
                <div class="box">
                    <div class="form-group">
                        <?=
                        $this->Form->input('title', array(
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => __("Título")
                        ));
                        ?>
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <?=
                        $this->Form->input('description', array(
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => __("Descripción")
                        ));
                        ?>
                    </div><!-- /.form-group -->
                </div><!-- /.box -->

                <div class="widget-title">
                    <h2><?= __("Atributos de la propiedad") ?></h2>
                </div>

                <div class="box">
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $this->Form->input('property_type', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => array(null => __('Tipo de propiedad')),
                                'div' => 'form-group'
                            )); ?>

                            <?= $this->Form->input('categories_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => array(null => __('Categoría')),
                                'div' => 'form-group'
                            ));?>

                            <?= $this->Form->input('countries_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => array(null => __('País')),
                                'div' => 'form-group'
                            )); ?>
                            <?= $this->Form->input('provinces_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => array(null => __('Provincia')),
                                'div' => 'form-group'
                            )); ?>
                            <?= $this->Form->input('cities_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => array(null => __('Ciudad')),
                                'div' => 'form-group'
                            ));?>
                        </div><!-- /.col-* -->

                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-arrows"></i></span>
                                <?= $this->Form->input('lot_area', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __("Área de terreno (metros cuadrados)")
                                ));
                                ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-arrows"></i></span>
                                <?= $this->Form->input('construction_area', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __("Área de construcción (metros cuadrados)")
                                )); ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-picture-o"></i></span>
                                <?= $this->Form->input('rooms', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __("Habitaciones")
                                )); ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-tint"></i></span>
                                <?= $this->Form->input('bathrooms', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __("Baños")
                                )); ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                <?= $this->Form->input('garages', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __("Garages")
                                )); ?>
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.box -->


                <div class="widget-title">
                    <h2><?= __("Precio de la propiedad") ?></h2>
                </div>

                <div class="box">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?= $this->Form->input('c_s_price', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'placeholder' => __('Precio de venta en colones'),
                                    'div' => 'form-group'
                                )); ?>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?= $this->Form->input('c_a_price', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'placeholder' => __('Precio de alquiler en colones'),
                                    'div' => 'form-group'
                                ));?>
                            </div>
                        </div><!-- /.col-* -->

                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?= $this->Form->input('d_s_price', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __('Precio de venta en dólares')
                                ));
                                ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?= $this->Form->input('d_a_price', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'number',
                                    'div' => 'form-group',
                                    'placeholder' => __('Precio de alquiler en dólares')
                                )); ?>
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.box -->


                <div class="row">
                    <div class="col-sm-12">
                        <div class="widget-title">
                            <h2><?= __("Posición en el mapa") ?></h2>
                        </div>

                        <div class="box">
                            <input id="pac-input" class="controls" type="text" placeholder="Enter a location">

                            <div id="map-canvas"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <?= $this->Form->input('latitude', array(
                                            'class' => 'form-control',
                                            'id' => 'input-latitude',
                                            'type' => 'text',
                                            'label' => false,
                                            'placeholder' => __("Latitud")
                                        )); ?>
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <?= $this->Form->input('longitude', array(
                                            'class' => 'form-control',
                                            'id' => 'input-longitude',
                                            'type' => 'text',
                                            'label' => false,
                                            'placeholder' => __("Longitud")
                                        )); ?>
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->
                        </div><!-- /.box -->
                    </div>

                    <div class="col-sm-12">
                        <div class="widget-title">
                            <h2><?= __("Galería de imágenes") ?></h2>
                        </div>

                        <div class="box">
                            <input id="input-file-properties" name="input-file-properties[]" type="file" class="file-loading" accept="image/*" multiple>
                        </div><!-- /.box -->
                    </div>
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="widget-title">
                            <h2 class="page-header"><?= __("Destaca tu propiedad") ?></h2>
                        </div>
                        <div class="box">
                            <div class="row property-demo">
                                <div class="col-sm-3">
                                      <a class="property-simple-image">
                                          <div class="featured-icon" style="display: none"></div>
                                          <div class="superoffer-icon" style="display: none"></div>
                                          <div class="nailthumb-container square-thumb properties-demo">
                                            <?=$this->Html->image('ai/ai-demo-property-img.jpg', array('class' => 'demo-image nailthumb-image'))?>
                                          </div>
                                      </a>
                                      <p>
                                          <span><?= __("Barva / Lote")?></span><br>
                                          <b><?= __("Vendo casa nueva") ?></b>
                                      </p>
                                </div>
                                <div class="col-sm-9">
                                    <p><?=__("Su propiedad será colocada entre los primeros lugares")?></p>
                                    <div class="input-group">
                                        <label><input type="checkbox" class="outstanding" name="data[Property][featured]" id="featured"><?=__("Destacada ($ 10 por mes)");?></label>
                                        <label><input type="checkbox" class="outstanding" name="data[Property][super_offer]" id="supper-offer"><?=__("Super oferta ($ 10 por mes)");?></label>
                                    </div>
                                    <div id="months-for-outstanding" style="display: none">
                                        <div class="form-group">
                                            <select class="form-control" id="months" name="data[Property][months]">
                                                <?php for($i = 1; $i <= 12; $i++): ?>
                                                    <option value="<?=$i?>"><?= $i == 1 ? "1 mes" : ($i . " meses"); ?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                             <label id="amount"><?=__("Se le aplicarán")?>&nbsp;<b><?=__("$ 10")?></b>&nbsp;<?=__("por destacar su propiedad durante 1 mes.")?></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                              <div class="col-sm-12">
                                  <label><input type="checkbox" id="free-outstanding"><?=__("No quiero destacar mi propiedad (no se aplicarán cargos)");?></label>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="widget-title">
                            <h2 class="page-header"><?= __("Etiqueta tu propiedad") ?>&nbsp;&nbsp;<span><?= __("($10 la unidad)") ?></span></h2>
                        </div>
                        <div class="box">
                            <div class="property-amenities">
                                <ul>
                                    <?php $i = 0; ?>
                                    <?php foreach($flags as $flag): ?>
                                        <li><label><input type="checkbox" name="data[PropertiesFlag][<?=$flag['Flag']['id']?>]" id="<?=$flag['Flag']['id']?>"><?=$flag['Flag']['name']?></label></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <hr>
                            <label><input type="checkbox" name="data[Property][free_flags]" id="free-flags"><?=__("No quiero etiquetar mi propiedad (no se aplicarán cargos)");?></label>
                        </div>
                    </div>
                </div>

                <div class="center">
                    <button class="btn btn-xl" type="submit"><?=__('Publicar mi propiedad');?><small><?=__("Al hacer click, acepto los términos y condiciones")?></small></button>
                </div><!-- /.center -->
            <?= $this->Form->end(); ?>
            </div><!-- /.content -->
            <div class="col-sm-4 col-md-3">
                <?= $this->element('Page/ads-sidebar') ?>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.content -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#PropertyCountriesId').change(function () {
            var getProvincesUrl = "<?= $this->Html->url(['controller' => 'properties', 'action' => 'getProvinces']); ?>/" + $(this).val();
            $.getJSON(getProvincesUrl, function (selectValues) {
                $('#PropertyProvincesId').find('option').remove().end();
                $('#PropertyProvincesId').append($("<option></option>").attr("value", "0").text("<?= __('Provincia'); ?>"));
                $.each(selectValues, function (key, value) {
                    $('#PropertyProvincesId').append($("<option></option>").attr("value", key).text(value));
                });
                $('#PropertyProvincesId').selectpicker('refresh');
            })
        });

        $('#PropertyProvincesId').change(function () {
            var getCitiesUrl = "<?= $this->Html->url(['controller' => 'properties', 'action' => 'getCities']); ?>/" + $(this).val();
            $.getJSON(getCitiesUrl, function (selectValues) {
                $('#PropertyCitiesId').find('option').remove().end();
                $('#PropertyCitiesId').append($("<option></option>").attr("value", "0").text("<?= __('Ciudad'); ?>"));
                $.each(selectValues, function (key, value) {
                    $('#PropertyCitiesId').append($("<option></option>").attr("value", key).text(value));
                });
                $('#PropertyCitiesId').selectpicker('refresh');
            });
        });

        $("#PropertyAddForm").submit(function(){
            var totalPrice = 0;
            if(!$("#free-outstanding").is(":checked")){
                if($("#featured").is(":checked")){
                      var months = $("#months").find("option:selected").val();
                      var  priceF = 10;
                      totalPrice += (months*priceF);
                }

                if($("#supper-offer").is(":checked")){
                      var months = $("#months").find("option:selected").val();
                      var  priceSO = 10;
                      totalPrice += (months*priceSO);
                }
            }
            if(!$("#free-flags").is(":checked")){
                var priceFlags = 5;
                $(".property-amenities ul li").each(function(i, item){
                    if($(item).find("input").is(":checked")){
                          totalPrice += priceFlags;
                    }
                });
            }
            if(totalPrice > 0){
                 $("#PropertyPaymentTotal").val(totalPrice);
            }
        });

        $("#free-flags").change(function(){
            if($(this).is(":checked")){
                $(".property-amenities").hide();
                $(".property-amenities").next().hide();
            } else {
                $(".property-amenities").show();
                  $(".property-amenities").next().show();
            }
        });
        $("#free-outstanding").change(function(){
            if($(this).is(":checked")){
                $(".property-demo").hide();
                $(".property-demo").next().hide();
            } else {
                $(".property-demo").show();
                  $(".property-demo").next().show();
            }
        });
        $(".outstanding").change(function(){
            var id = $(this).attr("id");
            if($(this).is(":checked")){
                if(id == "featured"){
                    $(".featured-icon").show();
                    $(".superoffer-icon").hide();
                    $("#supper-offer").prop("checked", false);
                } else {
                    $(".featured-icon").hide();
                    $(".superoffer-icon").show();
                    $("#featured").prop("checked", false);
                }
                $("#months-for-outstanding").show();
            } else {
                if(id == "featured"){
                  $(".featured-icon").hide();
                } else {
                  $(".superoffer-icon").hide();
                }

                if(!$("#featured").is(":checked") && !$("#supper-offer").is(":checked")){
                    $("#months-for-outstanding").hide();
                }
            }
        });
        $("#months").change(function(){
            var val = $(this).find("option:selected").val();
            var meses = val == 1 ? "1 mes" : (val + " meses");
            var text  = "Se le aplicarán <b>$ " + (val*7.5) + "</b> por destacar su propiedad durante " + meses;
            $("#amount").html(text);
        });

        'use strict';
        function initialize() {
            var mapOptions = {
                center: new google.maps.LatLng(9.9356124, -84.1483645),
                zoom: 16
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


            var image = 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png';
            var marker = new google.maps.Marker({
                position: {lat: 9.9356124, lng: -84.1483645},
                map: map,
                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });
            marker.setIcon(/** @type {google.maps.Icon} */({
                url: image,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));

            var input = /** @type {HTMLInputElement} */(
                document.getElementById('pac-input'));

                var types = document.getElementById('type-selector');
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);

                var infowindow = new google.maps.InfoWindow();

                google.maps.event.addListener(marker, "mouseup", function(event) {
                    $('#input-latitude').val(this.position.lat());
                    $('#input-longitude').val(this.position.lng());
                });

                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    infowindow.close();
                    marker.setVisible(false);
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        return;
                    }

                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }
                    marker.setIcon(/** @type {google.maps.Icon} */({
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(35, 35)
                    }));
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

                    $('#input-latitude').val(place.geometry.location.lat());
                    $('#input-longitude').val(place.geometry.location.lng());

                    var address = '';
                    if (place.address_components) {
                        address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');
                    }

                    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                    infowindow.open(map, marker);
                });
            }

            if ($('#map-canvas').length != 0) {
                google.maps.event.addDomListener(window, 'load', initialize);
            }
            //block enter on google maps
            var input = document.getElementById('pac-input');
            google.maps.event.addDomListener(input, 'keydown', function(e) {
              if (e.keyCode == 13) {
                  e.preventDefault();
              }
            });

            jQuery("#input-file-properties").fileinput({
//                uploadUrl: "/file-upload-batch/2",
                autoReplace: true,
                maxFileCount: 10,
                allowedFileExtensions: ["jpg", "png", "gif"],
//                dropZoneTitle: "Arrastra las fotos de tu propiedad aquí",
                browseLabel: "Explorar",
                removeLabel: "Remover fotos",
                uploadLabel: "Subir",
                initialCaption: "Selecciona las fotos de tu propiedad"
            });
    });
</script>
