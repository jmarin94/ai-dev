<div class="container">
    <div class="row">
        <div class="content col-sm-8 col-md-9">
            <h1 class="page-header"><?=__('Agregar Imagenes');?></h1>
            
            <?=$this->Form->create('Image', ['type' => 'file']); ?>
                <div class="box">
                    <?=$this->Form->input('property', ['type' => 'hidden', 'value' => $idPropertie]);?>
                    <?=$this->Form->input('url', array('type' => 'file', 'div' => false, 'label' => false, 'multiple' => true, 'name' => 'ImageUrl[]', 'accept' => 'image/*', 'id' => 'properties_images'))?>
                </div>
            <?=$this->Form->end(); ?>
            
            <div class="col-sm-12 col-md-12">
                <?php if($images){ ?>
                    <div class="col-sm-12 col-md-21">
                        <?php foreach ($images as $images){ ?>
                            <div class="col-sm-4 col-md-4">
                                <div class="box">
                                    <img src="<?=$this->Html->url('/files/img/' . $images['PropertiesImage']['rute']);?>" class="img-thumbnail">
                                    <a href="<?=$this->Html->url(['controller' => 'properties', 'action' => 'deleteImage/'.$idPropertie.'/'.$images['PropertiesImage']['id']]);?>" class="btn btn-xs btn-block">
                                        <i class="fa fa-trash"></i> <?=__('Borrar Imagen');?>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
            
            <div class="col-sm-2 col-md-2">
                <a href="<?=$this->Html->url(['controller' => 'properties', 'action' => 'edit/' . $idPropertie]);?>" class="btn">
                    <i class="fa fa-chevron-left"></i> <?=__('Anterior');?>
                </a>
            </div>
            <div class="col-sm-2 col-md-2 col-md-offset-8">
                <a href="<?=$this->Html->url(['controller' => 'properties', 'action' => 'flags/' . $idPropertie]);?>" class="btn"><i class="fa fa-chevron-right"></i> <?=__('Siguiente');?></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#properties_images').fileinput({
            previewFileType: "image",
            browseClass: "btn btn-success",
            browseLabel: " <?php echo __('Bucar Imagenes');?>",
            browseIcon: '<i class="fa fa-folder-open"></i>',
            removeClass: "btn btn-danger",
            removeLabel: " <?php echo __('Borrar');?>",
            removeIcon: '<i class="fa fa-trash"></i>',
            uploadClass: "btn btn-info",
            uploadLabel: " <?php echo __('Subir');?>",
            uploadIcon: '<i class="fa fa-upload"></i>',
            maxFileCount: 10,
            msgSizeTooLarge: 'El archivo "{name}" (<b>{size} KB</b>) exede el tamaño maximo de <b>{maxSize} KB</b>. por favor intentelo de nuevo!',
            msgFilesTooMany: 'El numero de archivos para subir <b>({n})</b> exede el numero maximo <b>{m}</b>. Por favor intentelo de nuevo',
            msgValidationError: '<span class="text-danger"><i class="fa fa-exclamation-triangle"></i></i> Error al cargar los archivos</span>',
            maxFileSize: 2000
        });
    })
</script>