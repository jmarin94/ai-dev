<div class="properties form">
<?php echo $this->Form->create('Property'); ?>
	<fieldset>
		<legend><?php echo __('Aipanel Add Property'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('property_type');
		echo $this->Form->input('countries_id');
		echo $this->Form->input('provinces_id');
		echo $this->Form->input('cities_id');
		echo $this->Form->input('lot_area');
		echo $this->Form->input('construction_area');
		echo $this->Form->input('rooms');
		echo $this->Form->input('bathrooms');
		echo $this->Form->input('garages');
		echo $this->Form->input('description');
		echo $this->Form->input('status');
		echo $this->Form->input('outstanding');
		echo $this->Form->input('d_s_price');
		echo $this->Form->input('d_a_price');
		echo $this->Form->input('c_s_price');
		echo $this->Form->input('c_a_price');
		echo $this->Form->input('Flag');
		echo $this->Form->input('Promotion');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Properties'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Type Properties'), array('controller' => 'property_type', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type Properties'), array('controller' => 'property_type', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Countries'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Provinces'), array('controller' => 'provinces', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Provinces'), array('controller' => 'provinces', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cities'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flags'), array('controller' => 'flags', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flag'), array('controller' => 'flags', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Promotions'), array('controller' => 'promotions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promotion'), array('controller' => 'promotions', 'action' => 'add')); ?> </li>
	</ul>
</div>
