<div class="content">
    <div class="container">
        <div class="row">
            <div class=" col-sm-8 col-md-9">
                <?php // debug($property);?>
                <h1 class="page-header"><?= $property['Property']['title'] ?></h1>
                <div class="row">
                    <div class="col-sm-6 col-md-5">
                        <div class="module">
                            <div class="module-info center vertical-align min-width">
                                <?=__("Ciudad")?>
                            </div><!-- /.module-info -->

                            <div class="module-content vertical-align">
                                <span><?=$property['Cities']['name']?></span>
                            </div><!-- /.module-content -->
                        </div><!--- /.module -->
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="module">
                            <div class="module-info center vertical-align min-width">
                                <?=__("Habitaciones")?>
                            </div><!-- /.module-info -->

                            <div class="module-content vertical-align">
                                <span><?=$property['Property']['rooms']?></span>
                            </div><!-- /.module-content -->
                        </div><!--- /.module -->
                    </div>

                    <div class="col-sm-6 col-md-2">
                        <div class="module">
                            <div class="module-info center vertical-align min-width">
                                <?=__("Baños")?>
                            </div><!-- /.module-info -->

                            <div class="module-content vertical-align">
                                <span><?=$property['Property']['bathrooms']?></span>
                            </div><!-- /.module-content -->
                        </div><!--- /.module -->
                    </div>

                    <div class="col-sm-6 col-md-2">
                        <div class="module">
                            <div class="module-info center vertical-align min-width">
                               <?=__("Garajes")?>
                            </div><!-- /.module-info -->

                            <div class="module-content vertical-align">
                                <span><?=$property['Property']['garages']?></span>
                            </div><!-- /.module-content -->
                        </div><!--- /.module -->
                    </div>
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-sm-12 col-md-7">

                        <div class="property-gallery">
                            <div class="property-gallery-preview">
                                <?php if (count($property['PropertiesImages']) > 0) { ?>
                                    <a href="javascript:void(8)">
                                        <div>
                                            <?php 
                                            $outstanding = "";
                                            if(isset($property['Property']['outstanding'])): 
                                                $date = new DateTime($property['Property']['created']);
                                                $monts = $property['Property']['months'];
                                                $expiration = strtotime('+' . $monts . ' month', strtotime($date->format('d-m-Y')));
                                                $expiration = date ('d-m-Y' , $expiration);

                                                if(strtotime($expiration) > strtotime(date('d-m-Y'))):
                                                    if($property['Property']['outstanding'] == 2): 
                                                        $outstanding = "property-featured";
                                                    elseif ($property['Property']['outstanding'] == 1):
                                                        $outstanding = "property-super-offer";
                                                    endif; 
                                                endif; 
                                            endif; 
                                            ?>
                                            <?php if($outstanding != "" && $outstanding == "property-featured"): ?>
                                                <div class="featured-icon"></div>
                                            <?php elseif($outstanding != "" && $outstanding == "property-super-offer"): ?>
                                                <div class="superoffer-icon" ></div>
                                            <?php endif; ?>
                                            <?php if(isset($property['PropertiesImages'][0]['rute'])): ?>
                                                <?= $this->Html->image($property['PropertiesImages'][0]['rute'], array("width" => "360")) ?>
                                            <?php else: ?>
                                                <?= $this->Html->image("ai/no-image.jpg", array("alt" => __("Imagen no disponible"), "width" => "360")); ?>
                                            <?php endif; ?>
                                        </div>
                                    </a>
                                <?php } else { ?>
                                    <a href="javascript:void(8)">
                                        <div>
                                            <?= $this->Html->image("ai/no-image.jpg", array("alt" => __("Imagen no disponible"), "width" => "360")); ?>
                                        </div>
                                    </a>
                                <?php }  ?>
                            </div>
                            <?php if (count($property['PropertiesImages']) > 0) { ?>
                                <div class="property-gallery-list-wrapper">
                                    <div class="property-gallery-list">
                                        <?php foreach ($property['PropertiesImages'] as $image) { ?>

                                            <div class="property-gallery-list-item active">
                                                <a href="<?= $this->Html->url("../img/" . $image['rute']); ?>">
                                                    <?= $this->Html->image($image['rute']) ?>
                                                </a>
                                            </div>

                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                    </div>

                    <div class="col-sm-12 col-md-5">
                        <div class="property-list">
                            <dl>
                                <?php
                                if ((isset($property['PropertyType']['name'])) && ($property['PropertyType']['name'] != "")) {
                                    echo '<dt>' . __('Tipo de propiedad') . '</dt><dd>' . $property['PropertyType']['name'] . '</dd>';
                                }
                                if ((isset($property['Categories']['name'])) && ($property['Categories']['name'] != "")) {
                                    echo '<dt>' . __('Categoría') . '</dt><dd>' . $property['Categories']['name'] . '</dd>';
                                }
                                if ((isset($property['Countries']['name'])) && ($property['Countries']['name'] != "")) {
                                    echo '<dt>' . __('País') . '</dt><dd>' . $property['Countries']['name'] . '</dd>';
                                }
                                if ((isset($property['Provinces']['name'])) && ($property['Provinces']['name'] != "")) {
                                    echo '<dt>' . __('Provincia') . '</dt><dd>' . $property['Provinces']['name'] . '</dd>';
                                }
                                if ((isset($property['Cities']['name'])) && ($property['Cities']['name'] != "")) {
                                    echo '<dt>' . __('Ciudad') . '</dt><dd>' . $property['Cities']['name'] . '</dd>';
                                }
                                if ((isset($property['Property']['lot_area'])) && ($property['Property']['lot_area'] != "")) {
                                    echo '<dt>' . __('Área del lote') . '</dt><dd>' . $property['Property']['lot_area'] . ' m<sup>2</sup></dd>';
                                }
                                if ((isset($property['Property']['construction_area'])) && ($property['Property']['construction_area'] != "")) {
                                    echo '<dt>' . __('Área de construcción') . '</dt><dd>' . $property['Property']['construction_area'] . ' m<sup>2</sup></dd>';
                                }
                                if ((isset($property['Property']['rooms'])) && ($property['Property']['rooms'] != "")) {
                                    echo '<dt>' . __('Habitaciones') . '</dt><dd>' . $property['Property']['rooms'] . '</dd>';
                                }
                                if ((isset($property['Property']['bathrooms'])) && ($property['Property']['bathrooms'] != "")) {
                                    echo '<dt>' . __('Baños') . '</dt><dd>' . $property['Property']['bathrooms'] . '</dd>';
                                }
                                if ((isset($property['Property']['garages'])) && ($property['Property']['garages'] != "")) {
                                    echo '<dt>' . __('Garajes') . '</dt><dd>' . $property['Property']['garages'] . '</dd>';
                                }
                                if ((isset($property['Property']['d_s_price'])) && ($property['Property']['d_s_price'] > 0)) {
                                    echo '<dt>' . __('Precio de venta') . '</dt><dd>&#36; ' . number_format($property['Property']['d_s_price'], 2, '.', ',') . '</dd>';
                                }
                                if ((isset($property['Property']['c_s_price'])) && ($property['Property']['c_s_price'] > 0)) {
                                    echo '<dt>' . __('Precio de venta') . '</dt><dd>&#8353; ' . number_format($property['Property']['c_s_price'], 2, '.', ',') . '</dd>';
                                }
                                if ((isset($property['Property']['d_a_price'])) && ($property['Property']['d_a_price'] > 0)) {
                                    echo '<dt>' . __('Precio de alquiler') . '</dt><dd>&#36; ' . number_format($property['Property']['d_a_price'], 2, '.', ',') . '</dd>';
                                }
                                if ((isset($property['Property']['c_a_price'])) && ($property['Property']['c_a_price'] > 0)) {
                                    echo '<dt>' . __('Precio de alquiler') . '</dt><dd>&#8353; ' . number_format($property['Property']['c_a_price'], 2, '.', ',') . '</dd>';
                                }
                                ?>
                            </dl>
                        </div>

                        <h2 class="mb30"><?= __('Compartir') ?></h2>
                        <ul class="clearfix sharing-buttons">
                            <li>
                                <a class="facebook" href="https://www.facebook.com/share.php?u=<?= Router::url($this->here, true); ?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                    <i class="fa fa-facebook fa-left"></i>
                                    <span class="social-name">Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a class="google-plus" href="https://plus.google.com/share?url=<?= Router::url($this->here, true); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                    <i class="fa fa-google-plus fa-left"></i>
                                    <span class="social-name">Google+</span>
                                </a>
                            </li>
                            <li>
                                <a class="twitter" href="https://twitter.com/home?status=<?= Router::url($this->here, true); ?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                    <i class="fa fa-twitter fa-left"></i>
                                    <span class="social-name">Twitter</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
                
                <?php if(count($property['Flag']) > 0): ?>
                    <h2 class="page-header"><?= __("Etiquetas") ?></h2>
                    <div class="property-amenities">
                        <ul>
                            <?php foreach($property['Flag'] as $flag): ?>
                                <li class="yes"><?= $flag['name'] ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div><!-- /.property-amenities -->
                <?php endif; ?>
                
                    <div class="mb30">
                        <h2><?=__("Descripción")?></h2>

                        <p class="text">
                            <?=$property['Property']['description']?>
                        </p>
                    </div>

                <?php if($property['Property']['latitude'] != 0): ?>
                    <h2 class="page-header"><?=__("Posición en el mapa")?></h2>

                    <div class="map-property">
                        <div id="map-property"></div><!-- /#map-property -->
                    </div><!-- /.map-property -->
                <?php endif; ?>

                
                <div class="module">
                    <div class="module-content">
                        <div class="agent-card">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 mb30">
                                    <?php $url = Router::url('/').'ver/anunciante/'.$user['User']['id'].'-' .strtolower(Inflector::slug($user['User']['full_name'], '-')); ?>
                                    <a href="<?= $url ?>" class="agent-card-image">
                                        <div class="nailthumb-container square-thumb property-owner">
                                            <?php if(isset($user['User']['img'])): ?>
                                                <?=$this->Html->image("ai/files/users/" . $user['User']['img'], array(
                                                    "alt" => "Imagen de perfil",
                                                    "class" => "img-responsive img-thumbnail"
                                                ));?>
                                            <?php else: ?>
                                                <?= $this->Html->image("ai/no-image.jpg", array("alt" => __("Imagen no disponible"))); ?>
                                            <?php endif; ?>
                                        </div>
                                    </a><!-- /.agent-card-image -->
                                </div>

                                <div class="col-sm-12 col-md-3">
                                    <h2><?=__("Información del anunciante")?></h2>

                                    <div class="agent-card-info">
                                        <?php ?>
                                        <ul>
                                            <?php if(isset($user['User']['phone']) && $user['User']['phone'] != ""): ?>
                                                <li><i class="fa fa-phone"></i><?=$user['User']['phone']?></li>
                                            <?php endif; ?>
                                            <?php if(isset($user['User']['username'])): ?>
                                                <li><i class="fa fa-at"></i> <a href="mailto:<?=$user['User']['username']?>"><?=$user['User']['username']?></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div><!-- /.agent-card-info -->
                                </div>

                                <div class="col-sm-12 col-md-5 mb30">
                                    <h2><?=__("Contáctalo")?></h2>

                                    <div class="agent-card-form">
                                        <form method="post" action="#">
                                            <div class="form-group">
                                                <input class="form-control" type="text" placeholder="Nombre">
                                            </div><!-- /.form-group -->

                                            <div class="form-group">
                                                <input class="form-control" type="text" placeholder="E-mail">
                                            </div><!-- /.form-group -->

                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="Mensaje"></textarea>
                                            </div><!-- /.form-group -->

                                            <button class="btn" type="submit"><?=__("Enviar mensaje");?></button>
                                        </form>
                                    </div><!-- /.agent-card-form -->
                                </div>
                            </div>
                        </div><!-- /.agent-card-->
                    </div><!-- /.module-content -->
                </div><!-- /.module -->
            </div>
            <?= $this->element('Page/ai-sidebar-search') ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var map_property = $('#map-property');
        if (map_property.length) {
            map_property.google_map({
                markers: [{
                    latitude: parseFloat('<?= $property['Property']['latitude'] ?>'),
                    longitude: parseFloat('<?= $property['Property']['longitude'] ?>')
                }], 
                center: {
                        latitude: parseFloat('<?= $property['Property']['latitude'] ?>'),
                        longitude: parseFloat('<?= $property['Property']['longitude'] ?>')
                },
                zoom: 16
            });
        }
    });
</script>