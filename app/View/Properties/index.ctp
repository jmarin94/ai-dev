<div class="properties index">
	<h2><?php echo __('Properties'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('property_type'); ?></th>
			<th><?php echo $this->Paginator->sort('countries_id'); ?></th>
			<th><?php echo $this->Paginator->sort('provinces_id'); ?></th>
			<th><?php echo $this->Paginator->sort('cities_id'); ?></th>
			<th><?php echo $this->Paginator->sort('lot_area'); ?></th>
			<th><?php echo $this->Paginator->sort('construction_area'); ?></th>
			<th><?php echo $this->Paginator->sort('rooms'); ?></th>
			<th><?php echo $this->Paginator->sort('bathrooms'); ?></th>
			<th><?php echo $this->Paginator->sort('garages'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('outstanding'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('d_s_price'); ?></th>
			<th><?php echo $this->Paginator->sort('d_a_price'); ?></th>
			<th><?php echo $this->Paginator->sort('c_s_price'); ?></th>
			<th><?php echo $this->Paginator->sort('c_a_price'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($properties as $property): ?>
	<tr>
		<td><?php echo h($property['Property']['id']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['title']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($property['PropertyType']['name'], array('controller' => 'property_type', 'action' => 'view', $property['PropertyType']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($property['Countries']['name'], array('controller' => 'countries', 'action' => 'view', $property['Countries']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($property['Provinces']['name'], array('controller' => 'provinces', 'action' => 'view', $property['Provinces']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($property['Cities']['name'], array('controller' => 'cities', 'action' => 'view', $property['Cities']['id'])); ?>
		</td>
		<td><?php echo h($property['Property']['lot_area']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['construction_area']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['rooms']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['bathrooms']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['garages']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['description']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['status']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['outstanding']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['created']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['d_s_price']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['d_a_price']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['c_s_price']); ?>&nbsp;</td>
		<td><?php echo h($property['Property']['c_a_price']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $property['Property']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $property['Property']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $property['Property']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $property['Property']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Property'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Type Properties'), array('controller' => 'property_type', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type Properties'), array('controller' => 'property_type', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Countries'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Provinces'), array('controller' => 'provinces', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Provinces'), array('controller' => 'provinces', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cities'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flags'), array('controller' => 'flags', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flag'), array('controller' => 'flags', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Promotions'), array('controller' => 'promotions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promotion'), array('controller' => 'promotions', 'action' => 'add')); ?> </li>
	</ul>
</div>
