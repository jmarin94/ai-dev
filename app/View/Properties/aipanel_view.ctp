<div class="properties view">
<h2><?php echo __('Property'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($property['Property']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($property['Property']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type Properties'); ?></dt>
		<dd>
			<?php echo $this->Html->link($property['PropertyType']['name'], array('controller' => 'property_type', 'action' => 'view', $property['PropertyType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Countries'); ?></dt>
		<dd>
			<?php echo $this->Html->link($property['Countries']['name'], array('controller' => 'countries', 'action' => 'view', $property['Countries']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Provinces'); ?></dt>
		<dd>
			<?php echo $this->Html->link($property['Provinces']['name'], array('controller' => 'provinces', 'action' => 'view', $property['Provinces']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cities'); ?></dt>
		<dd>
			<?php echo $this->Html->link($property['Cities']['name'], array('controller' => 'cities', 'action' => 'view', $property['Cities']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lot Area'); ?></dt>
		<dd>
			<?php echo h($property['Property']['lot_area']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Construction Area'); ?></dt>
		<dd>
			<?php echo h($property['Property']['construction_area']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rooms'); ?></dt>
		<dd>
			<?php echo h($property['Property']['rooms']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bathrooms'); ?></dt>
		<dd>
			<?php echo h($property['Property']['bathrooms']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Garages'); ?></dt>
		<dd>
			<?php echo h($property['Property']['garages']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($property['Property']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($property['Property']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Outstanding'); ?></dt>
		<dd>
			<?php echo h($property['Property']['outstanding']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($property['Property']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D S Price'); ?></dt>
		<dd>
			<?php echo h($property['Property']['d_s_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('D A Price'); ?></dt>
		<dd>
			<?php echo h($property['Property']['d_a_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('C S Price'); ?></dt>
		<dd>
			<?php echo h($property['Property']['c_s_price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('C A Price'); ?></dt>
		<dd>
			<?php echo h($property['Property']['c_a_price']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Property'), array('action' => 'edit', $property['Property']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Property'), array('action' => 'delete', $property['Property']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $property['Property']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Type Properties'), array('controller' => 'property_type', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type Properties'), array('controller' => 'property_type', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Countries'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Provinces'), array('controller' => 'provinces', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Provinces'), array('controller' => 'provinces', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cities'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Flags'), array('controller' => 'flags', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flag'), array('controller' => 'flags', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Promotions'), array('controller' => 'promotions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promotion'), array('controller' => 'promotions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Flags'); ?></h3>
	<?php if (!empty($property['Flag'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Cost'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($property['Flag'] as $flag): ?>
		<tr>
			<td><?php echo $flag['id']; ?></td>
			<td><?php echo $flag['name']; ?></td>
			<td><?php echo $flag['cost']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'flags', 'action' => 'view', $flag['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'flags', 'action' => 'edit', $flag['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'flags', 'action' => 'delete', $flag['id']), array('confirm' => __('Are you sure you want to delete # %s?', $flag['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Flag'), array('controller' => 'flags', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Promotions'); ?></h3>
	<?php if (!empty($property['Promotion'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Cost'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($property['Promotion'] as $promotion): ?>
		<tr>
			<td><?php echo $promotion['id']; ?></td>
			<td><?php echo $promotion['name']; ?></td>
			<td><?php echo $promotion['cost']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'promotions', 'action' => 'view', $promotion['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'promotions', 'action' => 'edit', $promotion['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'promotions', 'action' => 'delete', $promotion['id']), array('confirm' => __('Are you sure you want to delete # %s?', $promotion['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Promotion'), array('controller' => 'promotions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
