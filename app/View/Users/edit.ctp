<div class="container">
    <div class="content">
        <h1 class="page-header"><?= __('Editar mis datos'); ?></h1>
        <?= $this->Form->create('User', array('type' => 'file')); ?>
        <?php echo $this->Form->input('id', ['type', 'hidden']); ?>
        <?php if ($this->request->data['User']['img'] == null): ?>
            <img src="https://placeholdit.imgix.net/~text?txtsize=20&bg=EEEEEE&txtclr=6E7774&txt=Imagen%20de%20Usuario&w=140&h=140" class="img-responsive img-thumbnail">
        <?php else: ?>
            <?php
            echo $this->Html->image("ai/files/users/" . $this->request->data['User']['img'], array(
                "alt" => "Imagen de perfil",
                "class" => "img-responsive img-thumbnail"
            ));
            ?>
        <?php endif; ?>
        <div>
            <input type="file" name="img-profile" id="img-profile" accept="image/*">
        </div>
        <?= $this->Form->end(); ?>

        <?= $this->Form->create('User'); ?>
        <div class="box">
            <?php
            echo $this->Form->input('id', ['type', 'hidden']);
            echo $this->Form->input('username', [
                'label' => false,
                'class' => 'form-control',
                'placeholder' => __('Correo Electrónico', true),
                'div' => 'form-group'
            ]);
            echo $this->Form->input('full_name', [
                'label' => false,
                'class' => 'form-control',
                'placeholder' => __('Nombre Completo', true),
                'div' => 'form-group'
            ]);
            echo $this->Form->input('phone', [
                'label' => false,
                'class' => 'form-control',
                'placeholder' => __('Teléfono', true),
                'div' => 'form-group'
            ]);
            $isPropietario = $this->requestAction('app/checkRole/' . $loggedUser['user_type'] . '/' . 'propietario');
            if(!$isPropietario){
                echo $this->Form->input('description', [
                    'label' => false,
                    'class' => 'form-control',
                    'placeholder' => __('Descripción', true),
                    'div' => 'form-group'
                ]);
            }
            echo $this->Form->input('password', [
                'label' => false,
                'class' => 'form-control',
                'placeholder' => __('Nueva contraseña', true),
                'div' => 'form-group',
                'value' => ''
            ]);
            echo $this->Form->input('confirm_password', [
                'label' => false,
                'class' => 'form-control',
                'placeholder' => __('Confirmar nueva contraseña', true),
                'div' => 'form-group',
                'type' => 'password'
            ]);
            ?>
        </div>
        <div class="box"> 
            <div class="center">
                <?= $this->Form->button(__('Guardar'), ['class' => 'btn btn-xl',]); ?>
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>