 <?php
$checkIfAdministrador = $this->requestAction('app/checkRole/' . $user['user_type'] . '/' . 'administrador');
$checkIfDesarrolladora = $this->requestAction('app/checkRole/' . $user['user_type'] . '/' . 'desarrolladora');
$checkIfAgencia = $this->requestAction('app/checkRole/' . $user['user_type'] . '/' . 'agencia');
$checkIfAgente = $this->requestAction('app/checkRole/' . $user['user_type'] . '/' . 'agente');
?>
<div class="container">
    <div class="content">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= __('Datos de mi cuenta'); ?></h3>
            </div>
            <div class="panel-body user-data">
                <?php
                if(isset($user['img'])){
                    echo $this->Html->image("ai/files/users/" . $user['img'], array(
                        "alt" => "Imagen de perfil",
                        "class" => "img-responsive img-thumbnail"
                    ));
                } else {
                    echo $this->Html->image("ai/no-image.jpg", array(
                        "alt" => "Imagen de perfil",
                        "class" => "img-responsive img-thumbnail"
                    ));
                } ?>
                <ul class="data">
                    <li><label><?= __('Nombre completo') ?>:</label> <?= $user['full_name'] ?></li>
                    <li><label><?= __('Correo electrónico') ?>:</label> <?= $user['username'] ?></li>
                    <li><label><?= __('Teléfono') ?>:</label> <?= $user['phone'] ?></li>
                    <li><label><?= __('Tipo de usuario') ?>:</label> <?= $this->requestAction('app/getRole/' . $user['user_type']); ?></li>
                    <li>
                        <?php if($checkIfDesarrolladora): ?>
                            <a style="max-width: 272px;" href="<?= $this->Html->url(['controller' => 'users', 'action' => 'payment']); ?>" class="btn">
                                <i class="fa fa-pencil"></i> <?= __('Destacar desarrolladora') ?>
                            </a>
                        <?php endif; ?>
                        <?php if($checkIfAgencia || $checkIfAgente): ?>
                            <a style="max-width: 272px;" href="<?= $this->Html->url(['controller' => 'users', 'action' => 'payment']); ?>" class="btn">
                                <i class="fa fa-pencil"></i> <?= __('Destacar mi agencia') ?>
                            </a>
                        <?php endif; ?>
                    </li>
                </ul>
                <a href="<?= $this->Html->url(['controller' => 'users', 'action' => 'edit/' . $user['id']]); ?>" class="btn">
                    <i class="fa fa-pencil"></i> <?= __('Editar Perfil') ?>
                </a>
            </div>
        </div>
        <?php if (!$checkIfAdministrador && !$checkIfDesarrolladora) { ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= __('Mis Propiedades'); ?></h3>
                </div>
                <div class="panel-body">
                    <?php if (count($properties) > 0): ?>
                        <table class="table property-table">
                            <thead>
                                <tr>
                                    <th><?= __('Propiedad'); ?></th>
                                    <th><?= __('Tipo'); ?></th>
                                    <th><?= __('Categoría'); ?></th>
                                    <th><?= __('Estado'); ?></th>
                                    <th><?=__('Opciones');?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($properties as $property) { ?>
                                    <tr>
                                        <td class="property-table-info">
                                            <div class="pt-img">
                                                <?php if(isset($property['Property']['outstanding'])): ?>
                                                    <?php 
                                                        $date = new DateTime($property['Property']['created']);
                                                        $monts = $property['Property']['months'];
                                                        $expiration = strtotime('+' . $monts . ' month', strtotime($date->format('d-m-Y')));
                                                        $expiration = date ('d-m-Y' , $expiration);
                                                        if(strtotime($expiration) > strtotime(date('d-m-Y'))):
                                                    ?>
                                                            <?php if($property['Property']['outstanding'] == 2): ?>
                                                                <div class="featured-icon"></div>
                                                            <?php elseif ($property['Property']['outstanding'] == 1): ?>
                                                                <div class="superoffer-icon" ></div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                <?php endif; ?>
                                                        
                                                <?php if (isset($property['PropertiesImages'][0]['rute'])) { ?>
                                                    <?= $this->Html->image($property['PropertiesImages'][0]['rute']) ?>
                                                <?php } else { ?>
                                                    <?= $this->Html->image('ai/no-image.jpg') ?>
                                                <?php } ?>
                                            </div>
                                            <div class="property-table-info-content">
                                                <div class="property-table-info-content-title">
                                                    <?= $property['Property']['title'] ?>
                                                </div>
                                            </div>
                                        </td>
                                        <td><?= $property['PropertyType']['name'] ?></td>
                                        <td><?= $property['Categories']['name'] ?></td>
                                        <td class="info">
                                            <?php
                                            if ($property['Property']['status'] == 0) {
                                                echo __('Sin publicar');
                                            } else if ($property['Property']['status'] == 1) {
                                                $text = __('Publicada');
                                                switch ($property['Property']['outstanding']){
                                                    case 0:
                                                        $text .= ' ' . DS . ' Sin destacarse.';
                                                        break;
                                                    case 1:
                                                        $date = new DateTime($property['Property']['created']);
                                                        $monts = $property['Property']['months'];
                                                        $expiration = strtotime('+' . $monts . ' month', strtotime($date->format('d-m-Y')));
                                                        $expiration = date ('d-m-Y' , $expiration);
                                                        $text .= ' ' . DS . ' Destacada, expira el: ' . $expiration . '.';
                                                        break;
                                                    case 2:
                                                        $date = new DateTime($property['Property']['created']);
                                                        $monts = $property['Property']['months'];
                                                        $expiration = strtotime('+' . $monts . ' month', strtotime($date->format('d-m-Y')));
                                                        $expiration = date ('d-m-Y' , $expiration);
                                                        $text .= ' ' . DS . ' Destacada como Super Oferta, expira el: ' . $expiration . '.';
                                                        break;
                                                    default:
                                                        $text .= '.';
                                                        break;
                                                }
                                            }
                                            echo $text;
                                            ?>
                                        </td>
                                        <td>
                                            <a href="<?= $this->Html->url(['controller' => 'properties', 'action' => 'edit/' . $property['Property']['id']]) ?>" class="btn">
                                                <i class="fa fa-pencil"></i> <?= __('Editar'); ?>
                                            </a>
                                            <?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'properties', 'action' => 'delete', $property['Property']['id']), array('confirm' => __('¿Estás seguro de que quieres eliminar la propiedad: %s?', $property['Property']['title']),'class' => 'btn')); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="cake-pagination">
                            <?php
                            echo $this->Paginator->prev('< ', array(), null, array('class' => 'prev disabled'));
                            echo $this->Paginator->numbers(array('separator' => ''));
                            echo $this->Paginator->next(' >', array(), null, array('class' => 'next disabled'));
                            ?>
                        </div>
                        <br><br>
                        <div class="cake-pagination">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, empezando en el registro {:start}, y terminando en el {:end}')
                            ));
                            ?>	
                        </div>
                    <?php else: ?>
                        <?= $this->Html->link(__("Publicar propiedad"), array('controller' => 'properties', 'action' => 'add')) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php } else if ($checkIfDesarrolladora) { ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= __('Mis Proyectos'); ?></h3>
                </div>
                <div class="panel-body">
                    <?php if (count($projects) > 0): ?>
                        <table class="table projects-table">
                            <thead>
                                <tr>
                                    <th><?= __('Proyecto'); ?></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($projects as $project) { ?>
                                    <tr>
                                        <td><?= $project['Project']['name'] ?></td>
                                        <td>
                                            <a href="<?= $this->Html->url(['controller' => 'projects', 'action' => 'edit/' . $project['Project']['id']]) ?>" class="btn">
                                                <i class="fa fa-pencil"></i> <?= __('Editar') ?>
                                            </a>
                                            <a href="<?= $this->Html->url(['controller' => 'projects', 'action' => 'delete/' . $project['Project']['id']]) ?>" class="btn">
                                                <i class="fa fa-trash"></i> <?= __('Borrar'); ?>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="cake-pagination">
                            <?php
                            echo $this->Paginator->prev('< ', array(), null, array('class' => 'prev disabled'));
                            echo $this->Paginator->numbers(array('separator' => ''));
                            echo $this->Paginator->next(' >', array(), null, array('class' => 'next disabled'));
                            ?>
                        </div>
                        <br><br>
                        <div class="cake-pagination">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, empezando en el registro {:start}, y terminando en el {:end}')
                            ));
                            ?>	
                        </div>
                    <?php else: ?>
                        <?= $this->Html->link(__("Publicar proyecto"), array('controller' => 'projects', 'action' => 'add')) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>