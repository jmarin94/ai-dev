<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <h3 class="page-header"><?= __("Últimas propiedades") ?></h3>

            <div class="row">
                <?php foreach($properties as $property): ?>
                    <div class="col-sm-4 col-md-6 col-lg-4">
                        <div class="property-preview">
                            <div class="property-preview-image">
                                <a href="<?=$this->Html->url(array('controller' => 'properties', 'action' => 'edit', $property['Property']['id']))?>">
                                    <?php if (isset($property['PropertiesImages'][0]['rute'])) { ?>
                                        <?=$this->Html->image($property['PropertiesImages'][0]['rute'])?>
                                    <?php } else { ?>
                                     <?= $this->Html->image('ai/no-image.jpg') ?>
                                    <?php }  ?>
                                </a>
                            </div><!-- /.property-preview-image -->

                            <div class="property-preview-content">
                                <h2><a href="<?=$this->Html->url(array('controller' => 'properties', 'action' => 'edit', $property['Property']['id']))?>"><?=$property['Categories']['name']?></a></h2>
                                <a href="<?=$this->Html->url(array('controller' => 'properties', 'action' => 'edit', $property['Property']['id']))?>" class="property-preview-action-secondary">Edit</a>
                            </div><!-- /.property-preview-content -->
                        </div><!-- /.property-preview -->
                    </div><!-- /.col-* -->
                <?php endforeach; ?>
            </div><!-- /.row -->
            
            <?= $this->Html->link('Ver todas las propiedades', array('controller' => 'properties', 'action' => 'index')); ?>
        </div><!-- /.col-* -->
        <div class="col-sm-12 col-md-6">
            <h3 class="page-header"><?= __("Últimos proyectos") ?></h3>

            <div class="row">
                <?php foreach($projects as $project):  ?>
                    <div class="col-sm-4 col-md-6 col-lg-4">
                        <div class="property-preview">
                            <div class="property-preview-image">
                                <a href="<?=$this->Html->url(array('controller' => 'properties', 'action' => 'edit', $project['Project']['id']))?>">
                                    <?php if (isset($project['ProjectsImage'][0]['rute'])) { ?>
                                        <?=$this->Html->image($project['ProjectsImage'][0]['rute'])?>
                                    <?php } else { ?>
                                        <?= $this->Html->image('ai/no-image.jpg') ?>
                                    <?php }  ?>
                                </a>
                            </div><!-- /.property-preview-image -->

                            <div class="property-preview-content">
                                <h2><a href="<?=$this->Html->url(array('controller' => 'projects', 'action' => 'edit', $project['Project']['id']))?>"><?=$project['Project']['name']?></a></h2>
                                <a href="<?=$this->Html->url(array('controller' => 'projects', 'action' => 'edit', $project['Project']['id']))?>" class="property-preview-action-secondary">Edit</a>
                            </div><!-- /.property-preview-content -->
                        </div><!-- /.property-preview -->
                    </div><!-- /.col-* -->
                <?php endforeach; ?>
            </div><!-- /.row -->
            
            <?= $this->Html->link('Ver todos los proyectos', array('controller' => 'projects', 'action' => 'index')); ?>
        </div><!-- /.col-* -->

    </div><!-- /.row -->
</div><!-- /.container-fluid -->