<div class="container">
    <div class="content m-top2">
        <div class="col-sm-4 col-sm-offset-4">
            <h2 class="page-header center"><?= __('Inicia Sesión'); ?></h2>
            <div class="box">
              <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <i class="fa fa-exclamation-circle"></i>
                  <strong><?= __('Estimado usuario, debido a una restructuración en nuestro sitio web, antes de iniciar sesión, debe restaurar su contraseña, por favor haga click'); ?>&nbsp; <a href="<?=$this->Html->url(array('controller' => 'users', 'action' => 'forgot'));?>"><?=__("aquí")?></a></strong> <?= $message ?>
              </div>
                <?=$this->Form->create('User');?>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-envelope"></i>
                        </span>
                        <?php
                            echo $this->Form->input('username', array(
                                'label'         => false,
                                'div'           => false,
                                'class'         => 'form-control',
                                'placeholder'   => 'Correo electrónico',
                                'required'      => true
                            ));
                        ?>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-unlock-alt"></i>
                        </span>
                        <?php
                            echo $this->Form->input('password', array(
                                'label'         => false,
                                'div'           => false,
                                'class'         => 'form-control',
                                'placeholder'   => 'Contraseña',
                                'required'      => true
                            ));
                        ?>
                    </div>
                    <?=$this->Form->button(__('Iniciar Sesión'), array('class' => 'btn'));?>
                    <?=$this->Form->end();?>
                <a href="<?=$this->Html->url(array('controller' => 'users', 'action' => 'forgot'));?>" style="float: right; margin-top: -20px"><?=__('¿Olvidó su contraseña?');?></a>
            </div><!-- /.row -->
        </div><!-- /.col-* -->
    </div>
</div>
