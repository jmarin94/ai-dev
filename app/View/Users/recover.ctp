<div class="col-sm-4 col-sm-offset-4">
    <h2 class="page-header center"><?= __('Ingrese una nueva contraseña'); ?></h2>
    <div class="box">
        <?=$this->Form->create('User');?>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">
                    <i class="fa fa-unlock-alt"></i>
                </span>
                <?=$this->Form->input('password',['label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Nueva Contraseña')])?>
            </div>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">
                    <i class="fa fa-unlock-alt"></i>
                </span>
                <?=$this->Form->input('confirm_password',['label' => false, 'type' => 'password', 'div' => false, 'class' => 'form-control', 'placeholder' => __('Confirme la Contraseña')])?>
            </div>
            <?=$this->Form->input('id', ['type' => 'hidden', 'value' => $user['User']['id']]);?>
            <?=$this->Form->input('token', ['type' => 'hidden', 'value' => ""]);?>
            <?=$this->Form->button(__('Cambiar Contraseña'), ['class' => 'btn']);?>
        <?=$this->Form->end();?>
    </div>
</div>
