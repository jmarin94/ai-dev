<div class="container">
    <div class="content">
        <div class="content col-sm-12 col-md-12">
            <h1 class="page-header"><?= __("Agentes y Agencias") ?></h1>
            <?php foreach ($agencies as $agency): ?>
                <?php $url = Router::url('/').'ver/agencia/'.$agency['User']['id'].'-' .strtolower(Inflector::slug($agency['User']['full_name'], '-')); ?>
                <div class="agency-row">
                    <div class="row">
                        <div class="agency-row-image col-sm-3">
                            <a href="<?= $url; ?>">
                                <div class="nailthumb-container square-thumb agencies-list">
                                    <?php if(isset($agency['User']['img'])): ?>
                                        <?= $this->Html->image("ai/files/users/" . $agency['User']['img']); ?>
                                    <?php else: ?>
                                        <?= $this->Html->image("ai/no-image.jpg", array("alt" => __("Imagen no disponible"))); ?>
                                    <?php endif; ?>
                                </div>
                            </a>
                        </div><!-- /.agency-row-image -->

                        <div class="agency-row-content col-sm-5">
                            <h2 class="agency-row-title"><a href="<?= $url; ?>"</a></h2>
                            <?php $propertiesQty = $this->requestAction('app/countProperties/' . $agency['User']['id']); ?>
                            <div class="agency-row-subtitle"><?= $propertiesQty; ?>&nbsp;<?= $propertiesQty == 1 ? __("propiedad") : __("propiedades") ?></div>
                            <hr>
                            <?php if (isset($agency['User']['description']) && $agency['User']['description'] != ""): ?>
                                <p>
                                    <?= $agency['User']['description']; ?>
                                </p>
                            <?php endif; ?>
                        </div>
                        <div class="agency-row-info col-sm-4">
                            <ul>
                                <li><?= $agency['User']['phone'] ?></li>
                                <li><a href="mailto:<?= $agency['User']['username'] ?>"><?= $agency['User']['username'] ?></a></li>
                            </ul>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.agency-row -->
            <?php endforeach; ?>

            <div class="cake-pagination">
                <?php
                echo $this->Paginator->prev('< ', array(), null, array('class' => 'prev disabled'));
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next(' >', array(), null, array('class' => 'next disabled'));
                ?>
            </div>
            <br><br>
            <div class="cake-pagination">
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, empezando en el registro {:start}, y terminando en el {:end}')
                ));
                ?>	
            </div>
            
        </div><!-- /.content -->
    </div>
</div>