<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <h2 class="page-header center"><?= __('Recupera tu contraseña'); ?></h2>
                <div class="box">
                    <?= $this->Form->create('User'); ?>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">
                            <i class="fa fa-envelope"></i>
                        </span>
                        <?= $this->Form->input('username', array('label' => false, 'div' => false, 'required' => true, 'class' => 'form-control', 'placeholder' => __('Correo electrónico'))) ?>
                    </div>
                    <?= $this->Form->button(__('Recuperar contraseña'), array('class' => 'btn')); ?>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>