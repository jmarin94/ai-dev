<div class="content">
    <div class="container">
        <?php if(!isset($status)):  ?>
            <div class="feature col-sm-12">
                <h3><?=__("Pago con Paypal");?></h3>

                <p><?=__("Para comprar tu destacado, favor pulse el botón 'Comprar ahora' y será redirigido a la página de Paypal.");?></p>
                <div id="months-for-outstanding">
                    <div class="form-group">
                        <select class="form-control" id="months" name="data[Project][months]">
                            <?php for($i = 1; $i <= 12; $i++): ?>
                                <option value="<?=$i?>"><?= $i == 1 ? "1 mes" : ($i . " meses"); ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>
                    <div class="form-group">
                         <label id="amount"><?=__("Se le aplicarán")?>&nbsp;<b><?=__("$ ") . $data['paymentTotal'] ?></b>&nbsp;<?=__("por destacar su proyecto durante 1 mes.")?></label>
                    </div>
                </div>
            </div>
            <form class="col-sm-4" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="business" value="dsibaja@americainmobiliaria.com">
                <input type="hidden" name="item_name" value="Destacamiento de cuenta (agencia o desarrolladora)">
                <input type="hidden" name="item_number" value="1">
                <input type="hidden" id="paymentTotal" name="amount" value="<?php echo $data['paymentTotal'] ?>">
                <input type="hidden" name="no_shipping" value="0">
                <input type="hidden" name="no_note" value="1">
                <input type="hidden" name="currency_code" value="USD">
                <input type="hidden" name="lc" value="AU">
                <input type="hidden" name="notify_url" value="<?=Router::url(array('controller' => 'users', 'action' => 'payment', 'notify'), true);?>">
                <input type="hidden" name="return" value="<?=Router::url(array('controller' => 'users', 'action' => 'payment', 'success'), true);?>">
                <input type="hidden" name="cbt" value="Return to The Store">
                <input type="hidden" name="cancel_return" value="<?=Router::url(array('controller' => 'users', 'action' => 'payment', 'cancel'), true);?>">
                <input type="hidden" name="bn" value="PP-BuyNowBF">
                <input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal. La forma rápida y segura de pagar en Internet.">
                <img alt="" border="0" src="https://www.paypalobjects.com/es_ES/i/scr/pixel.gif" width="1" height="1">
            </form>
            <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'payment', 'cancel')) ?>" class="btn"><?=__("Cancelar")?></a>
        <?php else: ?>
            <?php if($status == 'cancel'): ?>
                <div class="feature col-sm-12">
                    <h3><?=__("Pago cancelado");?></h3>

                    <p><?=__("El pago no ha sido completado, su cuenta no ha sido destacada.");?></p>
                </div>
            <?php elseif($status == 'sucess'): ?>
                <div class="feature col-sm-12">
                    <h3><?=__("Pago procesado con Paypal");?></h3>

                    <p><?=__("El pago ha sido procesado, su cuenta será destacada.");?></p>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#months").change(function(){
            var price = "<?php echo $data['paymentTotal']; ?>"
            var val = $(this).find("option:selected").val();
            var meses = val == 1 ? "1 mes" : (val + " meses");
            var total = val*parseFloat(price);
            var text  = "Se le aplicarán <b>$ " + total + "</b> por destacar su cuenta durante " + meses;
            $("#paymentTotal").val(total);
            $("#amount").html(text);
        });
    });
</script>
