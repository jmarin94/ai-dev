<div class="container">
    <div class="row">
        <div class="content col-sm-8 col-md-9">
            <h1 class="page-header"><?= $user['User']['full_name'] ?></h1>

            <?php if(isset($user['User']['description']) && $user['User']['description'] != ""): ?>
                <p class="text mb30"><?=$user['User']['description']?></p>
            <?php endif; ?>

            <div class="module">
                <div class="module-content">
                    <div class="agent-card">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 mb30">
                                <a href="javascript:void(8)" class="agent-card-image">
                                    <div class="nailthumb-container square-thumb property-owner">
                                        <?php if(isset($user['User']['img'])): ?>
                                            <?=$this->Html->image("ai/files/users/" . $user['User']['img'], array(
                                                "alt" => "Imagen de perfil",
                                                "class" => "img-responsive img-thumbnail"
                                            ));?>
                                        <?php else: ?>
                                            <?= $this->Html->image("ai/no-image.jpg", array("alt" => __("Imagen no disponible"))); ?>
                                        <?php endif; ?>
                                    </div>
                                </a><!-- /.agent-card-image -->
                            </div>

                            <div class="col-sm-12 col-md-3">
                                <h2><?=__("Información del anunciante")?></h2>

                                <div class="agent-card-info">
                                    <?php ?>
                                    <ul>
                                        <?php if(isset($user['User']['phone']) && $user['User']['phone'] != ""): ?>
                                            <li><i class="fa fa-phone"></i><?=$user['User']['phone']?></li>
                                        <?php endif; ?>
                                        <?php if(isset($user['User']['username'])): ?>
                                            <li><i class="fa fa-at"></i> <a href="mailto:<?=$user['User']['username']?>"><?=$user['User']['username']?></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div><!-- /.agent-card-info -->
                            </div>

                            <div class="col-sm-12 col-md-5 mb30">
                                <h2><?=__("Contáctalo")?></h2>

                                <div class="agent-card-form">
                                    <form method="post" action="#">
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="Nombre">
                                        </div><!-- /.form-group -->

                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="E-mail">
                                        </div><!-- /.form-group -->

                                        <div class="form-group">
                                            <textarea class="form-control" placeholder="Mensaje"></textarea>
                                        </div><!-- /.form-group -->

                                        <button class="btn" type="submit"><?=__("Enviar mensaje");?></button>
                                    </form>
                                </div><!-- /.agent-card-form -->
                            </div>
                        </div>
                    </div><!-- /.agent-card-->
                </div><!-- /.module-content -->
            </div><!-- /.module -->

            
            <?php if ($user['User']['user_type'] != 1 && $user['User']['user_type'] != 6): ?>
                <h2 class="page-header"><?=__("Nuestras propiedades")?></h2>
                <?php if(count($properties) > 0): ?>
                    <div class="row">
                        <?php foreach($properties as $property): 
                            $url = Router::url('/').'detalle/propiedad/'.$property['Property']['id'].'-' .strtolower(Inflector::slug($property['Property']['title'], '-'));
                            $outstanding = "";
                            if(isset($property['Property']['outstanding'])): 
                                $date = new DateTime($property['Property']['created']);
                                $monts = $property['Property']['months'];
                                $expiration = strtotime('+' . $monts . ' month', strtotime($date->format('d-m-Y')));
                                $expiration = date ('d-m-Y' , $expiration);

                                if(strtotime($expiration) > strtotime(date('d-m-Y'))):
                                    if($property['Property']['outstanding'] == 2): 
                                        $outstanding = "property-featured";
                                    elseif ($property['Property']['outstanding'] == 1):
                                        $outstanding = "property-super-offer";
                                    endif; 
                                endif; 
                            endif; 
                        ?> 
                            <div class="col-sm-6 col-md-4">
                                <div class="property-box">
                                    <div class="property-box-image">
                                        <a href="<?=$url;?>">
                                            <div>
                                                <?php if($outstanding != "" && $outstanding == "property-featured"): ?>
                                                    <div class="featured-icon"></div>
                                                <?php elseif($outstanding != "" && $outstanding == "property-super-offer"): ?>
                                                    <div class="superoffer-icon" ></div>
                                                <?php endif; ?>
                                                <?php if(isset($property['PropertiesImages'][0]['rute'])): ?>
                                                    <?= $this->Html->image($property['PropertiesImages'][0]['rute']) ?>
                                                <?php else: ?>
                                                    <?= $this->Html->image("ai/no-image.jpg" , array("alt" => __("Imagen no disponible"))); ?>
                                                <?php endif; ?>
                                            </div>

                                            <span class="property-box-excerpt">
                                                <?php if(isset($property['Flag']) && count($property['Flag']) > 0): ?>
                                                    <ul class="properties-flags-home">
                                                        <?php foreach($property['Flag'] as $flag): ?>
                                                            <li><?= $flag['name']; ?></li>
                                                        <?php endforeach;; ?>
                                                    </ul>
                                                <?php else: ?>
                                                    <?= $property['Property']['description'] ?>
                                                <?php endif; ?>
                                            </span>
                                        </a>
                                    </div><!-- /.property-image -->

                                    <div class="property-box-content">
                                        <div class="property-box-meta">
                                            <div class="property-box-meta-item">
                                                <span><?=__("Habitaciones")?></span>
                                                <strong><?=$property['Property']['rooms']?></strong>
                                            </div><!-- /.property-box-meta-item -->

                                            <div class="property-box-meta-item">
                                                <span><?=__("Baños")?></span>
                                                <strong><?=$property['Property']['bathrooms']?></strong>
                                            </div><!-- /.property-box-meta-item -->

                                            <div class="property-box-meta-item">
                                                <span><?=__("Garages")?></span>
                                                <strong><?=$property['Property']['garages']?></strong>
                                            </div><!-- /.property-box-meta-item -->
                                        </div><!-- /.property-box-meta -->
                                    </div><!-- /.property-box-content -->

                                    <div class="property-box-bottom">
                                        <div class="property-box-price">
                                            <?=$property['Cities']['name']?>
                                        </div><!-- /.property-box-price -->

                                        <a href="<?=$this->Html->url(array('controller' => 'properties', 'action' => 'view', $property['Property']['id']))?>" class="property-box-view">
                                            <?= __("Ver detalle") ?>
                                        </a><!-- /.property-box-view -->
                                    </div><!-- /.property-box-bottom -->
                                </div><!-- /.property-box -->
                            </div>
                        <?php endforeach; ?>
                    </div><!-- /.row -->
                    <div class="cake-pagination">
                        <?php
                        echo $this->Paginator->prev('< ', array(), null, array('class' => 'prev disabled'));
                        echo $this->Paginator->numbers(array('separator' => ''));
                        echo $this->Paginator->next(' >', array(), null, array('class' => 'next disabled'));
                        ?>
                    </div>
                    <br><br>
                    <div class="cake-pagination">
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, empezando en el registro {:start}, y terminando en el {:end}')
                        ));
                        ?>	
                    </div>
                <?php else: ?>
                    <div class="feature center col-sm-4">
                        <i class="feature-icon fa fa-folder-open-o"></i>
                        <h3><?=__("No hay propiedades por mostrar");?></h3>
                    </div>
                <?php endif; ?>
            <?php elseif($user['User']['user_type'] == 6): ?>
                <h2 class="page-header"><?=__("Nuestros proyectos")?></h2>
                <?php if(count($projects) > 0): ?>
                    <div class="row">
                        <?php foreach($projects as $project): ?>
                            <?php $url = Router::url('/').'detalle/proyecto/'.$project['Project']['id'].'-' .strtolower(Inflector::slug($project['Project']['name'], '-')); ?>
                            <div class="col-sm-6 col-md-4">
                                <div class="property-box">
                                    <div class="property-box-image">
                                        <a href="<?=$url;?>">
                                            <div>
                                                <?php if(isset($project['ProjectsImage'][0]['rute'])): ?>
                                                    <?= $this->Html->image($project['ProjectsImage'][0]['rute']) ?>
                                                <?php else: ?>
                                                    <?= $this->Html->image("ai/no-image.jpg" , array("alt" => __("Imagen no disponible"))); ?>
                                                <?php endif; ?>
                                            </div>

                                            <span class="property-box-excerpt">
                                                <?= $project['Project']['description'] ?>
                                            </span>
                                        </a>
                                    </div><!-- /.property-image -->

                                    <div class="property-box-bottom">
                                        <div class="property-box-price">
                                            <?= $project['Project']['name'] ?>
                                        </div><!-- /.property-box-price -->
                                        <br><br>
                                        <div>
                                            <a href="<?=$this->Html->url(array('controller' => 'projects', 'action' => 'view', $project['Project']['id']))?>" class="btn">
                                                <?= __("Ver detalle") ?>
                                            </a><!-- /.property-box-view -->
                                        </div>
                                    </div><!-- /.property-box-bottom -->
                                </div><!-- /.property-box -->
                            </div>
                        <?php endforeach; ?> 
                    </div><!-- /.row -->
                    <div class="cake-pagination">
                        <?php
                        echo $this->Paginator->prev('< ', array(), null, array('class' => 'prev disabled'));
                        echo $this->Paginator->numbers(array('separator' => ''));
                        echo $this->Paginator->next(' >', array(), null, array('class' => 'next disabled'));
                        ?>
                    </div>
                    <br><br>
                    <div class="cake-pagination">
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, empezando en el registro {:start}, y terminando en el {:end}')
                        ));
                        ?>	
                    </div>
                <?php else: ?>
                    <div class="feature center col-sm-4">
                        <i class="feature-icon fa fa-folder-open-o"></i>
                        <h3><?=__("No hay proyectos por mostrar");?></h3>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
                    
        </div><!-- /.content -->
        <div class="col-sm-4 col-md-3">
            <?= $this->element('Page/ads-sidebar') ?>
        </div>
    </div><!-- /.row -->
</div><!-- /.container -->