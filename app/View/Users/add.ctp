<div class="container">
    <div class="content">
        <?=$this->Form->create('User');?>
        <div class="row">
            <div class="col-lg-4">
                <h1 class="page-header center"><?=__('Indica tus datos');?></h1>
                <div class="box register-form">
                    <?php
                        echo $this->Form->input('username', array(
                            'label'         => false,
                            'class'         => 'form-control',
                            'placeholder'   => __('Correo electrónico', true),
                            'div'           => 'form-group',
                            'required'      => true
                        ));
                        echo $this->Form->input('full_name', array(
                            'label'         => false,
                            'class'         => 'form-control',
                            'placeholder'   => __('Nombre completo', true),
                            'div'           => 'form-group',
                            'required'      => true
                        ));
                        echo $this->Form->input('phone', array(
                            'label'         => false,
                            'class'         => 'form-control',
                            'placeholder'   => __('Teléfono', true),
                            'div'           => 'form-group',
                            'required'      => true
                        ));
                        echo $this->Form->input('password', array(
                            'label'         => false,
                            'class'         => 'form-control',
                            'placeholder'   => __('Contraseña', true),
                            'div'           => 'form-group',
                            'required'      => true
                        ));
                        echo $this->Form->input('confirm_password', array(
                            'label'         => false,
                            'class'         => 'form-control',
                            'type'          => 'password',
                            'placeholder'   => __('Confirma tu contraseña', true),
                            'div'           => 'form-group',
                            'required'      => true
                        ));
                    ?>
                </div>
            </div>
            <div class="col-lg-8">
                <h1 class="page-header center"><?=__('Elige tu tipo de cuenta');?></h1>
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="property-medium type-user-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="col-sm-9">
                                    <div class="property-medium-content">
                                        <h3 class="property-medium-title"><?=__('AGENCIA');?></h3>
                                        <p class="register-description"><?=__('Exponga su cartera de propiedades bajo un mercado exclusivo de Bienes Raíces');?></p>
                                        <p class="register-checkboxes">
                                            <input type="checkbox" class="select-typeUser" id="type-agente" value="<?=array_search('agente', $userTypes);?>">
                                            <label for="type-agente"><?=__('Agente');?></label>
                                            &nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" class="select-typeUser" id="type-agencia" value="<?=array_search('agencia', $userTypes);?>">
                                            <label for="type-agencia"><?=__('Agencia');?></label> 
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12 col-md-6">
                        <div class="property-medium type-user-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-university"></i>
                                </div>
                                <div class="col-sm-9">
                                    <div class="property-medium-content">
                                        <h3 class="property-medium-title"><?=__('BANCOS');?></h3>
                                        <p class="register-description"><?=__('Hemos creado un directorio exclusivo para bienes adjudicados por Bancos');?></p>
                                        <p class="register-checkboxes">
                                            <input type="checkbox" class="select-typeUser" id="type-banco" value="<?=array_search('banco', $userTypes);?>">
                                            <label for="type-banco"><?=__('Banco');?></label> 
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <div class="property-medium type-user-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-key"></i>
                                </div>
                                <div class="col-sm-9">
                                    <div class="property-medium-content">
                                        <h3 class="property-medium-title"><?=__('PROPIETARIO');?></h3>
                                        <p class="register-description"><?=__('Exponga su vivienda, lote, quinta con nosotros, y serán expuestas bajo un mercado exclusivo de Bienes Raíces');?></p>
                                        <p class="register-checkboxes">
                                            <input type="checkbox" class="select-typeUser" id="type-propietario" value="<?=array_search('propietario', $userTypes);?>">
                                            <label for="type-propietario"><?=__('Propietario');?></label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <div class="property-medium type-user-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-truck"></i>
                                </div>
                                <div class="col-sm-9">
                                    <div class="property-medium-content">
                                        <h3 class="property-medium-title"><?=__('DESARROLLADORA');?></h3>
                                        <p class="register-description"><?=__('Publica todos tus proyectos, ilimitada cantidad de fotos, planos y videos.');?></p>
                                        <p class="register-checkboxes">
                                            <input type="checkbox" class="select-typeUser" id="type-desarrolladora" value="<?=array_search('desarrolladora', $userTypes);?>">
                                            <label for="type-desarrolladora"><?=__('Desarrolladora');?></label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box col-md-12"> 
            <div class="center">
                <?=$this->Form->input('user_type', array('type' => 'hidden'))?>
                <?=$this->Form->button(__('Regístrate'), array('class' => 'btn btn-xl'));?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>