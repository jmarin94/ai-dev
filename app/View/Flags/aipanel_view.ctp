<div class="flags view">
<h2><?php echo __('Flag'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($flag['Flag']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($flag['Flag']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cost'); ?></dt>
		<dd>
			<?php echo h($flag['Flag']['cost']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Flag'), array('action' => 'edit', $flag['Flag']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Flag'), array('action' => 'delete', $flag['Flag']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $flag['Flag']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Flags'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Flag'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Properties'); ?></h3>
	<?php if (!empty($flag['Property'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Type Properties Id'); ?></th>
		<th><?php echo __('Categories Id'); ?></th>
		<th><?php echo __('Countries Id'); ?></th>
		<th><?php echo __('Provinces Id'); ?></th>
		<th><?php echo __('Cities Id'); ?></th>
		<th><?php echo __('Lot Area'); ?></th>
		<th><?php echo __('Construction Area'); ?></th>
		<th><?php echo __('Rooms'); ?></th>
		<th><?php echo __('Bathrooms'); ?></th>
		<th><?php echo __('Garages'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Outstanding'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('D S Price'); ?></th>
		<th><?php echo __('D A Price'); ?></th>
		<th><?php echo __('C S Price'); ?></th>
		<th><?php echo __('C A Price'); ?></th>
		<th><?php echo __('Users Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($flag['Property'] as $property): ?>
		<tr>
			<td><?php echo $property['id']; ?></td>
			<td><?php echo $property['title']; ?></td>
			<td><?php echo $property['property_type']; ?></td>
			<td><?php echo $property['categories_id']; ?></td>
			<td><?php echo $property['countries_id']; ?></td>
			<td><?php echo $property['provinces_id']; ?></td>
			<td><?php echo $property['cities_id']; ?></td>
			<td><?php echo $property['lot_area']; ?></td>
			<td><?php echo $property['construction_area']; ?></td>
			<td><?php echo $property['rooms']; ?></td>
			<td><?php echo $property['bathrooms']; ?></td>
			<td><?php echo $property['garages']; ?></td>
			<td><?php echo $property['description']; ?></td>
			<td><?php echo $property['status']; ?></td>
			<td><?php echo $property['outstanding']; ?></td>
			<td><?php echo $property['created']; ?></td>
			<td><?php echo $property['d_s_price']; ?></td>
			<td><?php echo $property['d_a_price']; ?></td>
			<td><?php echo $property['c_s_price']; ?></td>
			<td><?php echo $property['c_a_price']; ?></td>
			<td><?php echo $property['users_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'properties', 'action' => 'view', $property['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'properties', 'action' => 'edit', $property['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'properties', 'action' => 'delete', $property['id']), array('confirm' => __('Are you sure you want to delete # %s?', $property['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
