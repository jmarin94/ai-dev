<div class="flags form">
<?php echo $this->Form->create('Flag'); ?>
	<fieldset>
		<legend><?php echo __('Aipanel Add Flag'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('cost');
		//echo $this->Form->input('Property');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Flags'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
	</ul>
</div>
