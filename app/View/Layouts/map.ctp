<!DOCTYPE html>
<html>
    <head>
        <?=$this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>americainmobiliaria</title>
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700&amp;subset=latin,latin-ext">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <?= $this->Html->css('Font-Awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('bootstrap-select/dist/css/bootstrap-select.min.css') ?>
        <?= $this->Html->css('bootstrap-fileinput/css/fileinput.min.css') ?>
        <?= $this->Html->css('nvd3/nv.d3.min.css') ?>
        <?= $this->Html->css('OwlCarousel/owl-carousel/owl.carousel.css') ?>
        <?= $this->Html->css('realsite.css') ?>
        
        
        <?= $this->Html->script('jquery.js') ?>
        <?= $this->Html->script('jquery-transit/jquery.transit.js') ?>
        
        <?= $this->Html->script('bootstrap/assets/javascripts/bootstrap/dropdown.js') ?>
        <?= $this->Html->script('bootstrap/assets/javascripts/bootstrap/collapse.js') ?>
        <?= $this->Html->script('bootstrap-select/dist/js/bootstrap-select.min.js') ?>
        <?= $this->Html->script('bootstrap-fileinput/js/fileinput.min.js') ?>
        
        <?= $this->Html->script('autosize/jquery.autosize.js') ?>
        <?= $this->Html->script('isotope/dist/isotope.pkgd.min.js') ?>
        <?= $this->Html->script('OwlCarousel/owl-carousel/owl.carousel.min.js') ?>
        <?= $this->Html->script('jquery.scrollTo/jquery.scrollTo.min.js') ?>
        
        <script src="http://maps.googleapis.com/maps/api/js?libraries=weather,geometry,visualization,places,drawing&amp;sensor=false" type="text/javascript"></script>
        <?= $this->Html->script('jquery-google-map/infobox.js') ?>
        <?= $this->Html->script('jquery-google-map/markerclusterer.js') ?>
        <?= $this->Html->script('jquery-google-map/jquery-google-map.js') ?>
        
        <?= $this->Html->script('nvd3/lib/d3.v3.js') ?>
        <?= $this->Html->script('nvd3/lib/d3.v3.js') ?>
        <?= $this->Html->script('nvd3/nv.d3.min.js') ?>
        
        <?= $this->Html->script('realsite.js') ?>
        <?= $this->Html->script('charts.js') ?>
        <?= $this->Html->script('map.js') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
    </head>
    <body>

        <div class="page-wrapper">
            <div class="header header-standard">
                
                <div class="header-topbar">
                    <div class="container">
                        <div class="header-topbar-left">
                            <!--<ol class="breadcrumb">
                                <li><a href="properties-submit.html#">Home</a></li>
                                <li><a href="properties-submit.html#">Properties</a></li>
                                <li class="active">Wardrobe Street</li>
                            </ol>-->
                        </div><!-- /.header-topbar-left -->

                        <div class="header-topbar-right">
                            

                            <ul class="header-topbar-social ml30">
                                <?php if($loggedUser != null){ ?>
                                    <li>
                                        <a href="<?=$this->Html->url(['controller' => 'users', 'action' => 'dashboard']);?>">
                                            <i class="fa fa-user"></i>
                                            <?=$loggedUser['full_name']?>
                                        </a>
                                    </li>
                                <?php } else { ?>
                                    <li><a href="<?=$this->Html->url(['controller' => 'users', 'action' => 'login']);?>"><i class="fa fa-user"></i> <?=__('Iniciar Sesion');?></a></li>
                                <?php }?>
                                <li></li>

                                
                            </ul><!-- /.header-topbar-social -->
                        </div><!-- /.header-topbar-right -->
                    </div><!-- /.container -->
                </div><!-- /.header-topbar -->
                
                <div class="container">
                    <div class="header-inner">
                        <div class="header-main">
                            <div class="header-title">
                                <a href="index.html">
                                    <!-- aqui va la imagen -->
                                </a>
                            </div><!-- /.header-title -->

                            <div class="header-navigation">
                                <div class="nav-main-wrapper">
                                    <div class="nav-main-title visible-xs">
                                        <a href="index.html">
                                            <img src="assets/img/logo-blue.png" alt="Realsite">

                                            Realsite
                                        </a>
                                    </div><!-- /.nav-main-title -->

                                    <div class="nav-main-inner">
                                        <nav>
                                            <ul id="nav-main" class="nav nav-pills">
                                                <li class="important">
                                                    <a href="#">
                                                        <?=__('Blog');?>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <?=__('Quienes Somos');?>
                                                    </a>
                                                </li>
                                                <li class="has-children">
                                                    <a href="#">
                                                        <?=__('Propiedades');?>
                                                        <i class="fa fa-caret-down"></i>
                                                    </a>
                                                    <div>
                                                        <a href="#"><?=__('Propiedades');?> <i class="fa fa-caret-down"></i></a>
                                                        <ul class="sub-menu">
                                                            <li>
                                                                <a href="#"><?=__('Todas las Propiedades');?></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><?=__('Propiedades Destacada');?></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><?=__('Super Ofertas');?></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><?=__('Propiedades Mas Buscadas');?></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><?=__('Nuevas Propiedades Ingresadas');?></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="has-children">
                                                    <a href="#">
                                                        <?=__('Proyectos');?>
                                                        <i class="fa fa-caret-down"></i>
                                                    </a>
                                                    <div>
                                                        <a href="#">
                                                            <?=__('Proyectos');?>
                                                            <i class="fa fa-caret-down"></i>
                                                        </a>
                                                        <ul class="sub-menu">
                                                            <li>
                                                                <a href="#"><?=__('Desarrolladoras Inmobiliarias');?></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><?=__('Agentes y Agencias');?></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="has-children">
                                                    <a href="#">
                                                        <?=__('Directorio');?>
                                                        <i class="fa fa-caret-down"></i>
                                                    </a>
                                                    <div>
                                                        <a href="#">
                                                            <?=__('Directorio');?>
                                                            <i class="fa fa-caret-down"></i>
                                                        </a>
                                                        <ul class="sub-menu">
                                                            <li>
                                                                <a href="#"><?=__('Bancos Venta de Propiedades');?></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><?=__('Financiamiento');?></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><?=__('Mapa del Sitio');?></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="important">
                                                    <a href="#">
                                                        <?=__('Cont&aacute;ctenos');?>
                                                    </a>
                                                </li>
                                                <li class="important">
                                                    <a href=""></a>
                                                </li>
                                            </ul><!-- /.nav -->
                                        </nav>
                                    </div><!-- /.nav-main-inner -->
                                </div><!-- /.nav-main-wrapper -->

                                <button type="button" class="navbar-toggle">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div><!-- /.header-navigation -->
                        </div><!-- /.header-main -->

                        <a class="header-action" href="<?=$this->Html->url(['controller' => 'properties', 'action' => 'add'])?>" title="Add New Property">
                            <i class="fa fa-plus"></i>
                        </a><!-- /.header-action -->
                    </div><!-- /.header-inner -->
                </div><!-- /.container -->
            </div><!-- /.header-->

            <div class="main">
                <?=$this->Flash->render();?>
                <?=$this->fetch('content');?>    
            </div><!-- /.main -->
            
            <div id="footer" class="footer">
                <div class="footer-bottom">
                    <div class="container">
                        <div class="footer-bottom-inner">
                            <div class="row">
                                
                                <div class="widget col-sm-4">
                                    <h2><?=__('Informacion de contacto')?></h2>

                                    Effectivity Real Estate<br>
                                    Wardrobe Street 90210, London<br>
                                    E-mail: <a href="mailto:">info@example.com</a><br>
                                    Phone: +000-123-456-789
                                </div>
                                
                                <div class="widget col-sm-4">
                                    <h2>&nbsp;</h2>

                                    &copy; Copyright <?=date('Y')?>. <?=__('Creado por')?> <a href="http://wamdigital.com">WamDigital</a>.<br>
                                    <?=__('Todos los derechos reservados');?>

                                    <ul class="clearfix sharing-buttons">
                                        <li>
                                            <a class="facebook" href="https://www.facebook.com/americainmobiliaria/">
                                                <i class="fa fa-facebook fa-left"></i>
                                                <span class="social-name">Facebook</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="google-plus" href="https://plus.google.com/">
                                                <i class="fa fa-google-plus fa-left"></i>
                                                <span class="social-name">Google+</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="twitter" href="https://twitter.com">
                                                <i class="fa fa-twitter fa-left"></i>
                                                <span class="social-name">Twitter</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                

                                <div class="widget col-sm-4">
                                    <h2>&nbsp;</h2>
                                    <p>
                                        <a href="http://www.paguelofacil.com">
                                            <img src="http://paguelofacil.com/wp-content/uploads/Logo200x50.png">
                                        </a>
                                    </p>
                                    <p>
                                        <img src="<?=$this->Html->url('/img/logo-godaddy.png')?>">
                                    </p>
                                </div>
                            </div><!-- /.row -->
                        </div><!-- /.footer-bottom-inner -->
                    </div><!-- /.container -->
                </div><!-- /.footer-bottom -->
    
                <div class="footer-bar">
                    <div class="container center">
                        <a class="scroll-top"><i class="fa fa-angle-up"></i></a>
                    </div><!-- /.container -->
                </div><!-- /.footer-bar -->
            </div><!-- /.footer -->
            
            
        </div><!-- /.page-wrapper-->


    </body>
</head>