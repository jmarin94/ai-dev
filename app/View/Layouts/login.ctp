<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700&amp;subset=latin,latin-ext">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <?= $this->Html->css('Font-Awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('bootstrap-select/dist/css/bootstrap-select.min.css') ?>
        <?= $this->Html->css('bootstrap-fileinput/css/fileinput.min.css') ?>
        <?= $this->Html->css('nvd3/nv.d3.min.css') ?>
        <?= $this->Html->css('OwlCarousel/owl-carousel/owl.carousel.css') ?>
        <?= $this->Html->css('realsite-admin.css') ?>
        <title>Login | Admin | Americainmobiliaria</title>
    </head>
    <body>
        <div class="admin-wrapper">
            <div class="admin-content" style="background-color: #1976D2">
                <div class="admin-content-image-text">
                    <h1><?=__('Bienvenido a panel de administracion de America Inmobiliaria');?></h1>
                    <h2></h2>
                </div><!-- /.admin-content-image-text -->
            </div><!-- /.admin-content -->
            <?= $this->fetch('content') ?>
        </div><!-- /.admin-landing-wrapper -->
    </body>
</html>