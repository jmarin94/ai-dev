<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700&amp;subset=latin,latin-ext">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <?= $this->Html->css('Font-Awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('bootstrap-select/dist/css/bootstrap-select.min.css') ?>
        <?= $this->Html->css('bootstrap-fileinput/css/fileinput.min.css') ?>
        <?= $this->Html->css('nvd3/nv.d3.min.css') ?>
        <?= $this->Html->css('OwlCarousel/owl-carousel/owl.carousel.css') ?>
        <?= $this->Html->css('realsite-admin.css') ?>
        <?= $this->Html->css('bootstrap-datepicker.css') ?>


        <?= $this->Html->script('jquery.js') ?>
        <?= $this->Html->script('jquery-transit/jquery.transit.js') ?>

        <?= $this->Html->script('bootstrap/assets/javascripts/bootstrap/dropdown.js') ?>
        <?= $this->Html->script('bootstrap/assets/javascripts/bootstrap/collapse.js') ?>
        <?= $this->Html->script('bootstrap-select/dist/js/bootstrap-select.min.js') ?>
        <?= $this->Html->script('bootstrap-fileinput/js/fileinput.min.js') ?>

        <?= $this->Html->script('autosize/jquery.autosize.js') ?>
        <?= $this->Html->script('isotope/dist/isotope.pkgd.min.js') ?>
        <?= $this->Html->script('OwlCarousel/owl-carousel/owl.carousel.min.js') ?>
        <?= $this->Html->script('jquery.scrollTo/jquery.scrollTo.min.js') ?>

        <script src="http://maps.googleapis.com/maps/api/js?libraries=weather,geometry,visualization,places,drawing&amp;sensor=false" type="text/javascript"></script>
        <?= $this->Html->script('jquery-google-map/infobox.js') ?>
        <?= $this->Html->script('jquery-google-map/markerclusterer.js') ?>
        <?= $this->Html->script('jquery-google-map/jquery-google-map.js') ?>

        <?= $this->Html->script('nvd3/lib/d3.v3.js') ?>
        <?= $this->Html->script('nvd3/lib/d3.v3.js') ?>
        <?= $this->Html->script('nvd3/nv.d3.min.js') ?>

        <?= $this->Html->script('realsite.js') ?>
        <?= $this->Html->script('charts.js') ?>
        <?= $this->Html->script('map.js') ?>
        <?= $this->Html->script('bootstrap-datepicker.min.js') ?>



        <title><?= __("América Inmobiliaria | Sitio administrativo") ?></title>
    </head>
    <body class="open hide-secondary-sidebar">

        <div class="admin-wrapper">
            <div class="admin-navigation">
                <div class="admin-navigation-inner">
                    <nav>
                        <ul class="menu">
                            <li class="avatar">
                                <a href="admin-dashboard.html#">
                                    <?=$this->Html->image("ai/ai-admin-logo.png")?>
                                    <span class="avatar-content">
                                        <span class="avatar-title"><?=$loggedUser['full_name']?></span>
                                        <span class="avatar-subtitle"><?=__("Administrador del sitio")?></span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'dashboard')) ?>">
                                    <strong><i class="fa fa-dashboard"></i></strong> 
                                    <span><?= __('Inicio'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'index')) ?>">
                                    <strong><i class="fa fa-users"></i></strong> 
                                    <span><?= __('Usuarios'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= $this->Html->url(array('controller' => 'properties', 'action' => 'index')) ?>">
                                    <strong><i class="fa fa-building"></i></strong> 
                                    <span><?= __('Propiedades'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= $this->Html->url(array('controller' => 'adverts', 'action' => 'index')) ?>">
                                    <strong><i class="fa fa-user"></i></strong> 
                                    <span><?= __('Publicidad'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= $this->Html->url(array('controller' => 'informations', 'action' => 'edit', 1)) ?>">
                                    <strong><i class="fa fa-info"></i></strong> 
                                    <span><?= __('¿Quiénes somos?'); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'logout')) ?>">
                                    <strong><i class="fa fa-sign-out"></i></strong> 
                                    <span><?= __('Cerrar sesión'); ?></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="layer"></div>
                </div>
            </div>

            <div class="admin-content">
                <div class="admin-content-inner">
                    <div class="admin-content-header">
                        <div class="admin-content-header-inner">
                            <div class="container-fluid">
                                <div class="admin-content-header-logo">
                                    <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'dashboard')) ?>"><?= __("América Inmobiliaria | Sitio administrativo") ?></a>
                                </div>

                                <div class="admin-content-header-menu">
                                    <ul class="admin-content-header-menu-inner collapse">
                                        <li><a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'logout')) ?>"><?= __('Cerrar Sesión'); ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="admin-content-main">
                        <div class="admin-content-main-inner">
                            <?= $this->fetch('content') ?>
                        </div>
                    </div>

                    <div class="admin-content-footer">
                        <div class="admin-content-footer-inner">
                            <div class="container-fluid">
                                <div class="admin-content-footer-left">
                                    &copy; <?= date('Y') ?> <?= __("América Inmobiliaria. All rights reserved.") ?>
                                </div>

                                <div class="admin-content-footer-right">
                                    <?= __("Powered by") ?> <a href="http://wamdigital.com" target="__blank"><?= __("WAM DIGITAL") ?></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>
</html>