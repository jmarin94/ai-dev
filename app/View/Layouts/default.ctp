<!DOCTYPE html>
<html>
    <head>
        <?=$this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title><?=__("América Inmobiliaria");?></title>
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700&amp;subset=latin,latin-ext">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">

        <?= $this->Html->css('Font-Awesome/css/font-awesome.min') ?>
        <?= $this->Html->css('bootstrap-select/dist/css/bootstrap-select.min') ?>
        <?= $this->Html->css('bootstrap-fileinput/css/fileinput.min') ?>
        <?= $this->Html->css('nvd3/nv.d3.min') ?>
        <?= $this->Html->css('OwlCarousel/owl-carousel/owl.carousel') ?>
        <?= $this->Html->css('jquery.nailthumb.1.1.min') ?>
        <?= $this->Html->css('messi.min') ?>

        <?= $this->Html->css('realsite') ?>

        <?= $this->Html->script('jquery') ?>
        <?= $this->Html->script('jquery-transit/jquery.transit') ?>

        <?= $this->Html->script('bootstrap/assets/javascripts/bootstrap/dropdown') ?>
        <?= $this->Html->script('bootstrap/assets/javascripts/bootstrap/collapse') ?>
        <?= $this->Html->script('bootstrap-select/dist/js/bootstrap-select.min') ?>
        <?= $this->Html->script('bootstrap-fileinput/js/fileinput.min') ?>

        <?= $this->Html->script('autosize/jquery.autosize') ?>
        <?= $this->Html->script('isotope/dist/isotope.pkgd.min') ?>
        <?= $this->Html->script('OwlCarousel/owl-carousel/owl.carousel.min') ?>
        <?= $this->Html->script('jquery.scrollTo/jquery.scrollTo.min') ?>

        <script src="http://maps.googleapis.com/maps/api/js?libraries=weather,geometry,visualization,places,drawing&amp;sensor=false" type="text/javascript"></script>
        <?= $this->Html->script('jquery-google-map/infobox') ?>
        <?= $this->Html->script('jquery-google-map/markerclusterer') ?>
        <?= $this->Html->script('jquery-google-map/jquery-google-map') ?>

        <?= $this->Html->script('nvd3/lib/d3.v3') ?>
        <?= $this->Html->script('nvd3/nv.d3.min') ?>

        <?= $this->Html->script('jquery.nailthumb.1.1.min') ?>
        <?= $this->Html->script('messi.min') ?>

        <?= $this->Html->script('realsite') ?>
        <?= $this->Html->script('america-inmobiliaria') ?>
        <?= $this->Html->script('charts') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
        <script type="text/javascript">
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-21794005-1']);
          _gaq.push(['_trackPageview']);

          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        </script>
    </head>
    <body>

        <div class="page-wrapper">
            <div class="header header-standard"><?=$this->element('Page/ai-header')?></div>

            <div class="main">
                <?=$this->Flash->render();?>
                <?=$this->element('Flash/authError');?>
                <?=$this->fetch('content');?>

                <?= $this->element('Page/ads-bottom') ?>
            </div>

            <div id="footer" class="footer"><?=$this->element('Page/ai-footer')?></div>
        </div>
    </body>
