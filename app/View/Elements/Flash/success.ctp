<div class="container">
    <div class="content">
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <i class="fa fa-exclamation-circle"></i>
            <strong><?= __('Éxito'); ?>:</strong> <?= $message ?>
        </div>
    </div>
</div>