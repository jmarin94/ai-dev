<?php $html = $this->Flash->render('auth'); ?>
<?php if($html): ?>
    <div class="container">
        <div class="content m-bottom1">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <i class="fa fa-exclamation-triangle"></i>
                <strong><?= __('Error'); ?>:</strong> <?=$html;?>
            </div>
        </div>
    </div>
<?php endif; ?>
