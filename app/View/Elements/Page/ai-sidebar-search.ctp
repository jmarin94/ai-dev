<div id="search-properties-sidebar" class="col-sm-4 col-md-3">
    <div class="widget-title">
        <h2><i class="fa fa-search"></i>&nbsp;&nbsp;<?= __('Buscar Propiedades'); ?></h2>
    </div>
    <div class="widget-content">
        <?php
        echo $this->Form->create('Property', array('novalidate' => true, 'url' => array('controller' => 'home', 'action' => 'index')));
        /*Tipo de propiedad*/
        if(isset($this->request->data['property_type'])){
            echo $this->Form->input('property_type', array(
                'label' => false,
                'class' => 'form-control',
                'div' => 'form-group'
            ));
        }else{
            echo $this->Form->input('property_type', array(
                'label' => false,
                'class' => 'form-control',
                'empty' => array(null => __('Tipo de propiedad')),
                'div' => 'form-group'
            ));
        }

        /*Categoría*/
        if(isset($this->request->data['categories_id'])){
            echo $this->Form->input('categories_id', array(
                'label' => false,
                'class' => 'form-control',
                'div' => 'form-group'
            ));
        } else {
            echo $this->Form->input('categories_id', array(
                'label' => false,
                'class' => 'form-control',
                'empty' => array(null => __('Categoría')),
                'div' => 'form-group'
            ));
        }

        /*País*/
        if(isset($this->request->data['countries_id'])){
            echo $this->Form->input('countries_id', array(
                'label' => false,
                'class' => 'form-control',
                'div' => 'form-group'
            ));
        } else {
            echo $this->Form->input('countries_id', array(
                'label' => false,
                'class' => 'form-control',
                'empty' => array(null => __('Pais')),
                'div' => 'form-group'
            ));
        }

        /*Provincia*/
        if(isset($this->request->data['provinces_id'])){
            echo $this->Form->input('provinces_id', array(
                'label' => false,
                'class' => 'form-control',
                'div' => 'form-group'
            ));
        } else {
            echo $this->Form->input('provinces_id', array(
                'label' => false,
                'class' => 'form-control',
                'empty' => array(null => __('Provincia')),
                'div' => 'form-group'
            ));
        }

        /*City*/
        if(isset($this->request->data['cities_id'])){
            echo $this->Form->input('cities_id', array(
                'label' => false,
                'class' => 'form-control',
                'div' => 'form-group'
            ));
        } else {
            echo $this->Form->input('cities_id', array(
                'label' => false,
                'class' => 'form-control',
                'empty' => array(null => __('Ciudad')),
                'div' => 'form-group'
            ));
        }

        /*Moneda*/
        if(isset($this->request->data['currency'])){
            echo $this->Form->input('currency', array(
                'label' => false,
                'class' => 'form-control',
                'div' => 'form-group',
                'options' => array(1 => 'Colones (₡)', 2 => 'Dólares ($)'),
                'default' => $this->request->data['currency']
            ));
        } else {
            echo $this->Form->input('currency', array(
                'label' => false,
                'class' => 'form-control',
                'empty' => array(null => __('Moneda')),
                'div' => 'form-group',
                'options' => array(1 => 'Colones (₡)', 2 => 'Dólares ($)'),
            ));
        }

        /*From price*/
        if(isset($this->request->data['from_price'])){
            echo $this->Form->input('from_price', array(
                'label' => false,
                'class' => 'form-control',
                'type' => 'number',
                'div' => 'form-group col-xs-6',
                'placeholder' => __("Desde"),
                'value' => $this->request->data['from_price']
            ));
        } else {
            echo $this->Form->input('from_price', array(
                'label' => false,
                'class' => 'form-control',
                'type' => 'number',
                'div' => 'form-group col-xs-6',
                'placeholder' => __("Desde")
            ));
        }

        if(isset($this->request->data['to_price'])){
            echo $this->Form->input('to_price', array(
                'label' => false,
                'class' => 'form-control',
                'type' => 'number',
                'div' => 'form-group col-xs-6',
                'placeholder' => __("Hasta"),
                'value' => $this->request->data['to_price']
            ));
        } else {
            echo $this->Form->input('to_price', array(
                'label' => false,
                'class' => 'form-control',
                'type' => 'number',
                'div' => 'form-group col-xs-6',
                'placeholder' => __("Hasta")
            ));
        }
        ?>
        <button class="btn btn-lg btn-block"><i class="fa fa-search"></i> <?= __('Buscar'); ?></button>
        <?= $this->Form->end(); ?>
    </div>
    <?php if(!$this->request->is('mobile')) { ?>
        <?= $this->element('Page/ads-sidebar') ?>
    <?php } ?>
</div>
<div class="ads_mobile standard_ads col-sm-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- bottom_ads -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:300px;height:250px"
         data-ad-client="ca-pub-1519286524933845"
         data-ad-slot="7412203355"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<div class="ads_mobile standard_ads col-sm-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- bottom_ads_2 -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:300px;height:250px"
         data-ad-client="ca-pub-1519286524933845"
         data-ad-slot="2929801352"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var getProvincesUrl = "<?= $this->Html->url(array('controller' => 'properties', 'action' => 'getProvinces')); ?>/" + "<?= $this->data['countries_id'] ?>";
        $.getJSON(getProvincesUrl, function (selectValues) {
            $('#PropertyProvincesId').find('option').remove().end();
            $('#PropertyProvincesId').append($("<option></option>").attr("value", "0").text("<?= __('Provincia'); ?>"));
            $.each(selectValues, function (key, value) {
                $('#PropertyProvincesId').append($("<option></option>").attr("value", key).text(value));
            });
           $('#PropertyProvincesId').selectpicker('refresh');
        });

        $('#PropertyCountriesId').change(function (key) {
            var getProvincesUrl = "<?= $this->Html->url(array('controller' => 'properties', 'action' => 'getProvinces')); ?>/" + $(this).val();
            $.getJSON(getProvincesUrl, function (selectValues) {
                $('#PropertyProvincesId').find('option').remove().end();
                $('#PropertyProvincesId').append($("<option></option>").attr("value", "0").text("<?= __('Provincia'); ?>"));
                $.each(selectValues, function (key, value) {
                    $('#PropertyProvincesId').append($("<option></option>").attr("value", key).text(value));
                });
               $('#PropertyProvincesId').selectpicker('refresh');
            });
        });

        $('#PropertyProvincesId').change(function () {
            var getCitiesUrl = "<?= $this->Html->url(array('controller' => 'properties', 'action' => 'getCities')); ?>/" + $(this).val();
            $.getJSON(getCitiesUrl, function (selectValues) {
                $('#PropertyCitiesId').find('option').remove().end();
                $('#PropertyCitiesId').append($("<option></option>").attr("value", "0").text("<?= __('Ciudad'); ?>"));
                $.each(selectValues, function (key, value) {
                    $('#PropertyCitiesId').append($("<option></option>").attr("value", key).text(value));
                });
               $('#PropertyCitiesId').selectpicker('refresh');
            });
        });

    });
</script>
