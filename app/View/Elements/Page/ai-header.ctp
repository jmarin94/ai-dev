<div class="header-topbar">
    <div class="container">
        <div class="header-topbar-right">
            <ul class="header-topbar-social ml30">
                <?php if($loggedUser != null){ ?>
                    <li>
                        <?php
                        $response = $this->requestAction('app/checkRole/' . $loggedUser['user_type'] . '/' . 'administrador');
                        if($response){
                            $url =  Router::url(array('controller' => 'users', 'action' => 'dashboard', 'aipanel' => true), true);
                        } else {
                            $url =  $this->Html->url(array('controller' => 'users', 'action' => 'dashboard'));
                        }
                        ?>
                        <a href="<?=$url?>">
                            <i class="fa fa-user"></i>
                            <?=$loggedUser['full_name']?>
                        </a>
                    </li>
                    <li><a href="<?=$this->Html->url(array('controller' => 'users', 'action' => 'logout'));?>"><i class="fa fa-sign-out"></i> <?=__('Cierra sesión');?></a></li>
                <?php } else { ?>
                    <li><a href="<?=$this->Html->url(array('controller' => 'users', 'action' => 'add'));?>"><i class="fa fa-user"></i> <?=__('Regístrate');?></a></li>
                    <li><a href="<?=$this->Html->url(array('controller' => 'users', 'action' => 'login'));?>"><i class="fa fa-sign-in"></i> <?=__('Inicia Sesión');?></a></li>
                <?php }?>
                <li></li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div class="header-inner">
        <div class="header-main">
            <div class="header-title">
                <a href="<?= $this->Html->url(array('controller' => 'Home', 'action' => 'index')); ?>">
                    <?= $this->Html->image("ai/logo-ai.png", array("alt" => "América Inmobiliaria"));?>
                </a>
            </div>

            <div class="header-navigation">
                <div class="nav-main-wrapper">
                    <div class="nav-main-title visible-xs">
                        <a href="<?= $this->Html->url(array('controller' => 'Home', 'action' => 'index')); ?>">
                            <?=$this->Html->image("ai/logo-ai.png", array("alt" => "América Inmobiliaria"));?>
                        </a>
                    </div>

                    <div class="nav-main-inner">
                        <nav>
                            <ul id="nav-main" class="nav nav-pills">
                                <!-- class important is for active class -->
                                <li>
                                    <a href="<?=$this->Html->url(array('controller' => 'properties', 'action' => 'viewAll'))?>">
                                        <?=__('Propiedades');?>
                                    </a>
                                </li>
                                <li class="has-children">
                                    <a href="#">
                                        <?=__('Proyectos');?>
                                        <i class="fa fa-caret-down"></i>
                                    </a>
                                    <div>
                                        <a href="#">
                                            <?=__('Proyectos habitacionales');?>
                                            <i class="fa fa-caret-down"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?=$this->Html->url(array('controller' => 'projects', 'action' => 'viewAll'));?>"><?=__('Desarrolladoras inmobiliarias');?></a>
                                            </li>
                                            <li>
                                                <a href="<?=$this->Html->url(array('controller' => 'users', 'action' => 'agencies'));?>"><?=__('Agentes y agencias');?></a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a href="<?=$this->Html->url(array('controller' => 'users', 'action' => 'banks'));?>">
                                        <?=__('Bancos (venta de propiedades)');?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=$this->Html->url(array('controller' => 'pages', 'action' => 'aboutUs'));?>">
                                        <?=__('¿Quiénes somos?');?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=$this->Html->url(array('controller' => 'pages', 'action' => 'contact'));?>"><?=__('Contáctenos');?></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        </div>

        <?php
        if($loggedUser){
            $url = $this->requestAction('app/checkRole/' . $loggedUser['user_type'] . '/' . 'desarrolladora') ? array('controller' => 'projects', 'action' => 'add') : array('controller' => 'properties', 'action' => 'add');
        } else{
            $url = array('controller' => 'properties', 'action' => 'add');
        }
        ?>
        <a class="header-action" href="<?=$this->Html->url($url)?>" title="Add New Property">
            <i class="fa fa-plus"></i>
        </a>
    </div>
</div>
