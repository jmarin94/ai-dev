<br><br>
<center>
    <?php if (count($advertsSidebar) > 0) { ?>
        <div class="widget-content">
            <?php
            foreach ($advertsSidebar as $advert) {
                if ($advert['Advert']['code'] != "") {
                    ?>
                    <br>
                    <div id="google_ads_160L1">
                        <?php echo $advert['Advert']['code']; ?>
                    </div>
                    <br>
                    <?php
                } else if ($advert['Advert']['file'] != "") {
                    echo '<br>';
                    echo '<a href="' . $advert['Advert']['url'] . '">' . $this->Html->image('ai/files/adverts/' . $advert['Advert']['file'], array('style' => 'max-width: 200px')) . '</a>';
                    echo '<br>';
                }
            }
            ?>
        </div>
    <?php } ?>
</center>
