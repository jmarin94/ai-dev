<div class="row">
    <div class="widget-title agencias-title">
        <h2><a href="<?=$this->Html->url(array('controller' => 'users', 'action' => 'agencies'));?>"><i class="fa fa-users"></i>&nbsp;&nbsp;<?= __('Agencias Destacadas'); ?></a></h2>
    </div>
    <?php if(count($agencias) > 0): ?>
    <ul class="agencias-list">
        <?php foreach ($agencias as $agencia): ?>
        <li>
            <?php $url = Router::url('/').'ver/agencia/'.$agencia['User']['id'].'-' .strtolower(Inflector::slug($agencia['User']['full_name'], '-')); ?>
            <a href="<?=$url;?>" class="property-simple-image">
                <div class="nailthumb-container square-thumb agencies">
                    <?php 
                    if($agencia['User']['img'] != null){ 
                        echo $this->Html->image("ai/files/users/" . $agencia['User']['img'] , array("alt" => $agencia['User']['full_name']));
                    } else {
                        echo $this->Html->image("ai/no-image.jpg" , array("alt" => __("Imagen no disponible")));
                    }
                    ?>
                </div>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php else: ?>
        <div class="feature center col-sm-4">
            <i class="feature-icon fa fa-folder-open-o"></i>
            <h3><?=__("No hay agencias ni agentes por mostrar");?></h3>
        </div>
    <?php endif; ?>
</div>