<div class="row">
    <div class="widget-title desarrolladoras-title">
        <h2><a href="<?=$this->Html->url(array('controller' => 'projects', 'action' => 'viewAll'));?>"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?= __('Desarrolladoras'); ?></a></h2>
    </div>
    <?php if(count($desarrolladoras) > 0): ?>
        <div id="ai-desarrolladoras" class="property-carousel-wrapper">
            <div class="property-carousel">
                <?php foreach ($desarrolladoras as $desarrolladora){ ?>
                    <div class="property-carousel-item">
                        <div class="property-simple">
                            <?php $url = Router::url('/').'ver/desarrolladora/'.$desarrolladora['User']['id'].'-' .strtolower(Inflector::slug($desarrolladora['User']['full_name'], '-')); ?>
                            <a href="<?=$url;?>" class="property-simple-image">
                                <div class="nailthumb-container square-thumb desarrolladoras">
                                    <?php 
                                    if($desarrolladora['User']['img'] != null){ 
                                        echo $this->Html->image("ai/files/users/" . $desarrolladora['User']['img'] , array("alt" => $desarrolladora['User']['full_name']));
                                    } else {
                                        echo $this->Html->image("ai/no-image.jpg" , array("alt" => __("Imagen no disponible")));
                                    }
                                    ?>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div><!-- /.property-carousel -->
        </div><!-- /.property-carousel-wrapper -->
    <?php else: ?>
        <div class="feature center col-sm-4">
            <i class="feature-icon fa fa-folder-open-o"></i>
            <h3><?=__("No hay desarrolladoras por mostrar");?></h3>
        </div>
    <?php endif; ?>
</div>
<div class="ads_mobile standard_ads col-sm-12">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- 320x50 -->
        <ins class="adsbygoogle"
         style="display:block"
         data-ad-client="ca-pub-1519286524933845"
         data-ad-slot="1681897355"
         data-ad-format="auto"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>