<div class="row">
    <h1 class="page-header"><?=__('Propiedades');?></h1>
    
    <?php if(count($propiedades) > 0): ?>
    <div class="box">
        <div class="row property-simple-wrapper row row-eq-height">
            <?php $flagForBanners = 0; ?>
            <?php foreach ($propiedades as $propiedad){?>
                <div class="col-sm-3 col-sm-3 col-xs-6">
                    <div class="property-simple">
                        <a href="<?=$this->Html->url(['controller' => 'properties', 'action' => 'view', $propiedad['Property']['id']])?>" class="property-simple-image">
                            <div class="nailthumb-container square-thumb properties-table">
                                <?php if(isset($propiedad['Property']['outstanding'])): ?>
                                    <?php 
                                        $date = new DateTime($propiedad['Property']['created']);
                                        $monts = $propiedad['Property']['months'];
                                        $expiration = strtotime('+' . $monts . ' month', strtotime($date->format('d-m-Y')));
                                        $expiration = date ('d-m-Y' , $expiration);
                                        if(strtotime($expiration) > strtotime(date('d-m-Y'))):
                                    ?>
                                            <?php if($propiedad['Property']['outstanding'] == 2): ?>
                                                <div class="featured-icon"></div>
                                            <?php elseif ($propiedad['Property']['outstanding'] == 1): ?>
                                                <div class="superoffer-icon" ></div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                <?php endif; ?>
                                <?php 
                                    if(isset($propiedad['PropertiesImages']) && count($propiedad['PropertiesImages']) > 0){ 
                                        echo $this->Html->image($propiedad['PropertiesImages'][0]['rute'] , array("alt" => ""));
                                    } else {
                                        echo $this->Html->image("ai/no-image.jpg" , array("alt" => __("Imagen no disponible")));
                                    }
                                ?>
                            </div>
                        </a>
                        <?php if(isset($propiedad['Flag']) && count($propiedad['Flag']) > 0): ?>
                        <ul class="properties-flags-home">
                            <?php foreach($propiedad['Flag'] as $flag): ?>
                                <li><?= $flag['name']; ?></li>
                            <?php endforeach;; ?>
                        </ul>
                        <?php endif; ?>
                        <p>
                            <span><?= $propiedad['Cities']['name'] . " " . DS  . " " . $propiedad['Categories']['name']  ?></span><br>
                            <b><?= $propiedad['PropertyType']['name'] ?></b>
                        </p>
                    </div><!-- /.property-simple -->
                </div>
                <?php if($flagForBanners == 7 || $flagForBanners == 22){ ?>
                    <div class="ads_mobile horizontal_ads col-sm-12">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- middle_properties -->
                        <ins class="adsbygoogle"
                             style="display:inline-block;width:320px;height:50px"
                             data-ad-client="ca-pub-1519286524933845"
                             data-ad-slot="5575543359"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
            <?php } $flagForBanners++;
            
                } ?>
        </div>
    </div>
    <div class="cake-pagination col-xs-12 text-center">
        <?php
        echo $this->Paginator->prev('< ', array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
    <br><br>
    <div class="cake-pagination col-xs-12 text-center">
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, empezando en el registro {:start}, y terminando en el {:end}')
        ));
        ?>	
    </div>
    <?php else: ?>
        <div class="feature center col-sm-4">
            <i class="feature-icon fa fa-folder-open-o"></i>
            <h3><?=__("No hay propiedades por mostrar");?></h3>
        </div>
    <?php endif; ?>
</div>