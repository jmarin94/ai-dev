<div class="footer-bottom">
    <div class="container">
        <div class="footer-bottom-inner">
            <div class="row">
                <div class="widget col-sm-4">
                    <h2><?=__('Información de contacto')?></h2>
                    <?=__("América Inmobiliaria")?><br>
                    <?=__("San José, Costa Rica")?><br>
                    <?=__("E-mail")?>:&nbsp;<a href="mailto:<?=__("info@americainmobiliaria.com")?>"><?=__("info@americainmobiliaria.com")?></a><br>
                </div>

                <div class="widget col-sm-4">
                    <h2>&nbsp;</h2>
                    &copy; <?=__("Copyright"); ?> <?=date('Y')?>. <?=__('Powered by')?> <a href="http://wamdigital.com"><?=__("WAMDIGITAL");?></a>.<br>
                    <?=__('Todos los derechos reservados');?>

                    <ul class="clearfix sharing-buttons">
                        <li>
                            <a class="facebook" target="__blank" href="https://www.facebook.com/americainmobiliaria/">
                                <i class="fa fa-facebook fa-left"></i>
                                <span class="social-name"><?=__("Facebook");?></span>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="widget col-sm-4">
                    <h2>&nbsp;</h2>
                    <p>
                        <?php 
                        echo $this->Html->image("ai/logo-godaddy.png", array(
                            "alt" => "GoDaddy",
                            "url" => "javascript:void(8)"
                        ));
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer-bar">
    <div class="container center">
        <a class="scroll-top"><i class="fa fa-angle-up"></i></a>
    </div>
</div>