<?php if (count($advertsBottom) > 0) { ?>
    <div class="container ads_desktop">
        <div class="widget-content">
            <?php
            foreach ($advertsBottom as $advert) {
                if ($advert['Advert']['code'] != "") {
                    ?>
                    <div id="google_ads_160L1">
                        <?php echo $advert['Advert']['code']; ?>
                    </div>
                    <?php
                } else if ($advert['Advert']['file'] != "") {
                    echo '<a class="col-sm-3" href="' . $advert['Advert']['url'] . '">' . $this->Html->image('ai/files/adverts/' . $advert['Advert']['file']) . '</a>';
                }
            }
            ?>
        </div>
    </div>
<?php } ?>