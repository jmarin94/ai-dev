<div class="adverts index">
	<h2><?php echo __('Publicidad'); ?></h2>
	<table class="table table-simple">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('start'); ?></th>
			<th><?php echo $this->Paginator->sort('end'); ?></th>
			<th><?php echo $this->Paginator->sort('code'); ?></th>
			<th><?php echo $this->Paginator->sort('file'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($adverts as $advert): ?>
	<tr>
		<td><?php echo h($advert['Advert']['id']); ?>&nbsp;</td>
		<td><?php echo h($advert['Advert']['start']); ?>&nbsp;</td>
		<td><?php echo h($advert['Advert']['end']); ?>&nbsp;</td>
		<td><?php echo h($advert['Advert']['code'] != '' ? $advert['Advert']['code'] : '--'); ?>&nbsp;</td>
		<td><?php echo h($advert['Advert']['file']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $advert['Advert']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $advert['Advert']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $advert['Advert']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Opciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Nuevo banner'), array('action' => 'add')); ?></li>
	</ul>
</div>
