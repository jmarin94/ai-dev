<div class="adverts view">
<h2><?php echo __('Advert'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($advert['Advert']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start'); ?></dt>
		<dd>
			<?php echo h($advert['Advert']['start']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End'); ?></dt>
		<dd>
			<?php echo h($advert['Advert']['end']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Url'); ?></dt>
		<dd>
			<?php echo h($advert['Advert']['url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($advert['Advert']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File'); ?></dt>
		<dd>
			<?php echo h($advert['Advert']['file']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Advert'), array('action' => 'edit', $advert['Advert']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Advert'), array('action' => 'delete', $advert['Advert']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $advert['Advert']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Adverts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Advert'), array('action' => 'add')); ?> </li>
	</ul>
</div>
