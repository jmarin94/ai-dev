<div class="container-fluid">
    <div class="col-md-5">
        <?php echo $this->Form->create('Advert', ['type' => 'file']); ?>
        <?php
        echo $this->Form->input('start', [
            'type' => 'text',
            'class' => 'datepicker',
            'label' => __("Fecha de inicio")
        ]);
        ?>
        <br><br>
        <?php
        echo $this->Form->input('end', [
            'type' => 'text',
            'class' => 'datepicker',
            'label' => __("Fecha final")
        ]);
        ?>
        <br><br>
        <?php
        $options = [
            0 => __('Seleccione ubicación'),
            1 => __('Sidebar'),
            5 => __('Contenido (saldrá en el bottom de la página)')
        ];
        echo $this->Form->input('location', ['options' => $options]);
        ?>
        <br><br>
        <?php
        echo $this->Form->input('code', array('label' => 'Código de Publicidad de Google'));
        ?>
        <br><br>
        <?php
        echo $this->Form->input('url', array('label' => 'URL de banner'));
        ?>
        <br><br>
        <?php
        echo $this->Form->input('file', ['type' => 'file']);
        ?>
        <br><br>
        <?php echo $this->Form->end(__('Guardar')); ?>
    </div>
</div>
<script type="text/javascript">
    $('.datepicker').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        language: "es"
    });
</script>