<?php
$images = "";
foreach($this->request->data['ProjectsImage'] as $img):
    if($img['type'] == 'image'){
        $images .= "'" . $this->Html->image($img['rute'], array('class' => 'file-preview-image')) . "'";
        $images .= ',';
    }
endforeach;

$blueprints = "";
foreach($this->request->data['ProjectsImage'] as $file):
    if($file['type'] == 'blueprint'){
        $blueprints .= "'" . $this->Html->image($img['rute'], array('class' => 'file-preview-image')) . "'";
        $blueprints .= ',';
    }
endforeach;
?>
<div class="content">
    <div class="container">
        <div class="add-properties row">
            <div class="col-sm-8 col-md-9">
                <div class="widget-title">
                    <h2><?= __("Publicar nuevo proyecto") ?></h2>
                </div>
                <?= $this->Form->create('Project', array('type' => 'file')); ?>
                <?=$this->Form->input('id');?>
                <?=$this->Form->input('users_id', array('type' => 'hidden', 'value' => $loggedUser['id']));?>
                <div class="box">
                    <div class="form-group">
                        <?=
                        $this->Form->input('name', array(
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => __("Nombre")
                        ));
                        ?>
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <?=
                        $this->Form->input('description', array(
                            'class' => 'form-control',
                            'label' => false,
                            'placeholder' => __("Información general")
                        ));
                        ?>
                    </div><!-- /.form-group -->
                </div><!-- /.box -->

                <div class="widget-title">
                    <h2><?= __("Detalles del proyecto") ?></h2>
                </div>

                <div class="box">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <?= $this->Form->input('email', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'placeholder' => __('E-mail de la empresa'),
                                    'div' => 'form-group'
                                )); ?>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <?= $this->Form->input('phone', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'placeholder' =>  __('Teléfono'),
                                    'type' => 'number',
                                    'div' => 'form-group'
                                ));?>
                            </div>
                        </div><!-- /.col-* -->

                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-university"></i></span>
                                <?= $this->Form->input('bank', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'div' => 'form-group',
                                    'placeholder' => __("Banco que financia el proyecto")
                                ));
                                ?>
                            </div><!-- /.form-group -->

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?= $this->Form->input('price', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'type' => 'text',
                                    'div' => 'form-group',
                                    'placeholder' => __("Precios desde")
                                )); ?>
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.box -->


                <div class="widget-title">
                    <h2><?= __("Ubicación de la propiedad") ?></h2>
                </div>

                <div class="box">
                    <div class="row">
                        <div class="col-sm-12">
                            <?= $this->Form->input('countries_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => array(null => __('País')),
                                'div' => 'form-group'
                            )); ?>
                            <?= $this->Form->input('provinces_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => array(null => __('Provincia')),
                                'div' => 'form-group'
                            )); ?>
                            <?= $this->Form->input('cities_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'empty' => array(null => __('Ciudad')),
                                'div' => 'form-group'
                            ));?>
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </div><!-- /.box -->


                <div class="row">
                    <div class="col-sm-12">
                        <div class="widget-title">
                            <h2><?= __("Posición en el mapa") ?></h2>
                        </div>

                        <div class="box">
                            <input id="pac-input" class="controls" type="text" placeholder="Enter a location">

                            <div id="map-canvas"></div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <?= $this->Form->input('latitude', array(
                                            'class' => 'form-control',
                                            'id' => 'input-latitude',
                                            'type' => 'text',
                                            'label' => false,
                                            'placeholder' => __("Latitud")
                                        )); ?>
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->

                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        <?= $this->Form->input('longitude', array(
                                            'class' => 'form-control',
                                            'id' => 'input-longitude',
                                            'type' => 'text',
                                            'label' => false,
                                            'placeholder' => __("Longitud")
                                        )); ?>
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->
                        </div><!-- /.box -->
                    </div>

                    <div class="col-sm-12">
                        <div class="widget-title">
                            <h2><?= __("Multimedia") ?></h2>
                        </div>

                        <div class="box">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-youtube"></i></span>
                                <?= $this->Form->input('youtube', array(
                                    'label' => false,
                                    'class' => 'form-control',
                                    'placeholder' => __("URL video de YouTube"),
                                    'div' => 'form-group'
                                )); ?>
                            </div>
                        </div>

                        <div class="box">
                            <input id="input-file-desarrolladora" name="input-file-desarrolladora[]" type="file" class="file-loading" accept="image/*" multiple>
                        </div><!-- /.box -->
                        <div class="box">
                            <input id="input-file-planos" name="input-file-planos[]" type="file" class="file-loading" accept="image/*" multiple>
                        </div><!-- /.box -->
                    </div>
                </div><!-- /.row -->

                <div class="center">
                    <button class="btn btn-xl" type="submit"><?=__('Editar');?></button>
                </div><!-- /.center -->
            <?= $this->Form->end(); ?>
            </div><!-- /.content -->
            <div class="col-sm-4 col-md-3">
                <?= $this->element('Page/ads-sidebar') ?>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.content -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#ProjectCountriesId').change(function () {
            var getProvincesUrl = "<?= $this->Html->url(['controller' => 'projects', 'action' => 'getProvinces']); ?>/" + $(this).val();
            $.getJSON(getProvincesUrl, function (selectValues) {
                $('#ProjectProvincesId').find('option').remove().end();
                $('#ProjectProvincesId').append($("<option></option>").attr("value", "0").text("<?= __('Provincia'); ?>"));
                $.each(selectValues, function (key, value) {
                    $('#ProjectProvincesId').append($("<option></option>").attr("value", key).text(value));
                });
                $('#ProjectProvincesId').selectpicker('refresh');
            })
        });

        $('#ProjectProvincesId').change(function () {
            var getCitiesUrl = "<?= $this->Html->url(['controller' => 'projects', 'action' => 'getCities']); ?>/" + $(this).val();
            $.getJSON(getCitiesUrl, function (selectValues) {
                $('#ProjectCitiesId').find('option').remove().end();
                $('#ProjectCitiesId').append($("<option></option>").attr("value", "0").text("<?= __('Ciudad'); ?>"));
                $.each(selectValues, function (key, value) {
                    $('#ProjectCitiesId').append($("<option></option>").attr("value", key).text(value));
                });
                $('#ProjectCitiesId').selectpicker('refresh');
            });
        });

        'use strict';
        function initialize() {
            var mapOptions = {
                center: new google.maps.LatLng($('#input-latitude').val(), $('#input-longitude').val()),
                zoom: 16
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


            var image = 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png';
            var marker = new google.maps.Marker({
                position: {lat: parseFloat($('#input-latitude').val()), lng: parseFloat($('#input-longitude').val())},
                map: map,
                draggable: true,
                anchorPoint: new google.maps.Point(0, -29)
            });
            marker.setIcon(/** @type {google.maps.Icon} */({
                url: image,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));

            var input = /** @type {HTMLInputElement} */(
                document.getElementById('pac-input'));

                var types = document.getElementById('type-selector');
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);

                var infowindow = new google.maps.InfoWindow();

                google.maps.event.addListener(marker, "mouseup", function(event) {
                    $('#input-latitude').val(this.position.lat());
                    $('#input-longitude').val(this.position.lng());
                });

                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    infowindow.close();
                    marker.setVisible(false);
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        return;
                    }

                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }
                    marker.setIcon(/** @type {google.maps.Icon} */({
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(35, 35)
                    }));
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

                    $('#input-latitude').val(place.geometry.location.lat());
                    $('#input-longitude').val(place.geometry.location.lng());

                    var address = '';
                    if (place.address_components) {
                        address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');
                    }

                    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                    infowindow.open(map, marker);
                });
            }

            if ($('#map-canvas').length != 0) {
                google.maps.event.addDomListener(window, 'load', initialize);
            }

            jQuery("#input-file-desarrolladora").fileinput({
                uploadUrl: "/file-upload-batch/2",
//                autoReplace: true,
                maxFileCount: 10,
                allowedFileExtensions: ["jpg", "png", "gif"],
                dropZoneTitle: "Arrastra las fotos de tu desarrolladora",
                browseLabel: "Explorar",
                removeLabel: "Remover fotos",
                uploadLabel: "Subir",
                initialPreview: [
                    <?=$images?>
                ]
            });
            jQuery("#input-file-planos").fileinput({
                uploadUrl: "/file-upload-batch/2",
//                autoReplace: true,
                maxFileCount: 10,
                allowedFileExtensions: ["jpg", "png", "gif", "pdf"],
                dropZoneTitle: "Arrastra los archivos de los planos",
                browseLabel: "Explorar",
                removeLabel: "Remover archivos",
                uploadLabel: "Subir",
                initialPreview: [
                    <?=$blueprints?>
                ]
            });
    });
</script>
