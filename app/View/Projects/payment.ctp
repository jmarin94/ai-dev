<div class="content">
    <div class="container">
        <?php if(!isset($status)):  ?>
            <div class="feature col-sm-12">
                <h3><?=__("Pago con Paypal");?></h3>

                <p><?=__("Para ir a un entorno seguro de pago, favor pulse el botón 'Comprar ahora' y será redirigido a la página de Paypal.");?></p>
                <b>
                    <p><?=__("Pago por destacados: $") . $data['paymentTotal'];?></p>
                </b>
            </div>
            <form class="col-sm-4" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="business" value="dsibaja@americainmobiliaria.com">
                <input type="hidden" name="item_name" value="Destacamiento de proyecto">
                <input type="hidden" name="item_number" value="1">
                <input type="hidden" name="amount" value="<?php echo $data['paymentTotal'] ?>">
                <input type="hidden" name="no_shipping" value="0">
                <input type="hidden" name="no_note" value="1">
                <input type="hidden" name="currency_code" value="USD">
                <input type="hidden" name="lc" value="AU">
                <input type="hidden" name="notify_url" value="<?=Router::url(array('controller' => 'projects', 'action' => 'payment', 'notify'), true);?>">
                <input type="hidden" name="return" value="<?=Router::url(array('controller' => 'projects', 'action' => 'payment', 'success'), true);?>">
                <input type="hidden" name="cbt" value="Return to The Store">
                <input type="hidden" name="cancel_return" value="<?=Router::url(array('controller' => 'projects', 'action' => 'payment', 'cancel'), true);?>">
                <input type="hidden" name="bn" value="PP-BuyNowBF">
                <input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal. La forma rápida y segura de pagar en Internet.">
                <img alt="" border="0" src="https://www.paypalobjects.com/es_ES/i/scr/pixel.gif" width="1" height="1">
            </form>
            <a href="<?= $this->Html->url(array('controller' => 'projects', 'action' => 'payment', 'cancel')) ?>" class="btn"><?=__("Cancelar")?></a>
        <?php else: ?>
            <?php if($status == 'cancel'): ?>
                <div class="feature col-sm-12">
                    <h3><?=__("Pago cancelado");?></h3>

                    <p><?=__("El pago no ha sido completado, su proyecto no ha sido destacado.");?></p>
                </div>
            <?php elseif($status == 'sucess'): ?>
                <div class="feature col-sm-12">
                    <h3><?=__("Pago procesado con Paypal");?></h3>

                    <p><?=__("El pago ha sido procesado, su proyecto será destacado.");?></p>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
