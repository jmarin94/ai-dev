<?php 
$user = $project['Users'];
?>
<div class="content">
    <div class="container">
        <div class="row">
            <div class=" col-sm-8 col-md-9">
                <?php // debug($project);?>
                <h1 class="page-header"><?= $project['Project']['name'] ?></h1>
                <div class="row">
                    <div class="col-sm-12 col-md-7">
                        
                        <div class="property-gallery">
                            <div class="property-gallery-preview">
                                <?php if (count($project['ProjectsImage']) > 0) { ?>
                                    <a href="javascript:void(8)">
                                        <div>
                                            <?php if(isset($project['ProjectsImage'][0]['rute'])): ?>
                                                <?= $this->Html->image($project['ProjectsImage'][0]['rute']) ?>
                                            <?php else: ?>
                                                <?= $this->Html->image("ai/no-image.jpg" , array("alt" => __("Imagen no disponible"))); ?>
                                            <?php endif; ?>
                                        </div>
                                    </a>
                                <?php } else { ?>
                                    <a href="javascript:void(8)">
                                        <div>
                                            <?= $this->Html->image("ai/no-image.jpg", array("alt" => __("Imagen no disponible"), "width" => "360")); ?>
                                        </div>
                                    </a>
                                <?php }  ?>
                            </div>
                            <?php if (count($project['ProjectsImage']) > 0) { ?>
                                <div class="property-gallery-list-wrapper">
                                    <div class="property-gallery-list">
                                        <?php foreach ($project['ProjectsImage'] as $image) { ?>
                                            <?php if($image['type'] == 'image'): ?>
                                                <div class="property-gallery-list-item active">
                                                    <a href="<?= $this->Html->url("../img/" . $image['rute']); ?>">
                                                        <?= $this->Html->image($image['rute']) ?>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        
                        <?php 
                        $planos = 0;
                        $keyPlano = 0;
                        foreach ($project['ProjectsImage'] as $key => $image) { 
                            if($image['type'] == 'blueprint'):
                                $planos++;
                                if($planos == 1)
                                    $keyPlano = $key;
                            endif;
                        }
                        ?>
                        
                        <?php if($planos > 0): ?>
                            <h2 class="mb30"><?= __('Planos') ?></h2>
                            <div class="property-gallery">
                                <div class="property-gallery-preview">
                                    <?php if (count($project['ProjectsImage']) > 0) { ?>
                                        <a href="javascript:void(8)">
                                            <div>
                                                <?php if(isset($project['ProjectsImage'][$keyPlano]['rute'])): ?>
                                                    <?= $this->Html->image($project['ProjectsImage'][$keyPlano]['rute']) ?>
                                                <?php else: ?>
                                                    <?= $this->Html->image("ai/no-image.jpg" , array("alt" => __("Imagen no disponible"))); ?>
                                                <?php endif; ?>
                                            </div>
                                        </a>
                                    <?php } else { ?>
                                        <a href="javascript:void(8)">
                                            <div>
                                                <?= $this->Html->image("ai/no-image.jpg", array("alt" => __("Imagen no disponible"), "width" => "360")); ?>
                                            </div>
                                        </a>
                                    <?php }  ?>
                                </div>
                                <?php if (count($project['ProjectsImage']) > 0) { ?>
                                    <div class="property-gallery-list-wrapper">
                                        <div class="property-gallery-list">
                                            <?php foreach ($project['ProjectsImage'] as $image) { ?>
                                                <?php if($image['type'] == 'blueprint'): ?>
                                                    <div class="property-gallery-list-item active">
                                                        <a href="<?= $this->Html->url("../img/" . $image['rute']); ?>">
                                                            <?= $this->Html->image($image['rute']) ?>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php endif; ?>

                    </div>

                    <div class="col-sm-12 col-md-5">
                        <div class="property-list">
                            <dl>
                                <?php
                                if ((isset($project['Project']['name'])) && ($project['Project']['name'] != "")) {
                                    echo '<dt>' . __('Nombre') . '</dt><dd>' . $project['Project']['name'] . '</dd>';
                                }
                                if ((isset($project['Cities']['name'])) && ($project['Cities']['name'] != "")) {
                                    echo '<dt>' . __('Ubicación') . '</dt><dd>' . $project['Cities']['name'] . ', ' . $project['Provinces']['name'] . ', ' . $project['Countries']['name'] . '</dd>';
                                }
                                if ((isset($project['Project']['bank'])) && ($project['Project']['bank'] != "")) {
                                    echo '<dt>' . __('Bancos que financian') . '</dt><dd>' . $project['Project']['bank'] . '</dd>';
                                }
                                if ((isset($project['Project']['price'])) && ($project['Project']['price'] != "")) {
                                    echo '<dt>' . __('Precios desde') . '</dt><dd>' . $project['Project']['price'] . '</dd>';
                                }
                                if ((isset($project['Project']['phone'])) && ($project['Project']['phone'] != "")) {
                                    echo '<dt>' . __('Teléfono') . '</dt><dd>' . $project['Project']['phone'] . '</dd>';
                                }
                                if ((isset($project['Project']['email'])) && ($project['Project']['email'] != "")) {
                                    echo '<dt>' . __('E-mail') . '</dt><dd>' . $project['Project']['email'] . '</dd>';
                                }
                                ?>
                            </dl>
                        </div>

                        <h2 class="mb30"><?= __('Compartir') ?></h2>
                        <ul class="clearfix sharing-buttons">
                            <li>
                                <a class="facebook" href="https://www.facebook.com/share.php?u=<?= Router::url($this->here, true); ?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                    <i class="fa fa-facebook fa-left"></i>
                                    <span class="social-name">Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a class="google-plus" href="https://plus.google.com/share?url=<?= Router::url($this->here, true); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                    <i class="fa fa-google-plus fa-left"></i>
                                    <span class="social-name">Google+</span>
                                </a>
                            </li>
                            <li>
                                <a class="twitter" href="https://twitter.com/home?status=<?= Router::url($this->here, true); ?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                    <i class="fa fa-twitter fa-left"></i>
                                    <span class="social-name">Twitter</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
                
                <div class="mb30">
                    <h2><?=__("Descripción")?></h2>

                    <p class="text">
                        <?=$project['Project']['description']?>
                    </p>
                </div>
                
                <?php if ((isset($project['Project']['youtube'])) && ($project['Project']['youtube'] != "")): ?>
                    <div class="mb30">
                        <h2 class="page-header"><?=__("Video")?></h2>

                       <?php 
                        $urlYoutube = $project['Project']['youtube'];
                        $step1=explode('v=', $urlYoutube);
                        $step2 =explode('&',$step1[1]);
                        $video_id = $step2[0];
                        ?>
                        <center>
                            <object width="600" height="400" data="<?php echo 'http://www.youtube.com/embed/'. $video_id; ?>"></object>
                        </center>
                    </div>
                <?php endif; ?>

                <?php if($project['Project']['latitude'] != 0): ?>
                    <h2 class="page-header"><?=__("Posición en el mapa")?></h2>

                    <div class="map-property">
                        <div id="map-property"></div><!-- /#map-property -->
                    </div><!-- /.map-property -->
                <?php endif; ?>
                    
                <div class="module">
                    <div class="module-content">
                        <div class="agent-card">
                            <div class="row">
                                <div class="col-sm-12 col-md-4 mb30">
                                    <?php $url = Router::url('/').'ver/desarrolladora/'.$user['id'].'-' .strtolower(Inflector::slug($user['full_name'], '-')); ?>
                                    <a href="<?= $url ?>" class="agent-card-image">
                                        <div class="nailthumb-container square-thumb property-owner">
                                            <?php if(isset($user['img'])): ?>
                                                <?=$this->Html->image("ai/files/users/" . $user['img'], array(
                                                    "alt" => "Imagen de perfil",
                                                    "class" => "img-responsive img-thumbnail"
                                                ));?>
                                            <?php else: ?>
                                                <?= $this->Html->image("ai/no-image.jpg", array("alt" => __("Imagen no disponible"))); ?>
                                            <?php endif; ?>
                                        </div>
                                    </a><!-- /.agent-card-image -->
                                </div>

                                <div class="col-sm-12 col-md-3">
                                    <h2><?=__("Información de la desarrolladora")?></h2>

                                    <div class="agent-card-info">
                                        <?php ?>
                                        <ul>
                                            <?php if(isset($user['phone']) && $user['phone'] != ""): ?>
                                                <li><i class="fa fa-phone"></i><?=$user['phone']?></li>
                                            <?php endif; ?>
                                            <?php if(isset($user['username'])): ?>
                                                <li><i class="fa fa-at"></i> <a href="mailto:<?=$user['username']?>"><?=$user['username']?></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div><!-- /.agent-card-info -->
                                </div>

                                <div class="col-sm-12 col-md-5 mb30">
                                    <h2><?=__("Contáctalo")?></h2>

                                    <div class="agent-card-form">
                                        <form method="post" action="#">
                                            <div class="form-group">
                                                <input class="form-control" type="text" placeholder="Nombre">
                                            </div><!-- /.form-group -->

                                            <div class="form-group">
                                                <input class="form-control" type="text" placeholder="E-mail">
                                            </div><!-- /.form-group -->

                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="Mensaje"></textarea>
                                            </div><!-- /.form-group -->

                                            <button class="btn" type="submit"><?=__("Enviar mensaje");?></button>
                                        </form>
                                    </div><!-- /.agent-card-form -->
                                </div>
                            </div>
                        </div><!-- /.agent-card-->
                    </div><!-- /.module-content -->
                </div><!-- /.module -->
            </div>
            <?= $this->element('Page/ai-sidebar-search') ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var map_property = $('#map-property');
        if (map_property.length) {
            map_property.google_map({
                markers: [{
                    latitude: parseFloat('<?= $project['Project']['latitude'] ?>'),
                    longitude: parseFloat('<?= $project['Project']['longitude'] ?>')
                }], 
                center: {
                        latitude: parseFloat('<?= $project['Project']['latitude'] ?>'),
                        longitude: parseFloat('<?= $project['Project']['longitude'] ?>')
                },
                zoom: 16
            });
        }
    });
</script>