<?php
$projects = preg_replace('#\\/#', '/', json_encode($projects));
$escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
$replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
$projects = str_replace($escapers, $replacements, $projects);
?>
<div class="map-wrapper">
    <div id="map" class="map" data-transparent-marker-image="<?= $this->webroot ?>img/assets/transparent-marker-image.png"></div><!-- /.map -->
</div><!-- /.map-wrapper -->
<div class="map-filter-horizontal">
        <div class="container">
            <?= $this->Form->create('Project') ?>
                <div class="row">
                    <?= $this->Form->input('countries_id', array(
                        'label' => false,
                        'class' => 'form-control',
//                        'empty' => array(null => __('País')),
                        'div' => 'col-sm-4'
                    )); ?>
                    <?= $this->Form->input('provinces_id', array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => array(null => __('Provincia')),
                        'div' => 'col-sm-4',
                    )); ?>
                    <?= $this->Form->input('cities_id', array(
                        'label' => false,
                        'class' => 'form-control',
                        'empty' => array(null => __('Ciudad')),
                        'div' => 'col-sm-4'
                    ));?>
                </div><!-- /.row -->
            <?= $this->Form->end(); ?>
        </div><!-- /.container -->
    </div>

<div class="container">
    <div class="content">
        <div class="content col-sm-12 col-md-12">
            <h1 class="page-header"><?= __("Desarrolladoras e inmobiliarias") ?></h1>

            <?php foreach ($desarrolladoras as $desarrolladora): ?>
                <?php $url = Router::url('/').'ver/desarrolladora/'.$desarrolladora['User']['id'].'-' .strtolower(Inflector::slug($desarrolladora['User']['full_name'], '-')); ?>
                <div class="agency-row">
                    <div class="row">
                        <div class="agency-row-image col-sm-3">
                            <a href="<?= $url; ?>">
                                <div class="nailthumb-container square-thumb agencies-list">
                                    <?php if(isset($desarrolladora['User']['img'])): ?>
                                        <?= $this->Html->image("ai/files/users/" . $desarrolladora['User']['img']); ?>
                                    <?php else: ?>
                                        <?= $this->Html->image("ai/no-image.jpg", array("alt" => __("Imagen no disponible"))); ?>
                                    <?php endif; ?>
                                </div>
                            </a>
                        </div><!-- /.agency-row-image -->

                        <div class="agency-row-content col-sm-5">
                            <h2 class="agency-row-title"><a href="<?= $url; ?>"><?= $desarrolladora['User']['full_name']; ?></a></h2>
                            <?php $propertiesQty = $this->requestAction('app/countProperties/' . $desarrolladora['User']['id']); ?>
                            <div class="agency-row-subtitle"><?= $propertiesQty; ?>&nbsp;<?= $propertiesQty == 1 ? __("proyecto") : __("proyectos") ?></div>
                            <hr>
                            <?php if (isset($desarrolladora['User']['description']) && $desarrolladora['User']['description'] != ""): ?>
                                <p>
                                    <?= $desarrolladora['User']['description']; ?>
                                </p>
                            <?php endif; ?>
                        </div>
                        <div class="agency-row-info col-sm-4">
                            <ul>
                                <li><?= $desarrolladora['User']['phone'] ?></li>
                                <li><a href="mailto:<?= $desarrolladora['User']['username'] ?>"><?= $desarrolladora['User']['username'] ?></a></li>
                            </ul>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.agency-row -->
            <?php endforeach; ?>

            <div class="cake-pagination col-xs-12 text-center">
                <?php
                echo $this->Paginator->prev('< ', array(), null, array('class' => 'prev disabled'));
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next(' >', array(), null, array('class' => 'next disabled'));
                ?>
            </div>
            <br><br>
            <div class="cake-pagination col-xs-12 text-center">
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, empezando en el registro {:start}, y terminando en el {:end}')
                ));
                ?>
            </div>
        </div><!-- /.content -->
    </div>
</div>

<script>
    function updateCity(city, flag){
        if(city != null){
            var getCitiesUrl = "<?= $this->Html->url(['controller' => 'projects', 'action' => 'getCities']); ?>/" + city;
            $.getJSON(getCitiesUrl, function (selectValues) {
                $('#ProjectCitiesId').find('option').remove().end();
                $('#ProjectCitiesId').append($("<option></option>").attr("value", "0").text("<?= __('Ciudad'); ?>"));
                $.each(selectValues, function (key, value) {
                    $('#ProjectCitiesId').append($("<option></option>").attr("value", key).text(value));
                });
                if(flag == false){
                    $('#ProjectCitiesId').selectpicker('val', "<?= $this->data['Project']['cities_id'] ?>");
                }
                $('#ProjectCitiesId').selectpicker('refresh');
            });
        }
    }
    function updateProvince(province, flag){
        if(province != null){
            var getProvincesUrl = "<?= $this->Html->url(['controller' => 'projects', 'action' => 'getProvinces']); ?>/" + province;
            $.getJSON(getProvincesUrl, function (selectValues) {
                $('#ProjectProvincesId').find('option').remove().end();
                $('#ProjectProvincesId').append($("<option></option>").attr("value", "0").text("<?= __('Provincia'); ?>"));
                $.each(selectValues, function (key, value) {
                    $('#ProjectProvincesId').append($("<option></option>").attr("value", key).text(value));
                });
                if(flag == false){
                    $('#ProjectProvincesId').selectpicker('val', "<?= $this->data['Project']['provinces_id'] ?>");
                }
                $('#ProjectProvincesId').selectpicker('refresh');
            });
        }
    }
    $(document).ready(function () {
        $('#ProjectCountriesId').selectpicker('val', "<?= $this->data['Project']['countries_id'] ?>");
        $('#ProjectCountriesId').selectpicker('refresh');
        updateProvince($('#ProjectCountriesId').val(), false);
        updateCity($('#ProjectProvincesId').val(), false);

        $('#ProjectCountriesId').change(function () {
           updateProvince($(this).val(), true);
        });

        $('#ProjectProvincesId').change(function () {
            updateCity($(this).val(), true);
            location.href = "<?= $this->Html->url(array('controller' => 'projects', 'action' => 'viewAll')); ?>/" + $('#ProjectCountriesId').val() + "/" + $(this).val();
        });

        $('#ProjectCitiesId').change(function () {
            location.href = "<?= $this->Html->url(array('controller' => 'projects', 'action' => 'viewAll')); ?>/" + $('#ProjectCountriesId').val() + "/" + $('#ProjectProvincesId').val() + "/" + $(this).val();
        });


        var map = $('#map');
        var markers = new Array();
//      var colors = ['orange', 'blue', 'cyan', 'pink', 'deep-purple', 'teal', 'indigo', 'green', 'light-green', 'amber', 'yellow', 'deep-orange', 'brown', 'grey'];

        var projects = '<?= $projects; ?>';

        $.each($.parseJSON(projects), function(i, item){
            var id = item['Project']['id'];
            var name = item['Project']['name'];
            var email = item['Project']['email'];
            var phone = item['Project']['phone'];
            var outstanding = item['Project']['outstanding'];
            var latitude = parseFloat(item['Project']['latitude']);
            var longitude = parseFloat(item['Project']['longitude']);
            if(typeof item['ProjectsImage'][0] != 'undefined'){
                var urlImg = item['ProjectsImage'][0]['rute'] ;
            } else {
                var urlImg = 'ai/no-image.jpg';
            }
            var pais = item['Countries']['name'];
            var provincia = item['Provinces']['name'];
            var ciudad = item['Cities']['name'];

            var dir = ciudad + ', ' + provincia;

            if(outstanding == 1){
                color = 'deep-purple';
            } else {
                color = 'orange';
            }
            
            var url =  "<?php echo Router::url('/') ?>" + "detalle/proyecto/"  + id + "-" + slugify(name);

            markers.push({
                latitude: latitude,
                longitude: longitude,
                marker_content: '<div class="marker ' + color + '"><img src="<?php echo $this->webroot; ?>img/assets/house.png" alt=""></div>',
                content: '<div class="infobox ' + color + ' "><a class="infobox-image" href="' + url + '"><div style="margin: 1.4em 0;"><span></span><img width="120" class="img-responsive" src="<?php echo $this->webroot; ?>img/' + urlImg + '" alt=""></div></a><div class="infobox-content"><div class="infobox-content-title"><a href="#">' + name + '</a></div><div class="infobox-content-body"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;' + dir +  '<br><i class="fa fa-at"></i>&nbsp;&nbsp;' + email + '<br><i class="fa fa-phone"></i>&nbsp;&nbsp;' + phone + '</div></div>'
            });
        });


        if (map.length) {
            map.google_map({
                infowindow: {
                    borderBottomSpacing: 0,
                    height: 120,
                    width: 424,
                    offsetX: 30,
                    offsetY: -80
                },
                zoom: 11,
                transparentMarkerImage: map.data('transparent-marker-image'),
                transparentClusterImage: map.data('transparent-marker-image'),
                markers: markers,
                styles: [{"featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{"color": "#444444"}]}, {"featureType": "landscape", "elementType": "all", "stylers": [{"color": "#f2f2f2"}]}, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "poi.government", "elementType": "labels.text.fill", "stylers": [{"color": "#b43b3b"}]}, {"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"hue": "#ff0000"}]}, {"featureType": "road", "elementType": "all", "stylers": [{"saturation": -100}, {"lightness": 45}]}, {"featureType": "road", "elementType": "geometry.fill", "stylers": [{"lightness": "8"}, {"color": "#bcbec0"}]}, {"featureType": "road", "elementType": "labels.text.fill", "stylers": [{"color": "#5b5b5b"}]}, {"featureType": "road.highway", "elementType": "all", "stylers": [{"visibility": "simplified"}]}, {"featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#7cb3c9"}, {"visibility": "on"}]}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#abb9c0"}]}, {"featureType": "water", "elementType": "labels.text", "stylers": [{"color": "#fff1f1"}, {"visibility": "off"}]}]
            });
        }
    });
    function slugify(text)
    {
      return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    }
</script>
