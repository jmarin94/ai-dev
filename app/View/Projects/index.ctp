<div class="map-wrapper">
    <div id="map" class="map"></div>
    <?php debug($projects);?>
</div>

<script type="text/css">
    
    var latitude = 9.921;
    var longitud = -84.088;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: new google.maps.LatLng(latitude, longitud),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    
</script>