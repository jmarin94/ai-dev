<div class="container">
    <div class="row">
        <div class=" col-sm-8 col-md-9">
            
            <h1 class="page-header"><?=__('Propiedades');?></h1>
            <div class="box">
                <div class="row property-simple-wrapper">
                    
                    <?php foreach ($properties as $propertie){?>
                        
                        <?php $propertieUrl = $propertie['Property']['id'] . '/' . $this->getUrl->clearName($propertie['Property']['title']); ?>
                        <div class="col-sm-3">
                            <div class="property-simple">
                                <a href="<?=$this->Html->url(['controller' => 'Properties', 'action' => 'view/' . $propertieUrl])?>" class="property-simple-image">
                                    <?php if(count($propertie['PropertiesImages']) > 0){ ?>
                                        <img src="<?=$this->Html->url('/files/img/' . $propertie['PropertiesImages'][0]['rute'])?>" height="100" alt="">
                                    <?php } ?>
                                </a>

                                <div class="property-simple-content">
                                    <h2 class="property-simple-title">
                                        <a href="<?=$this->Html->url(['controller' => 'Properties', 'action' => 'view/' . $propertieUrl])?>">
                                            <?=$propertie['Property']['title']?>
                                        </a>
                                    </h2>

                                    <ul class="property-simple-location">
                                        <li><?=$propertie['Provinces']['name']?>,</li>
                                        <li><?=$propertie['Cities']['name']?></li>
                                    </ul>

                                    <div class="property-simple-status">
                                        <?=$propertie['Categories']['name']?>, <?=$propertie['PropertyType']['name']?>
                                    </div>
                                </div><!-- /.property-simple-content -->
                            </div><!-- /.property-simple -->
                        </div>
                    
                    <?php } ?>
                    
                </div>
            </div>
            
        </div>
        <div class="col-sm-4 col-md-3">
            <div class="widget-title">
                <h2><?=__('Buscar Propiedades');?></h2>
            </div>
            <div class="widget-content">
                <?php
                    echo $this->Form->create('Property', ['novalidate' => true, 'url' => ['controller' => 'search', 'action' => 'index']]);
                    echo $this->Form->input('property_type', [
                        'label'         => false,
                        'class'         => 'form-control',
                        'empty'         => [null => __('Tipo de Propiedad')],
                        'div'           => 'form-group'
                    ]);
                    echo $this->Form->input('categories_id', [
                        'label'         => false,
                        'class'         => 'form-control',
                        'empty'         => [null => __('Categoria')],
                        'div'           => 'form-group'
                    ]);
                    echo $this->Form->input('countries_id', [
                        'label'         => false,
                        'class'         => 'form-control',
                        'empty'         => [null => __('Pais')],
                        'div'           => 'form-group'
                    ]);
                    echo $this->Form->input('provinces_id', [
                        'label'         => false,
                        'class'         => 'form-control',
                        'empty'         => [null => __('Provincia')],
                        'div'           => 'form-group'
                    ]);
                     echo $this->Form->input('cities_id', [
                        'label'         => false,
                        'class'         => 'form-control',
                        'empty'         => [null => __('Ciudad')],
                        'div'           => 'form-group'
                    ]);
                ?>
                <button class="btn btn-lg btn-block"><i class="fa fa-search"></i> <?=__('Buscar');?></button>
                <?=$this->Form->end();?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#PropertyCountriesId').change(function(){
            var getProvincesUrl = "<?=$this->Html->url(['controller' => 'properties', 'action' => 'getProvinces']);?>/" + $(this).val();
            $.getJSON(getProvincesUrl, function(selectValues){
                $('#PropertyProvincesId').find('option').remove().end();
                $('#PropertyProvincesId').append($("<option></option>").attr("value","0").text("<?=__('Provincia');?>")); 
                $.each(selectValues, function(key, value) {   
                    $('#PropertyProvincesId').append($("<option></option>").attr("value",key).text(value)); 
               });
               $('#PropertyProvincesId').selectpicker('refresh');
            })
        });
        
        $('#PropertyProvincesId').change(function(){
            var getCitiesUrl = "<?=$this->Html->url(['controller' => 'properties', 'action' => 'getCities']);?>/" + $(this).val();
            $.getJSON(getCitiesUrl, function(selectValues){
                $('#PropertyCitiesId').find('option').remove().end();
                $('#PropertyCitiesId').append($("<option></option>").attr("value","0").text("<?=__('Ciudad');?>")); 
                $.each(selectValues, function(key, value) { 
                    $('#PropertyCitiesId').append($("<option></option>").attr("value",key).text(value)); 
                });
                $('#PropertyCitiesId').selectpicker('refresh');
            });
        });
        
    });
</script>