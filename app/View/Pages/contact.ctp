
<div id="map-contact"></div><!-- /#map-contact -->

<div class="container">
    <div class="content">

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <h2 class="mb30 mt16">Dirección</h2>

                        <p>
                            San José, Costa Rica
                        </p>
                    </div><!-- /.col-* -->

                    <div class="col-sm-6">
                        <h2 class="mb30 mt16">Persona a contactar</h2>

                        <p>
                            Andrés Sancho<br>
                            E-mail: <a href="mailto:<?=__("info@americainmobiliaria.com");?>"><?=__("info@americainmobiliaria.com");?></a>
                        </p>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-sm-6">
                        <h2>E-mail</h2>

                        <p>
                            Information: <a href="mailto:<?=__("info@americainmobiliaria.com");?>"><?=__("info@americainmobiliaria.com");?></a><br>
                        </p>
                    </div><!-- /.col-* -->

                    <div class="col-sm-6">
                        <h2>Redes Sociales</h2>

                        <ul class="clearfix sharing-buttons">
                            <li>
                                <a class="facebook" target="_blank" href="https://www.facebook.com/americainmobiliaria/?fref=ts">
                                    <i class="fa fa-facebook fa-left"></i>
                                    <span class="social-name">Facebook</span>
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.col-* -->

            <div class="col-sm-6">
                <div class="box">
                    <h2 class="mt0">Formulario</h2>

                    <?= $this->Form->create(null); ?>
                        <div class="form-group">
                            <input class="form-control" type="text" required="required" name="data[name]" placeholder="Nombre">
                        </div><!-- /.form-group -->

                        <div class="form-group">
                            <input class="form-control" type="text" required="required" name="data[subject]" placeholder="Asunto">
                        </div><!-- /.form-group -->

                        <div class="form-group">
                            <input class="form-control" type="text" required="required" name="data[email]" placeholder="E-mail">
                        </div><!-- /.form-group -->

                        <div class="form-group">
                            <textarea class="form-control" required="required" name="data[message]" placeholder="Mensaje"></textarea>
                        </div><!-- /.form-group -->

                        <button class="btn" type="submit"><?=__("Enviar mensaje");?></button>
                    <?= $this->Form->end(); ?>
                </div><!-- /.box -->
            </div><!-- /.col-* -->
        </div><!-- /.row -->

    </div><!-- /.content -->
</div><!-- /.container -->
