<div class="map-wrapper">
    <div id="map" class="map" data-transparent-marker-image="<?= $this->webroot ?>img/assets/transparent-marker-image.png"></div><!-- /.map -->
</div><!-- /.map-wrapper -->

<div class="container">
    <div class="content">
        <div class="content col-sm-12 col-md-12">
            <h1 class="page-header"><?= __("Desarrolladoras e inmobiliarias") ?></h1>

            <?php foreach ($desarrolladoras as $desarrolladora): ?>
                <div class="agency-row">
                    <div class="row">
                        <div class="agency-row-image col-sm-3">
                            <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'view', $desarrolladora['User']['id'])) ?>">
                                <div class="nailthumb-container square-thumb agencies-list">
                                    <?= $this->Html->image("ai/files/users/" . $desarrolladora['User']['img']); ?>
                                </div>
                            </a>
                        </div><!-- /.agency-row-image -->

                        <div class="agency-row-content col-sm-5">
                            <h2 class="agency-row-title"><a href="javascript:void(8)"><?= $desarrolladora['User']['full_name']; ?></a></h2>
                            <?php $propertiesQty = $this->requestAction('app/countProperties/' . $desarrolladora['User']['id']); ?>
                            <div class="agency-row-subtitle"><?= $propertiesQty; ?>&nbsp;<?= __("proyectos") ?></div>
                            <hr>
                            <?php if (isset($desarrolladora['User']['description']) && $desarrolladora['User']['description'] != ""): ?>
                                <p>
                                    <?= $desarrolladora['User']['description']; ?>
                                </p>
                            <?php endif; ?>
                        </div>
                        <div class="agency-row-info col-sm-4">
                            <ul>
                                <li><?= $desarrolladora['User']['phone'] ?></li>
                                <li><a href="mailto:<?= $desarrolladora['User']['username'] ?>"><?= $desarrolladora['User']['username'] ?></a></li>
                            </ul>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.agency-row -->
            <?php endforeach; ?>

            <p>
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de un total de {:count}, empezando en el registro {:start}, y terminando en el {:end}')
                ));
                ?>	
            </p>
            <div class="paging">
                <?php
                echo $this->Paginator->prev('< ' . __('ANTERIOR'), array(), null, array('class' => 'prev disabled'));
                echo $this->Paginator->numbers(array('separator' => ''));
                echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                echo $this->Paginator->next(__('SIGUIENTE') . ' >', array(), null, array('class' => 'next disabled'));
                ?>
            </div>
        </div><!-- /.content -->
    </div>
</div>

<script>
    $(document).ready(function () {
        var map = $('#map');
        var markers = new Array();
//      var colors = ['orange', 'blue', 'cyan', 'pink', 'deep-purple', 'teal', 'indigo', 'green', 'light-green', 'amber', 'yellow', 'deep-orange', 'brown', 'grey'];
        
        var projects = '<?= json_encode($projects); ?>';
        
        $.each($.parseJSON(projects), function(i, item){
            var id = item['Project']['id'];
            var name = item['Project']['name'];
            var email = item['Project']['email'];
            var phone = item['Project']['phone'];
            var outstanding = item['Project']['outstanding'];
            var latitude = parseFloat(item['Project']['latitude']);
            var longitude = parseFloat(item['Project']['longitude']);
            var urlImg = item['ProjectsImage'][0]['rute'];
            var pais = item['Countries']['name'];
            var provincia = item['Provinces']['name'];
            var ciudad = item['Cities']['name'];
            
            var dir = ciudad + ', ' + provincia + ', ' + pais;
            
            if(outstanding == 1){
                color = 'orange';
            } else {
                color = 'deep-purple';
            }
            
            markers.push({
                latitude: latitude,
                longitude: longitude,
                marker_content: '<div class="marker ' + color + '"><img src="img/assets/house.png" alt=""></div>',
                content: '<div class="infobox ' + color + ' "><a class="infobox-image" href=""><div style="margin: 1.4em 0;"><span></span><img width="120" class="img-responsive" src="img/' + urlImg + '" alt=""></div></a><div class="infobox-content"><div class="infobox-content-title"><a href="#">' + name + '</a></div><div class="infobox-content-body"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;' + dir +  '<br><i class="fa fa-at"></i>&nbsp;&nbsp;' + email + '<br><i class="fa fa-phone"></i>&nbsp;&nbsp;' + phone + '</div></div>'
            });
        });
        

//        function get_gps_ranges(center_lat, center_lng, range_level_lat, range_level_lng) {
//            var lat = center_lat + (Math.random() * (range_level_lat + range_level_lat) - range_level_lat);
//            var lng = center_lng + (Math.random() * (range_level_lng + range_level_lng) - range_level_lng);
//            return Array(lat, lng);
//        }
//
//        for (var i = 0; i < 30; i++) {
//            var position = get_gps_ranges(9.9356124, -84.1483645, 0.08, 0.60);
//            var color = colors[Math.floor(Math.random() * colors.length)];
//            markers.push({
//                latitude: position[0],
//                longitude: position[1],
//                marker_content: '<div class="marker ' + color + '"><img src="img/assets/house.png" alt=""></div>',
//                content: '<div class="infobox ' + color + ' "><a class="infobox-image" href=""><img src="assets/img/tmp/1-small.png" alt=""></a><div class="infobox-content"><div class="infobox-content-title"><a href="#">Madison Street 322</a></div><div class="infobox-content-price">$ 230,000</div><div class="infobox-content-body">Integer sit amet nibh erat. Maecenas accumsan nibh at porta euismod.</div></div><div class="infobox-contact"><div class="infobox-contact-title"><a href="#">John Doe</a></div><div class="infobox-contact-body">Effectivity Real Estate<br>Wardrobe Street 90210<br><i class="fa fa-phone"></i>012-123-456</div><a href="#" class="close"><i class="fa fa-close"></i></a></div></div>'
//            });
//        }

        if (map.length) {
            map.google_map({
                infowindow: {
                    borderBottomSpacing: 0,
                    height: 120,
                    width: 424,
                    offsetX: 30,
                    offsetY: -80
                },
                zoom: 11,
                transparentMarkerImage: map.data('transparent-marker-image'),
                transparentClusterImage: map.data('transparent-marker-image'),
                markers: markers,
                styles: [{"featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{"color": "#444444"}]}, {"featureType": "landscape", "elementType": "all", "stylers": [{"color": "#f2f2f2"}]}, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "poi.government", "elementType": "labels.text.fill", "stylers": [{"color": "#b43b3b"}]}, {"featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{"hue": "#ff0000"}]}, {"featureType": "road", "elementType": "all", "stylers": [{"saturation": -100}, {"lightness": 45}]}, {"featureType": "road", "elementType": "geometry.fill", "stylers": [{"lightness": "8"}, {"color": "#bcbec0"}]}, {"featureType": "road", "elementType": "labels.text.fill", "stylers": [{"color": "#5b5b5b"}]}, {"featureType": "road.highway", "elementType": "all", "stylers": [{"visibility": "simplified"}]}, {"featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "transit", "elementType": "all", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "elementType": "all", "stylers": [{"color": "#7cb3c9"}, {"visibility": "on"}]}, {"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#abb9c0"}]}, {"featureType": "water", "elementType": "labels.text", "stylers": [{"color": "#fff1f1"}, {"visibility": "off"}]}]
            });
        }
    });
</script>