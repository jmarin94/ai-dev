<?php
App::uses('AppModel', 'Model');
/**
 * Flag Model
 *
 * @property Property $Property
 */
class Flag extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

        public $hasAndBelongsToMany = array(
		'Properties' => array(
			'className' => 'Property',
			'joinTable' => 'properties_flags',
			'foreignKey' => 'flags_id',
			'associationForeignKey' => 'id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);
}
