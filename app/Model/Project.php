<?php
App::uses('AppModel', 'Model');
/**
 * Project Model
 *
 * @property Users $Users
 * @property Countries $Countries
 * @property Provinces $Provinces
 * @property Cities $Cities
 */
class Project extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'users_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
            'name' => array(
                'alphaNumeric' => array(
                    'rule'      => array('minLength', 3),
                    'message'   => 'Este campo es requerido y debe de tener almenos 3 caracteres',
                )
            )
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Users' => array(
			'className' => 'Users',
			'foreignKey' => 'users_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Countries' => array(
			'className' => 'Countries',
			'foreignKey' => 'countries_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Provinces' => array(
			'className' => 'Provinces',
			'foreignKey' => 'provinces_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cities' => array(
			'className' => 'Cities',
			'foreignKey' => 'cities_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        
        public $hasMany = array(
            'ProjectsImage' => array(
                    'className' => 'ProjectsImage',
                    'foreignKey' => 'projects_id'
            )
        );
}
