<?php
App::uses('AppModel', 'Model');
/**
 * PropertiesFlag Model
 *
 * @property Flags $Flags
 * @property Properties $Properties
 */
class PropertiesFlag extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'flags_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'properties_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Flags' => array(
			'className' => 'Flags',
			'foreignKey' => 'flags_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Properties' => array(
			'className' => 'Properties',
			'foreignKey' => 'properties_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
