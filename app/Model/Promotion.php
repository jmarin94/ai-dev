<?php
App::uses('AppModel', 'Model');
/**
 * Promotion Model
 *
 * @property Property $Property
 */
class Promotion extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Property' => array(
			'className' => 'Property',
			'joinTable' => 'properties_promotions',
			'foreignKey' => 'promotion_id',
			'associationForeignKey' => 'property_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
