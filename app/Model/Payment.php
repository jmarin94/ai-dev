<?php
App::uses('AppModel', 'Model');
/**
 * Project Model
 *
 * @property Users $Users
 * @property Countries $Countries
 * @property Provinces $Provinces
 * @property Cities $Cities
 */
class Payment extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
