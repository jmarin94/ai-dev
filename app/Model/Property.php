<?php
App::uses('AppModel', 'Model');
/**
 * Property Model
 *
 * @property TypeProperties $TypeProperties
 * @property Categories $Categories
 * @property Countries $Countries
 * @property Provinces $Provinces
 * @property Cities $Cities
 * @property Flag $Flag
 * @property Promotion $Promotion
 */
class Property extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
                'title' => array(
                    'alphaNumeric' => array(
                        'rule'      => array('minLength', 3),
                        'message'   => 'Este campo es requerido y debe de tener almenos 3 caracteres',
                    )
                ),
		'type_properties_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Este campo es requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'categories_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Este campo es requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'countries_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Este campo es requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'provinces_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Este campo es requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cities_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Este campo es requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'PropertyType' => array(
                        'className' => 'PropertyType',
			'foreignKey' => 'property_type',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Categories' => array(
			'className' => 'Categories',
			'foreignKey' => 'categories_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Countries' => array(
			'className' => 'Countries',
			'foreignKey' => 'countries_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Provinces' => array(
			'className' => 'Provinces',
			'foreignKey' => 'provinces_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cities' => array(
			'className' => 'Cities',
			'foreignKey' => 'cities_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        
        
        public $hasMany = array(
            'PropertiesImages' => array(
                    'className' => 'PropertiesImages',
                    'foreignKey' => 'properties_id'
            )
        );
        /**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Flag' => array(
			'className' => 'Flag',
			'joinTable' => 'properties_flags',
			'foreignKey' => 'properties_id',
			'associationForeignKey' => 'flags_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
