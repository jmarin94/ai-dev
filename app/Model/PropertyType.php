<?php
App::uses('AppModel', 'Model');
/**
 * TypeProperty Model
 *
 */
class PropertyType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
