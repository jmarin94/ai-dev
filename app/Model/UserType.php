<?php
App::uses('AppModel', 'Model');
/**
 * TypeUser Model
 *
 */
class UserType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
