<?php
App::uses('TypeProperty', 'Model');

/**
 * TypeProperty Test Case
 */
class TypePropertyTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.type_property'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TypeProperty = ClassRegistry::init('TypeProperty');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TypeProperty);

		parent::tearDown();
	}

}
